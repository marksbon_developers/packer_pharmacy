<script type="text/javascript">
    $(document).ready(function(){

        // General Settings
        var resulturl = location.href;
        $('#resulturl').val(location.href) ;
        $('[name="resulturl"]').val(resulturl);

        // Edit Category
        $(".table").on("click", ".editcat", function(){
            $("#editid").val($(this).data('editid'));
            $("#oldname").val($(this).data('editname'));
            $('#catdescmodal').modal('show');
        });

        // Delete Category
        $(".deletebtn").click(function(){ // Click to only happen on announce links
            $("#form_url").attr('action', $(this).data('formurl'));
            $("#deleteId").val($(this).data('delid'));
            $("#deletename").text($(this).data('delname'));
            $('[name="resulturl"]').val(resulturl);
            $('#deletemodal').modal('show');
        });

    });
</script>