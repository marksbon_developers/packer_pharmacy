<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#employees" aria-controls="messages" role="tab" data-toggle="tab">All Employees</a></li>
                <li role="presentation"><a href="#users" aria-controls="home" role="tab" data-toggle="tab">All Users</a></li>
                <li role="presentation"><a href="#new" aria-controls="profile" role="tab" data-toggle="tab">Add New</a></li>
                <!--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>-->
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="employees">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body no-padding">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="width:5%;">#</th>
                              <th style="width:15%;">Employee ID #</th>
                              <th style="width:20%;">FullName</th>
                              <th style="width:15%;">Residence</th>
                              <th style="width:15%;">Telephone</th>
                              <th style="width:30%;">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                #Displaying All Suppliers Info
                                if(!empty($allemployees)) {
                                  
                                  $counter = 1;
                                  foreach($allemployees As $employee) {
                                    #System Admin Restriction
                                    if((substr_count($employee->employee_id, "KAD/SYS/")) && ($_SESSION['username'] != "sysadmin"))
                                        continue;
                              ?>
                              <tr>
                                <td><?= @$counter; ?></td>
                                <td><?= $employee->employee_id; ?></td>
                                <td><?= $employee->fullname; ?></td>
                                <td><?= $employee->residence; ?></td>
                                <td><?= $employee->tel1; ?></td>
                                <td>
                                <form action="Activation" method="Post" style="display:inline !important">
                                <input type="hidden" name="autoid" value="<?= $employee->id; ?>" />
                                  <?php 
                                    if( isset($_SESSION['username']) &&  in_array('USERS',$_SESSION['rows_exploded']) ) {
                                  ?>
                                  <a title="Edit" class="btn btn-primary btn-xs userdetails" data-key="<?= $employee->id ?>" data-empid="<?= $employee->employee_id ?>" data-name="<?= $employee->fullname ?>" data-dob="<?= $employee->dob ?>" data-gender="<?= $employee->gender ?>" data-res="<?= $employee->residence ?>" data-tel1="<?= $employee->tel1 ?>" data-tel2="<?= $employee->tel2 ?>"  data-email="<?= $employee->email ?>" data-emename="<?= $employee->eme_name ?>" data-emetel="<?= $employee->eme_tel ?>">
                                    <i class="fa fa-pencil"></i> Edit 
                                  </a>
                                  <?php 
                                        }
                                    if( false /*isset($_SESSION['username']) &&  in_array('USERS',$_SESSION['rows_exploded'])*/ ) {
                                  ?>
                                  <a title="Delete" class="btn btn-danger btn-xs deletebtn" data-toggle="modal" data-delname="<?= $employee->fullname; ?>" data-delid="<?= $employee->employee_id; ?>" data-formurl="Del_Employee_Complete">
                                    <i class="fa fa-trash"></i> Delete
                                  </a>
                                  <?php } ?>
                                </form>
                                <button type='button' name='user_details' class='btn btn-info btn-xs userdetails' data-key="<?= $employee->id ?>" data-empid="<?= $employee->employee_id ?>" data-name="<?= $employee->fullname ?>" data-dob="<?= $employee->dob ?>" data-gender="<?= $employee->gender ?>" data-res="<?= $employee->residence ?>" data-tel1="<?= $employee->tel1 ?>" data-tel2="<?= $employee->tel2 ?>"  data-email="<?= $employee->email ?>" data-emename="<?= $employee->eme_name ?>" data-emetel="<?= $employee->eme_tel ?>"><i class='fa fa-th'></i> Details</button>
                                </td> 
                              </tr> 
                              <?php
                                #
                                    $counter++; }
                                  }
                              ?> 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="users">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body no-padding">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th style="width:5%;">ID #</th>
                              <th style="width:20%;">UserName</th>
                              <th style="width:15%;">Account Status</th>
                              <th style="width:40%;">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                #Displaying All Suppliers Info
                                if(!empty($allusers)) {
                                  
                                  $counter = 1;
                                  foreach($allusers As $user) {
                                    #System Admin Restriction
                                    if(($user->Employee_id == "KAD/SYS/OO1") && ($_SESSION['username'] != "sysadmin"))
                                        continue;
                              ?>
                              <tr>
                                <td><?= $counter; ?></td>
                                <td><?= $user->Username; ?></td>
                                <td><?php $status = $user->Account_Status; 
                                    if($status == 0)
                                       print "<span class='label label-danger'>Inactive</span>";
                                    else if($status == 1)
                                       print "<span class='label label-success'>Active</span>";
                                    else
                                      print "<span class='label label-warning'>Unknown</span>";
                                ?>
                                </td>
                                <td>
                                <form action="Activation" method="Post" style="display:inline !important">
                                <input type="hidden" name="username" value="<?= $user->Username; ?>" />
                                  <?php 
                                    if( isset($_SESSION['username']) &&  in_array('USERS',$_SESSION['rows_exploded']) ) {
                                  ?>
                                  <a title="Delete" class="btn btn-primary btn-xs resetbtn" data-username="<?= $user->Username; ?>" data-userde="<?= base64_encode($user->Username); ?>">
                                    <i class="fa fa-key"></i> Reset 
                                  </a>
                                  <?php 
                                        }
                                    if( isset($_SESSION['username']) &&  in_array('USERS',$_SESSION['rows_exploded'])) {
                                  ?>
                                  <!-- <a title="Delete" class="btn btn-danger btn-xs deletebtn" data-toggle="modal" data-delname="<?= $user->Username; ?>" data-delid="<?= $user->id; ?>" data-formurl="Delete_User">
                                    <i class="fa fa-trash"></i> Delete -->
                                  </a>
                                  <?php } ?>
                                  <?php $status = $user->Account_Status; 
                                    if($status == 0 &&  in_array('USERS',$_SESSION['rows_exploded']))
                                       print "<button type='submit' name='activate_usr' class='btn btn-success btn-xs' style='width: 85px !important;padding-right: 20px;'><i class='fa fa-unlock'></i> Activate</button>";
                                    
                                    else if($status == 1 &&  in_array('USERS',$_SESSION['rows_exploded']))
                                       print "<button type='submit' name='deactivate_usr' class='btn btn-danger btn-xs'><i class='fa fa-lock'></i> Deactivate</button>";
                                  ?>
                                </form>
                                <button type='button' class='btn btn-info btn-xs user_details' data-username="<?= $user->Username; ?>" data-userk="<?= base64_encode($user->id) ?>"><i class='fa fa-th'></i> Roles</button>
                                </td> 
                              </tr> 
                              <?php
                                #
                                    $counter++; }
                                  }
                              ?> 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="new">
                  <form action="Add_User" method="post">
                    <input type="hidden" class="form-control" name="id" value="<?= $id; ?>" readonly required />
                    <div class="col-md-4">
                      <label class="col-md-12 control-label" style="font-size:25px !important;">Basic Information</label>
                           <div class="form-group">
                          <label class="col-md-6 control-label">Full Name <span style="color:red">*</span></label>
                          <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Enter Here....." name="fullname" required>
                          </div>
                        </div>
                        <!--<div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label">Date Of Birth<span style="color:red">*</span></label>
                            <div class="">
                              <input type="text" class="form-control datepicker" placeholder="" name="dob">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                         <div class="form-group">
                          <label class="control-label">Gender <span style="color:red">*</span></label>
                           <select class="select2" name="gender" data-placeholder="--Select--" style="width:100%">
                            <option></option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                          </select>
                        </div>
                        </div>-->
                          <div class="form-group">
                          <label class="col-md-8 control-label">Emergency Contact Name <span style="color:red">*</span></label>
                          <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Enter Here......" name="emergencyname">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="col-md-12 control-label">Emergency Contact Number <span style="color:red">*</span></label>
                          <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="Enter Here....." name="emergencytel">
                          </div>
                        </div>
                        </div>
                    <div class="col-md-4">
                      <label class="col-md-12 control-label" style="font-size:25px !important;">Contact Information</label>
                       <div class="form-group">
                      <label class="col-md-8 control-label">Residence Address <span style="color:red">*</span></label>
                      <div class="col-md-12">
                        <input type="text" class="form-control" placeholder="Enter Here......" name="residenceAddr" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-8 control-label">Telephone Number <span style="color:red">*</span></label>
                      <div class="col-md-12">
                        <input type="text" class="form-control" placeholder="Enter Here....." name="tel1" required>
                      </div>
                   </div>
                    <div class="form-group">
                    <label class="col-md-8 control-label">Alt. Telephone number</label>
                    <div class="col-md-12">
                      <input type="text" class="form-control" placeholder="Enter Here......." name="tel2">
                    </div>
                  </div>
                   <!--<div class="form-group">
                    <label class="col-md-4 control-label">Email</label>
                    <div class="col-md-12">
                      <input type="email" class="form-control" placeholder="Enter Here......" name="email">
                    </div>
                  </div>-->
                  </div>
                  <div class="col-md-4">
                  <label class="col-md-12 control-label" style="font-size:25px !important;">Login Information</label>
                    <div class="form-group">
                      <!--<label class="col-md-7 control-label">EmployeesID</label>-->
                      <div class="col-md-12">
                        <input type="hidden" class="form-control" name="employee_id" value="<?= $next_usr_id ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-7 control-label">Username <span style="color:red">*</span></label>
                      <div class="col-md-12">
                        <input type="text" class="form-control" placeholder="Enter Here......" name="usr_name" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-7 control-label">Password <span style="color:red">*</span></label>
                      <div class="col-md-12">
                        <input type="password" class="form-control" placeholder="" name="usr_pwd_1" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-7 control-label">Confim Password <span style="color:red">*</span></label>
                      <div class="col-md-12">
                        <input type="password" class="form-control" placeholder="" name="usr_pwd_2" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12" >
                    <center>
                      <div class="form-footer">
                        <div class="form-group">
                          <div class="col-md-7 col-md-offset-2">
                            <button type="submit" class="btn btn-primary" name="add_usr">Save</button>
                            <button type="button" class="btn btn-danger">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </center>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>