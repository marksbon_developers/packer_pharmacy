<script type="text/javascript">

  function fetch_itms_from_category(descid, arraySize) {
      // disabling other tabs
      for(i = 1; i <= arraySize; i++) {
          if (i == descid) {
              $('#cat-'+ i).css('background-color', "#efefef");
              continue;
          }
          /*else
              $('#cat-'+ i).css("pointer-events", "none");*/
      }

    $.ajax({
      type: 'POST',
      url: 'prod_ret_ajax_cat',
      data: {desc_id: descid},
      success: function(prod_response){
        if(prod_response)
        {
          document.getElementById("ProductDisplay").innerHTML=prod_response;
        }
        else 
        {
          $('#noproductmodal').modal('show');
        }
      }   
    });
  }

  function addtotally(obj) {
    var prodname = obj.getAttribute('data-prodname');
    var unitprice = obj.getAttribute('data-unitprice');
    var promounitprice = obj.getAttribute('data-promounitprice');
    var promounitqty = obj.getAttribute('data-promounitqty');
    
    $('#tallytbl tr:last').after('<tr><td style="color:red"><i class="fa fa-close del_req_row" style="cursor:pointer"></i></td><td><input type="hidden" name="prodname[]" value="'+prodname+'"/>'+prodname+'</td><td><input type="number" value="0" min="1" class="me qty" name="qty[]" data-unitprice="'+unitprice+'"  data-promo_unit_price="'+promounitprice+'" data-promo_unit_qty="'+promounitqty+'" /></td><td><input type="number" placeholder="1" min="1" class="me unit promocheck" name="unit[]" value="'+unitprice+'" readonly/></td><td><input type="text" placeholder="0" min="1" class="  me totalamt" name="totalsum[]" readonly /></td></tr>');

    $(".qty").on('input',compute);
    $(".unit").on('input',compute);
  }

  /******** Unit / Price = Total Calculation **************/
  $(".qty").on('input',compute);

  function compute() {
    var tr = $(this).closest("tr");
    //Promo Check 
    var promo_price = $(this).data('promo_unit_price');
    var promo_qty = $(this).data('promo_unit_qty');

    if(promo_price != 0 && promo_qty != 0 ) {
      var qtyVal = tr.find('.qty').val();
      var unitVal = $(this).data('unitprice');

      if(qtyVal >= promo_qty) {
        tr.find(".unit").val(promo_price);
        var unitVal = tr.find(".unit").val();
      }

      else if(qtyVal < promo_qty) {
        tr.find(".unit").val(unitVal);
        var unitVal = tr.find(".unit").val();
      }
    }
    else {
      var qtyVal = tr.find('.qty').val();
      var unitVal = tr.find(".unit").val();
    }

    if(typeof qtyVal == "undefined" || typeof unitVal == "undefined")
      return;

    var subTotal = qtyVal * unitVal;

      tr.find(".totalamt").val(subTotal.toFixed(2));
      fnAlltotal();
  }

  function fnAlltotal() {
    var total = 0;
    $(".totalamt").each(function(){
      total += parseFloat($(this).val()||0)
    });

    $("#totalcost").val(total);
    $("#balance").val(total);


    $("#totcost").val(total.toFixed(2));
    $(".checkoutamt").text(total.toFixed(2));
  }

  function fetch_itms(product_id) {
    $.ajax({
      type: 'POST',
      url: 'prod_ret_ajax',
      data: {productid: product_id},
      success: function(prod_response){
        if(prod_response)
        {
          document.getElementById("ProductDisplay").innerHTML=prod_response;
        }
        else 
        {
          alert("Failed");
        }
      }   
    });
  }

  $(".table").on("click", ".del_req_row", function(){
    $(this).closest('tr').remove();
    fnAlltotal();
  });

  $('.checkout').click(function(){ 
    //$('#checkoutform').submit();
    $('#checkouttable').html($('#tallytable').clone());
    $('#checkoutmodal').modal('show');
  });

  $('#confirmcheckout').click(function(){ 
    $('#checkout_confirmed').submit();
  });

</script>

<!-- Modals -->
<?php
    include_once "pos/pagesettings.php";
    include_once "pos/modals.php";
?>

<script type="text/javascript">
  $(document).on('input', '.cash_received', function(){
    let originalPrice = parseInt($('.checkoutamt').text());
    let cashReceived = parseInt($('.cash_received').val());
    let balanceRemaining = (cashReceived - originalPrice);//.toFixed(2);
    

    $('.changeamt').text(balanceRemaining);
    $('#change').val(balanceRemaining);
    //console.log(balanceRemaining);
  });
</script>