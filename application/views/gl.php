<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <form action="retrieve_record" method="post">
                    <div class="col-md-3">
                        <label class="col-md-12 control-label">User</label>
                        <select class="form-control" name="user" placeholder="--- Select One ---"
                                style="width:100% !important" value="">
                            <option></option>
                           <?php
                           if (!empty($employees)) :
                              foreach ($employees As $emp) :
                                 if ($emp->employee_id == "KAD/SYS/OO1")
                                    continue;

                                 print "<option value='" . $emp->employee_id . "'>" . $emp->fullname . "</option>";
                              endforeach;
                           endif;
                           ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-6 control-label">Account</label>
                            <select class=" form-control" name="account" placeholder="" style="width:100% !important" required readonly>
                                <option value="dailysales"> Daily Sales</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-6 control-label">Duration</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control datepicker" placeholder="Date" name="date"
                                       required style="height:33px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="col-md-6 control-label"></label>
                            <div class="col-md-12">
                                <button type="submit" name="gen_report" class="btn btn-sm btn-warning"
                                        style="margin-top:15px !important;">Generate
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="card-body">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-body no-padding">
                               <?php if (!empty($_SESSION['dailyreport']) /*&& $_SESSION['tabletype'] == "dailysales"*/) :

                                  $dailyreport = $_SESSION['dailyreport'];

                                  unset($_SESSION['dailyreport']);
                                  ?>
                                   <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                                       <thead>

                                       <tr>
                                           <th>ID</th>
                                           <th>Products</th>
                                           <th>Description</th>
                                           <th>Sub Total</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       <?php if (!empty($dailyreport)) : $counter = 1;
                                          foreach ($dailyreport As $report) : ?>
                                              <tr>
                                                  <td><?= $counter ?></td>
                                                 <?php
                                                 $prods = explode('|', $report->prod_id);

                                                 foreach ($prods As $prod) {
                                                    $data = ['prod_id' => $prod];
                                                    $product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes', 'prod_id', $data);

                                                    if (!empty($product)) {
                                                       @$productname .= $product->prod_name . ",";
                                                    }

                                                 }
                                                 ?>

                                                  <td><?php print substr(@$productname, 0, 30);
                                                     unset($productname); ?></td>
                                                  <td><?= $report->description ?></td>
                                                  <td><?= $report->tot_cost ?></td>
                                                  <td>
                                                      <button class='btn btn-success btn-xs salesdetails'
                                                              data-trans="<?= $report->id ?>"><i class='fa fa-lock'></i>
                                                          Details
                                                      </button>
                                                  </td>
                                              </tr>
                                             <?php $counter++; endforeach; endif; ?>
                                       </tbody>

                                   </table>
                                   <div class="col-md-3"></div>
                                   <div class="col-md-3"></div>
                                   <div class="col-md-6">
                                       <p class="pull-left"><b>Total:</b> GHȻ <b class="checkoutamt"
                                                                                 style="font-size: 40px"><?= $_SESSION['report']['total'] ?></b>
                                       </p>
                                   </div>
                               <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>