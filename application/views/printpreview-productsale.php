<head>
    <title>Invoice</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

    <!-- Theme -->
    <!--  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css"> -->

</head>
<body >
  <div class="wrapper">
    <section class="invoice">
      <div class="row">
        <div class="col-xs-12">
          <div class="invoice-col">
            <address style="margin-bottom: 0px;">
              <strong><?= @$_SESSION['company_name'] ?></strong><br>
              <?= @$_SESSION['company_address'] .',<br/> ', @$_SESSION['company_location']?>.<br>
              <strong>Tel:</strong> <?= @$_SESSION['company_tel'] ?><br>
              <strong>Invoice#:</strong> <?=str_pad($printpreview['last_id']+1, 6,"0",STR_PAD_LEFT)?>
            </address>
            ---------------------------------
            <table class="table-xxs" border="1" frame="void" rules="all">
              <thead">
                <tr>
                  <th align="left">Item(s)</th>
                  <th align="center">Qty</th>
                  <th align="center">Unit</th>
                  <th align="center">SubT</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($printpreview)) : $counter = 1; for($a = 0; $a < sizeof($printpreview['prodname']); $a++) : ?>
                <tr>
                  <td><?= $printpreview['prodname'][$a] ?></td>
                  <td align="center"><?= $printpreview['prodqty'][$a] ?></td>
                  <td align="center"><?= $printpreview['produnit'][$a]?></td>
                  <td align="center">
                    <?php
                      $sum_of_subtotals[] = $printpreview['prodqty'][$a] * $printpreview['produnit'][$a];
                      print $printpreview['prodqty'][$a] * $printpreview['produnit'][$a];
                    ?>
                  </td>
                </tr>
                <?php $counter++; endfor; endif; $sum_of_unit_prices = array_sum($sum_of_subtotals); ?>
                </tbody>
            </table>

          </div>
        </div><!-- /.col -->
        ---------------------------------
        <div class="col-xs-12">
          <div class="invoice-col">
            <div class="">
                <table class="" border="0" cellpadding="">
                  <tr>
                    <td><strong>Billed to:</strong> <?=$printpreview['customer']?></td>
                    <td style="padding-left: 7px"><strong>Total: </strong> GHȻ<?= $sum_of_units = round($sum_of_unit_prices,2) ?></td>
                  </tr>
                  <tr>
                    <td><strong>Served by:</strong> <?=$printpreview['sold_by']?></td>
                    <td style="padding-left: 7px"><strong>Cash: </strong> <?=number_format($printpreview['amountreceived'], 2)?><td>
                  </tr>
                  <tr>
                    <td><strong>Date:</strong> <?= gmdate("d M, Y")?></td>
                    <td style="padding-left: 7px"><strong>Change: </strong> <?=number_format($printpreview['change'], 2)?><td>
                  </tr>
                  <tr>
                    <td colspan=2 height="10px">-----------------------------</td>
                  </tr>
                  <tr>
                    <td>Rate : <?=$_SESSION['settings']['tax']?>%</td>
                    <td>VAT: <?=number_format(($_SESSION['settings']['tax'] / 100) * $sum_of_units, 2)?><td>
                  </tr>
                </table>
                <p>
                  
                </p>
              </div>
          </div>
        </div><!-- /.col -->
            
            <!-- /.col -->
            <!-- accepted payments column -->
            <div class="row">
              
                    
            </div><!-- /.col -->
            <div class="">
                <div class="">
                    <table class="">
                    </table>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- ./wrapper -->
<!-- AdminLTE App -->
<script type="text/javascript">
    window.print();
    var delay = 100;
    var url = '<?=base_url()?>dashboard/pos'
    setTimeout(function(){ window.location = url; }, delay);
</script>

</body>
