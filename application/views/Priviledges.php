<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          User Priviledges
        </div>
        <div class="card-body">
          <form action="Assign_Roles" method="post">
              <div class="row">
            <div class="col-md-3">
              <div class="col-md-12">
                <select class="select2" name="username" data-placeholder="--- Select One ---" onchange="fetch_roles(this.value)">
                  <option></option>
                  <?php
                    if(!empty($allusers)) :
                      
                      foreach($allusers As $user) :
                        #System Admin Restriction
                        if(($user->Employee_id == "KAD/SYS/OO1") && ($_SESSION['username'] != "sysadmin"))
                          continue;
                  ?>
                  <option value="<?= base64_encode($user->id) ?>"><?= $user->Username ?></option>
                  <?php
                      endforeach;
                    endif;
                  ?>
                </select>
              </div>
              <div class="form-footer"></div>
            </div>
          
              <div class="col-md-9">
                <div class="col-md-4">
                <div class="checkbox" >
                  <input type="checkbox" id="role_statistics" name="roles[]" value="STATISTICS" >
                  <label for="role_statistics">
                      Statistics
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_pos" name="roles[]" value="POS">
                  <label for="role_pos">
                      Point Of Sale
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_sup_cust" name="roles[]" value="SUPCUST">
                  <label for="role_sup_cust">
                      Suppliers & Customers 
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_prod" name="roles[]" value="PROD">
                  <label for="role_prod">
                      Products & Management
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_cash" name="roles[]" value="CASH">
                  <label for="role_cash">
                      Cash Management
                  </label>
                </div>
                </div>
                <div class="col-md-4">
                <div class="checkbox">
                  <input type="checkbox" id="role_user" name="roles[]" value="USERS">
                  <label for="role_user">
                      User Management
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_priv" name="roles[]" value="ROLES">
                  <label for="role_priv">
                      Roles / Priviledges
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_rep" name="roles[]" value="REPORT">
                  <label for="role_rep">
                      Report / Audit
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_order" name="roles[]" value="ORDER">
                  <label for="role_order">
                      Order
                  </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" id="role_set" name="roles[]" value="SETTINGS">
                  <label for="role_set">
                      System Settings
                  </label>
                </div>
                </div>
                <div class="col-md-4">
                 
                 <div class="checkbox">
                  <input type="checkbox" id="role_iledger" name="roles[]" value="INV">
                  <label for="role_iledger">
                     Inventory Ledger
                  </label>
                </div> 
                 <div class="checkbox">
                  <input type="checkbox" id="role_gledger" name="roles[]" value="GNL">
                  <label for="role_gledger">
                     General Ledger
                  </label>
                </div> 
                </div>
                <div class="form-footer">
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                    <button type="submit" name="assign" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
           </div> 
          </form>
        </div>
      </div>
    </div>
</div>