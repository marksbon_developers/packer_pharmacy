<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller 
{
	/************************************ Constructor / Logout ******************************/
		/*******************************
			Constructor 
		*******************************/
		public function index() { redirect('Dashboard'); }
	/***************************************	Interfaces 	***********************************/
    /***************************
			Sales Ledger
		***************************/
		public function salseledger() 
		{
			if(isset($_SESSION['username']))
			{
				if(TRUE)//in_array('New Stock',$_SESSION['rows_exploded'])) 
				{
					# Loading models...
          //$this->load->model('UserModel'); /*Needed By header, nav*/
          //$this->load->model('Retrieval');

           # Loading Functions
           //$data['suppliers_info'] = $this->Retrieval->All_Info('suppliers');

           /********** Interface ***********************/    
						$headertag['title'] = "Sales Ledger";
						$this->load->view('headtag',$headertag);
						$this->load->view('salseledger',@$data);
						$this->load->view('footer');
					/********** Interface ***********************/
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('Dashboard');
				}
			}
			else
			redirect('Access/Login'); 
		}
		   /***************************
			inventory Report
		***************************/
		public function inventoryreport() 
		{
			if(isset($_SESSION['username']))
			{
				if(TRUE)//in_array('New Stock',$_SESSION['rows_exploded'])) 
				{
					# Loading models...
          //$this->load->model('UserModel'); /*Needed By header, nav*/
          //$this->load->model('Retrieval');

           # Loading Functions
           //$data['suppliers_info'] = $this->Retrieval->All_Info('suppliers');

           /********** Interface ***********************/    
						$headertag['title'] = "PayRoll";
						$this->load->view('headtag',$headertag);
						$this->load->view('inventoryreport',@$data);
						$this->load->view('footer');
					/********** Interface ***********************/
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('Dashboard');
				}
			}
			else
			redirect('Access/Login'); 
		}
		   /***************************
			inventory Report
		***************************/
		public function paymentreport() 
		{
			if(isset($_SESSION['username']))
			{
				if(TRUE)//in_array('New Stock',$_SESSION['rows_exploded'])) 
				{
					# Loading models...
          //$this->load->model('UserModel'); /*Needed By header, nav*/
          //$this->load->model('Retrieval');

           # Loading Functions
           //$data['suppliers_info'] = $this->Retrieval->All_Info('suppliers');

           /********** Interface ***********************/    
						$headertag['title'] = "PayRoll";
						$this->load->view('headtag',$headertag);
						$this->load->view('paymentreport',@$data);
						$this->load->view('footer');
					/********** Interface ***********************/
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('Dashboard');
				}
			}
			else
			redirect('access/login'); 
		}
		/***************************
			
		***************************/
		public function General_Report() 
		{
			if(isset($_SESSION['username']))
			{
				if(in_array('GNL',$_SESSION['rows_exploded'])) 
				{
					# Loading models...
          $this->load->model('Universal_Retrieval');

           # Loading Functions
           $data['employees'] = $this->Universal_Retrieval->All_Info('hr_emp_pers_info');
           $data['accounts'] = $this->Universal_Retrieval->All_Info('account_types');

           /********** Interface ***********************/    
						$headertag['title'] = "General Report";
						$this->load->view('headtag',$headertag);
						$this->load->view('gl',$data);
						$this->load->view('footer');
					/********** Interface ***********************/
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('Dashboard');
				}
			}
			else
			redirect('Access/Login'); 
		}
		/***************************
			Daily Report
		***************************/
		public function dailyreport() 
		{
			if(isset($_SESSION['username']))
			{
				if(TRUE)//in_array('New Stock',$_SESSION['rows_exploded'])) 
				{
					# Loading Model
				$this->load->model('Universal_Retrieval');

				$whereCondition = [ 'employee_id' => $_SESSION['employee_id'] ];

				$likecondition = [ 'date_withdrawn' => gmdate('Y-m-d') ];
				
				$data['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact',$whereCondition,$likecondition);

				if(!empty($data['dailyreport']))
				{
					foreach ($data['dailyreport'] as $value) 
					{
						@$data['totalSales'] += $value->tot_cost;
					}
				}

           /********** Interface ***********************/    
						$headertag['title'] = "Report";
						$this->load->view('dailyreport',@$data);
						$this->load->view('footer');
					/********** Interface ***********************/
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('Dashboard');
				}
			}
			else
			redirect('Access/Login'); 
	}

	/***************************
			Daily Report
	***************************/
		public function retrieve_record() 
		{
			if(isset($_SESSION['username']))
			{
				if(in_array('REPORT',$_SESSION['rows_exploded'])) 
				{
					# Loading Model
					$this->form_validation->set_rules('user','Employee','trim');
					$this->form_validation->set_rules('account','Account Type','trim');
					$this->form_validation->set_rules('date','Date','required|trim');

		      if ($this->form_validation->run() === FALSE) 
		      {
		        $this->session->set_flashdata('error',validation_errors());
						redirect($this->input->post('resulturl'));
		      }
		      else 
		      { 
						$this->load->model('Universal_Retrieval');

						# Defining Variables
						$whereCondition = $dbname = ($this->input->post('user')) ? array('employee_id' => $this->input->post('user')) : array();
						$daterange = explode("To", $this->input->post('date'));
						$daterange[0] = date('Y-m-d',strtotime($daterange[0]));
						$daterange[1] = date('Y-m-d',strtotime($daterange[1]));
						$new_daterange = strcasecmp($daterange[0], $daterange[1]);

						$tablename = ($this->input->post('account') == "dailysales") ? "vw_product_transact" : "accounts_transactions";
						$date_field = ($tablename == "vw_product_transact") ? "date_withdrawn" : "date_created";

						if($new_daterange < 0){
							$whereCondition["DATE($date_field) >="] = $daterange[0];
							$whereCondition["DATE($date_field) <="] = $daterange[1];
						}
						if($new_daterange == 0){
							$whereCondition["DATE($date_field) ="] = $daterange[0];
						}

						if($tablename == "accounts_transactions") 
							$whereCondition['acc_type_id'] = $this->input->post('account');
						else
							$whereCondition['status'] = "active";

						$_SESSION['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where($tablename,$whereCondition);
						if($_SESSION['dailyreport']) {
							foreach ($_SESSION['dailyreport'] as $key => $value) {
								# code...
								$total_sales[] = @$value->tot_cost;
							}
							$_SESSION['report']['total'] = number_format(array_sum($total_sales),2);
						}
						//print "<pre>"; print_r($whereCondition); print "</pre>"; exit;
						redirect('report/general_report');
	          
					}
				}
				else
				{
					$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
					redirect('dashboard');
				}
			}
			else
			redirect('access/login'); 
		}



 }//End of Class
