<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Universal_Retrieval extends CI_Model 
{
	/*******************************
		Constructor 
	*******************************/
	public function index() 
  {
			#Redirect to Dashboard Where Roles Are Defined
			redirect('Access');
	}
    
  ############################### Generating ID ################################
    /***********************************************
			Generating New ID
    ************************************************/
    public function LastID($TableName,$FieldName) 
    {
      $this->db->select($FieldName);

      $query = $this->db->get($TableName);
        
      return ( ($query->num_rows() > 0) ? $query->row(0) : FALSE );
	  }
  ############################### Generating ID ################################
    
  ############################### All Data Info ################################
  	public function All_Info_Row($tablename) 
      {
          $query = $this->db->get($tablename);
              
          return ( ($query->num_rows() > 0) ? $query->row() :false );
  	}

    public function All_Info($tablename) 
    {
      $query = $this->db->get($tablename);
              
      return ( ($query->num_rows() > 0) ? $query->result() :false );
    }

    public function All_Info_API($tablename) 
    {
      $query = $this->db->get($tablename);
              
      return ( ($query->num_rows() > 0) ? json_encode($query->result()) :false );
    }
  ############################### All Data Info ################################
    
    ############################### Retrieval Table Fields ########################
    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    
    public function ret_table_fields($tablename) 
    { 
        $result = $this->db->list_fields($tablename); 
        
        return(($result) ? $result : false);  
    }
    
    ############################### Retrieval With Single Condition ########################
    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    public function ret_data_with_s_cond($tablename,$IdField,$data) 
    { 
      $tablename = $tablename;
        
      $this->db->where($IdField,$data[$IdField]);
        
      $query = $this->db->get_where($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->result() :false );
    }

    public function ret_data_with_s_cond_row($tablename,$IdField,$data,$dbres=null) 
    { 
      $tablename = $tablename;

      if(!empty($dbres)) {
        $dbres = $this->load->database($dbres,true);
        $dbres->where($IdField,$data[$IdField]);
        $query = $dbres->get_where($tablename); 
      }
      else {
        $this->db->where($IdField,$data[$IdField]);
        $query = $this->db->get_where($tablename); 
      }
        
      return ( ($query->num_rows() > 0) ? $query->row() :false );
    }

    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    public function ret_data_with_s_cond_boolean($tablename,$IdField,$data) 
    { 
      $tablename = $tablename;
        
      $this->db->where($IdField,$data[$IdField]);
        
      $query = $this->db->get_where($tablename); 
        
      return ( ($query->num_rows() > 0) ? TRUE : FALSE );
    }

    /***********************************************
            Retreieve Table Data 
    ************************************************/
    public function ret_data_with_s_cond_where($tablename,$whereCondition) 
    { 
      $tablename = $tablename;

      $this->db->where($whereCondition);
      //print $this->db->get_compiled_select($tablename);
      $query = $this->db->get($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->result() : FALSE );
    }

    /***********************************************
            Retreieve Table Data 
    ************************************************/
    public function ret_data_with_s_cond_where_like($tablename,$whereCondition,$likecondition) 
    { 
      $tablename = $tablename;

      $this->db->where($whereCondition);
        
      $this->db->like($likecondition);
//print $this->db->get_compiled_select($tablename);
      $query = $this->db->get($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->result() : FALSE );
    }

    /***********************************************
      Retreieve Table Fields 
    ************************************************/
    public function last_transaction_id() 
    { 
      $tablename = "product_transact";
      $this->db->select("MAX(id) as last_id");
      $query = $this->db->get($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->row() :false );
    }












  /***********************************************
      Retreieve Table Fields 
  ************************************************/
  public function retrieve_price_list($description_id) 
  { 
    $dbres = $this->load->database('packer_admin',TRUE);
    $tablename = "full_product_details";
      
    $dbres->where('DescriptionID',$description_id);
      
    $query = $dbres->get_where($tablename); 
      
    return ( ($query->num_rows() > 0) ? $query->result() :false );
  }

  /***********************************************
      Retreieve Table Fields 
  ************************************************/
  public function retrieve_product_info($tablename,$where_condition = array()) 
  { 
    $dbres = $this->load->database('packer_admin',TRUE);
    $tablename = $tablename;
    $dbres->where($where_condition);
    $query = $dbres->get($tablename); 
      
    return ( ($query->num_rows() > 0) ? $query->result() :false );
  }

  /***********************************************
      Retreieve Table Fields 
  ************************************************/
  public function retrieve_tax() 
  { 
    $dbres = $this->load->database('packer_admin',TRUE);
    $tablename = "tax_system";
    /*$dbres->select("percentage,getfund,nhil");*/
    $where_condition = array('status'=>"active");
    $query = $dbres->get_where($tablename,$where_condition); 
      
    return ( ($query->num_rows() > 0) ? $query->row() :false );
  }
    
}//End of class
