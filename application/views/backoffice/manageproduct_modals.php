<!-- Edit Description / Category Modals -->
<div class="modal fade" id='catdescmodal' role='dialog' aria-hidden='true' >
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="update_description" method="post" id="cateditforms">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<h4 class="modal-title">Edit Category</h4>
				</div>
				<div class="modal-body">
					<div class="control-label">
						<label></label>
						<div class="input-group">
							<input type="text" class="form-control" id="oldname" name="new_name" required/>
						</div><!-- /.input group -->
					</div>
					<input type="hidden" id="editid" name="ResetId"/>
				</div>
				<div class="modal-footer">
					<div class="col-md-2"></div>
					<div class="col-md-6">
						<button class="btn btn-success" type="submit" name="editbtn"><i class="fa fa-database"></i> Update</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
					</div>
					<div class="col-md-4"></div>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<!-- Delete Category Modal-->
<div class="modal fade" id='deletemodal' role='dialog' aria-hidden='true' >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_url" action="" method="post">
                <input type="hidden" name="resulturl" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    Do You Want To Really Delete <?php echo "<strong><em id='deletename'></em></strong>"; ?> .... ?
                    <input type="hidden" id="deleteId" name="deleteid" value="" />
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button class="btn btn-danger" type="submit" name="deletebutton"><i class="fa fa-database"></i> Delete</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->