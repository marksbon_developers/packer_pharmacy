<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	/***********************************************
	 * Constructor + Initialization
	 ************************************************/
	public function index()
	{
		if (isset($_SESSION['username'])) {
			$this->load->model('Universal_Retrieval');

			if (in_array('STATISTICS', $_SESSION['rows_exploded']))
				redirect('dashboard/statistics');

			elseif (in_array('POS', $_SESSION['rows_exploded']))
				redirect('dashboard/pos');

			elseif (in_array('REPORT', $_SESSION['rows_exploded']))
				redirect('administration/report');

			elseif (in_array('SUPCUST', $_SESSION['rows_exploded']))
				redirect('backoffice/vendor');

			elseif (in_array('PROD', $_SESSION['rows_exploded']))
				redirect('backoffice/manage_product');

			elseif (in_array('CASH', $_SESSION['rows_exploded']))
				redirect('backoffice/cash_manage');

			elseif (in_array('USERS', $_SESSION['rows_exploded']))
				redirect('administration/users');

			elseif (in_array('ROLES', $_SESSION['rows_exploded']))
				redirect('administration/priviledges');

			elseif (in_array('SETTINGS', $_SESSION['rows_exploded']))
				redirect('report/report');
		} else
			redirect('access');
	}

	/***********************************************
	 * Statistics Dashboard
	 ************************************************/
	public function master()
	{
		if (in_array('STATISTICS', $_SESSION['rows_exploded'])) :
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$all_prod = $this->Universal_Retrieval->All_Info('full_product_details');

			/************* Variable Declarations *************/
			$data = [
			  'ofs' => 0,
			  'availstock' => 0,
			  'totalSales' => 0,
			  'totalCustomers' => 0
			];
			/************* Variable Declarations *************/

			/*************** Out Of Stock Product Retrieve ****************/
			if (!empty($all_prod)) :
				foreach ($all_prod As $prod_det) :
					# Definning All Criticals
					if ($prod_det->Current_Qty <= 0)
						$data['ofs'] += 1;
				endforeach;
			endif;
			/*************** Out Of Stock Product Retrieve ****************/

			/*************** Available Stock Product Retrieve *************/
			if (!empty($all_prod)) :
				foreach ($all_prod As $prod_det) :
					# Definning All Criticals
					if ($prod_det->Current_Qty != 0)
						$data['availstock'] += 1;
				endforeach;
			endif;
			/*************** Available Stock Product Retrieve *************/

			/*************** Daily Total Sales Retrieve *************/
			$whereCondition = ['employee_id' => $_SESSION['employee_id']];

			$likecondition = ['date_withdrawn' => gmdate('Y-m-d')];

			$dailyreportsum = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact', $whereCondition, $likecondition);

			if (!empty($dailyreportsum)) {
				foreach ($dailyreportsum as $value) {
					$data['totalSales'] += $value->tot_cost;
				}
			}
			/*************** Daily Total Sales Retrieve *************/

			/*************** Total Customers Retrieve *************/
			$allcustomers = $this->Universal_Retrieval->All_Info('product_customers');

			if (!empty($allcustomers))
				$data['totalCustomers'] = count($allcustomers);

			/*************** Total Customers Retrieve *************/
			$headertag['title'] = "Dashboard";
			$headertag['totalSales'] = $data['totalSales'];
			$this->load->view('headtag1', $headertag);
			$this->load->view('master', $data);
			$this->load->view('footer');
		/*************** Total Customers Retrieve *************/
		else :
			redirect('dashboard');
		endif;
	}
	/******************************   Interfaces ****************************************/
	/***********************************************
	 * Statistics Dashboard
	 ************************************************/
	public function statistics()
	{
		if (in_array('STATISTICS', $_SESSION['rows_exploded'])) :
			# Loading Model
			$this->load->model('Universal_Retrieval');
			$this->load->model('DashboardModel');

			$all_prod = $this->Universal_Retrieval->All_Info('full_product_details');

			/************* Variable Declarations *************/
			$data = [
			  'ofs' => 0,
			  'availstock' => 0,
			  'totalSales' => 0,
			  'totalCustomers' => 0
			];
			/************* Variable Declarations *************/

			/*************** Out Of Stock Product Retrieve ****************/
			if (!empty($all_prod)) :
				foreach ($all_prod As $prod_det) :
					# Definning All Criticals
					if ($prod_det->Current_Qty <= 0)
						$data['ofs'] += 1;
				endforeach;
			endif;
			/*************** Out Of Stock Product Retrieve ****************/

			/*************** Available Stock Product Retrieve *************/
			if (!empty($all_prod)) :
				foreach ($all_prod As $prod_det) :
					# Definning All Criticals
					if ($prod_det->Current_Qty != 0)
						$data['availstock'] += 1;
				endforeach;
			endif;
			/*************** Available Stock Product Retrieve *************/

			/*************** Daily Total Sales Retrieve *************/
			$whereCondition = ['employee_id' => $_SESSION['employee_id']];
			$likecondition = ['date_withdrawn' => gmdate('Y-m-d')];
			$dailyreportsum = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact', $whereCondition, $likecondition);

			if (!empty($dailyreportsum)) {
				$data['total_transactions'] = sizeof($dailyreportsum);
				foreach ($dailyreportsum as $value) {
					$data['totalSales'] += $value->tot_cost;
				}
			}
			/*************** Daily Total Sales Retrieve *************/

			/*************** Total Customers Retrieve *************/
			$allcustomers = $this->Universal_Retrieval->All_Info('product_customers');
			if (!empty($allcustomers))
				$data['totalCustomers'] = sizeof($allcustomers);

			/*************** Total Customers Retrieve *************/

			/* daily cash sale */
			$data['cashsales'] = (!empty($cashQueryResult = $this->DashboardModel->getDailyPaymentModeTotal($paymentmode = "cash"))) ? $cashQueryResult->total_amount : 0;
			$data['momosales'] = (!empty($cashQueryResult = $this->DashboardModel->getDailyPaymentModeTotal($paymentmode = "momo"))) ? $cashQueryResult->total_amount : 0;


			$headertag['title'] = "Dashboard";
			$headertag['totalSales'] = $data['totalSales'];
			$this->load->view('headtag1', $headertag);
			$this->load->view('dashboard', $data);
			$this->load->view('footer');
		/*************** Total Customers Retrieve *************/

		else :
			redirect('dashboard');

		endif;
	}

	/******************************
	 * Point OF Sale
	 *******************************/
	public function pos()
	{
		# loading register view
		if (isset($_SESSION['username'])) {
			$this->load->model('Universal_Retrieval');

			# Retrieving Data
			$whereCondition = ['status != ' => 'deleted'];
			$data['desc_info'] = $this->Universal_Retrieval->retrieve_product_info('product_desc', $whereCondition);
			$data['allproduct'] = $this->Universal_Retrieval->All_Info('full_product_details');
			$data['accounts'] = $this->Universal_Retrieval->All_Info('account_types');
			$scripts = array();
			//print "<pre>"; print_r($data['desc_info']); print "</pre>"; exit;
			/********** Interface ***********/
			$title['title'] = "Point Of Sale";
			$this->load->view('headtag', $title);
			$this->load->view('sellingpoint', $data);
			$this->load->view('footer', $scripts);
			$this->load->view('dashboard_pagesettings');
			/********** Interface ***********/
		} else {
			redirect('access/login');
		}
	}

	/*******************************
	 * Print View
	 *******************************/
	public function posprint()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('POS', $_SESSION['rows_exploded']) && isset($_POST)) {
				$this->form_validation->set_rules('prodname[]', 'Product Name(s)', 'required|trim');
				$this->form_validation->set_rules('qty[]', 'qunatities', 'required|trim');
				$this->form_validation->set_rules('unit[]', 'Units', 'required|trim');
				$this->form_validation->set_rules('tot_cost', 'Total Cost', 'required|trim');
				$this->form_validation->set_rules('sold_by', 'Sold By', 'required|trim');
				$this->form_validation->set_rules('customer_name', 'Customer', 'required|trim');
				$this->form_validation->set_rules('cash_received', 'Cash Received', 'required|trim');
				$this->form_validation->set_rules('paymentmode', 'Payment Mode', 'required|trim');
				$this->form_validation->set_rules('change', 'Change', 'trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('dashboard/pos');
				} else {
					# Loading models...
					$this->load->model('Universal_Retrieval');
					$this->load->model('Universal_Update');
					$this->load->model('Universal_Insertion');

					# Variable Declaration
					$prodname = $this->input->post('prodname');
					$prodqty = $this->input->post('qty');
					$produnit = $this->input->post('unit');
					$prodtotcost = $this->input->post('tot_cost');

					# Looping Through the variables
					for ($a = 0; $a < sizeof(@$prodname); $a++) {
						$temp = ['prod_name' => $prodname[$a]];
						$prod_id = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes', 'prod_name', $temp, $dbres = "packer_admin");
						@$savedata['prodids'] .= $prod_id->prod_id . "|";

						/************ Reducing From Product Quantity ****************/
						$qty_ret = ['prod_id' => $prod_id->prod_id];
						$ret_res = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_details', 'prod_id', $qty_ret, $dbres = "packer_admin");
						$prod_qty = $ret_res->avail_qty;

						if ($prod_qty < $prodqty[$a]) {
							$this->session->set_flashdata('error', "{$prodname[$a]} - Remaining Quanity : " . $prod_qty);
							redirect('dashboard/pos');
						} else {
							$data = ['avail_qty' => $prod_qty - $prodqty[$a]];
							$where_condition = ['prod_id' => $prod_id->prod_id];
							$qty_red = $this->Universal_Update->Multiple_Update('product_details', $data, $where_condition);
						}
						/************ Reducing From Product Quantity ****************/

						@$savedata['qtys'] .= $prodqty[$a] . "|";
						@$savedata['units'] .= $produnit[$a] . "|";
					}

					# retrieveing last transaction id
					$last_id = $this->Universal_Retrieval->last_transaction_id();

					$savedata['totcost'] = $prodtotcost;
					$savedata['sold_by'] = $this->input->post('sold_by');
					$savedata['customer'] = $this->input->post('customer_name');
					$savedata['cash_received'] = $this->input->post('cash_received');
					$savedata['paymentmode'] = $this->input->post('paymentmode');

					// getting actual unit price
					$unitArray = $this->input->post('unit');
					$qtyArray = $this->input->post('qty');

					for ($i=0; $i < sizeof($unitArray); $i++) {
						$subTotals[] = Dashboard::getSubTotal($unitArray[$i], $qtyArray[$i]);
					}

					/*foreach ($this->input->post('unit') as $value) {
						$subTotals[] = Dashboard::getSubTotal($value);
					}*/

					$printdata['printpreview'] = [
					  'prodname' => $this->input->post('prodname'),
					  'prodqty' => $this->input->post('qty'),
					  'produnit' => $this->input->post('unit'),
					  'subtotals' => $subTotals,
					  'prodtotcost' => $this->input->post('tot_cost'),
					  'sold_by' => $this->input->post('sold_by'),
					  'customer' => $this->input->post('customer_name'),
					  'last_id' => $last_id->last_id,
					  'amountreceived' => $this->input->post('cash_received'),
					  'change' => $this->input->post('change'),
					  'paymentmode' => $this->input->post('paymentmode'),
					];


					$data = [
					  'prod_id' => $savedata['prodids'], 'description' => "customer",
					  'qty_withdrawn' => $savedata['qtys'], 'unit_price' => $savedata['units'],
					  'tot_cost' => $savedata['totcost'], 'tax' => $_SESSION['settings']['tax'],
					  'sold_by' => $savedata['sold_by'], 'customer' => $savedata['customer'],
					  'employee_id' => $_SESSION['employee_id'], 'cashreceived' => $savedata['cash_received'],
					  'paymentmode' => $savedata['paymentmode']
					];

					$result = $this->Universal_Insertion->DataInsert('product_transact', $data);

					if ($result)
						$this->session->set_flashdata('success', "Transaction Successful");
					else
						$this->session->set_flashdata('error', "Transaction Failed");

					// vat + its nonsense calculation
					$graCalculations = Dashboard::GRA_Calautions($prodtotcost);
					$printdata['printpreview'] = array_merge($printdata['printpreview'], $graCalculations);
					//print_r($printdata); exit;
					/*$printdata['formsubmit'] = "
						<form action='".base_url()."dashboard/save_pos_inv' method='post' id='possave'>
							<input type='hidden' name='comb_prod_ids' value='".$savedata['prodids']."' />
							<input type='hidden' name='comb_qtys' value='".$savedata['qtys']." '/>
							<input type='hidden' name='comb_units' value='".$savedata['units']."' />
							<input type='hidden' name='totalcost' value='".$savedata['totcost']."' />
							<input type='hidden' name='sold_by' value='".$savedata['sold_by']."' />
							<input type='hidden' name='customer' value='".$savedata['customer']."' />
						</form> ";*/

					/********** Interface ***********************/
					$headertag['title'] = "Product Vendors";
					$this->load->view('print', $printdata);
					//$this->load->view('footer');
					/********** Interface ***********************/
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('dashboard');
			}
		} else {
			redirect('access/login');
		}
	}

	/***********************************************
	 * Setting View
	 ************************************************/
	public function salesreport()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('POS', $_SESSION['rows_exploded'])) {
				# Loading Model
				$this->load->model('Universal_Retrieval');

				$whereCondition = ['employee_id' => $_SESSION['employee_id']];
				$likecondition = ['date_withdrawn' => gmdate('Y-m-d')];
				$like_condition = ['date_created' => gmdate('Y-m-d')];

				$data['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact', $whereCondition, $likecondition);
				$data['cashreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('accounts_transactions', $whereCondition, $like_condition);

				if (!empty($data['dailyreport'])) {
					foreach ($data['dailyreport'] as $value) {
						@$data['totalSales'] += $value->tot_cost;
					}
				}

				/****** Interface ********************/
				$headertag['title'] = "Dashboard";
				$this->load->view('headtag', $headertag);
				$this->load->view('dreport', $data);
				$this->load->view('footer');
				/****** Interface ********************/
			} else {
				redirect('dashboard');
			}
		} else {
			redirect('access');
		}
	}

	public function customersattach()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('POS', $_SESSION['rows_exploded'])) {
				# Loading Model
				$this->load->model('Universal_Retrieval');

				$whereCondition = ['employee_id' => $_SESSION['employee_id']];

				$likecondition = ['date_withdrawn' => gmdate('Y-m-d')];
				$like_condition = ['date_created' => gmdate('Y-m-d')];

				$data['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact', $whereCondition, $likecondition);
				$data['cashreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('accounts_transactions', $whereCondition, $like_condition);

				if (!empty($data['dailyreport'])) {
					foreach ($data['dailyreport'] as $value) {
						@$data['totalSales'] += $value->tot_cost;
					}
				}

				/****** Interface ********************/
				$headertag['title'] = "Dashboard";
				$this->load->view('headtag', $headertag);
				$this->load->view('customersattach', $data);
				$this->load->view('footer');
				/****** Interface ********************/
			} else {
				redirect('dashboard');
			}
		} else {
			redirect('access');
		}
	}

	/***************************   Data Insertion   *****************************/

	/***********************************
	 * Save Expences
	 ***********************************/
	public function Save_Expenses()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('POS', $_SESSION['rows_exploded']) && isset($_POST['save_expenses'])) {
				$this->form_validation->set_rules('acc_type', 'Account Type', 'required|trim');
				$this->form_validation->set_rules('deb_amt', 'Debit Amount', 'trim');
				$this->form_validation->set_rules('cred_amt', 'Credit Amount', 'trim');
				$this->form_validation->set_rules('purpose', 'Purpose', 'required|trim');
				$this->form_validation->set_rules('resulturl', 'URL', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect($this->input->post('resulturl'));
				} else {
					# Loading Model
					$this->load->model('Universal_Insertion');

					if (empty($this->input->post('deb_amt')) && empty($this->input->post('cred_amt'))) {
						$this->session->set_flashdata('error', "Please Enter Debit OR Credit Amount");
						redirect($this->input->post('resulturl'));
					} else {
						$data = [
						  'acc_type_id' => base64_decode($this->input->post('acc_type')),
						  'debit_amt' => (int)$this->input->post('deb_amt'),
						  'credit_amt' => (int)$this->input->post('cred_amt'),
						  'balance' => ((int)(int)$this->input->post('cred_amt') - (int)$this->input->post('deb_amt')),
						  'purpose' => $this->input->post('purpose'),
						  'employee_id' => $_SESSION['employee_id']
						];

						$result = $this->Universal_Insertion->DataInsert('accounts_transactions', $data);

						if ($result)
							$this->session->set_flashdata('success', "Save Successful");

						else
							$this->session->set_flashdata('error', "Save Failed");

						redirect($this->input->post('resulturl'));
					}
				}
			} else {
				$this->session->set_flashdata('error', 'Permission Denied. Contact Administrator');
			}
		} else {
			redirect('access/login');
		}
	}


	/*********************************   Data Update   ****************************/

	/**********************************************************************************************************************************************************
	 ********************************************************   Data Retrieval   *****************************************************************************
	 **********************************************************************************************************************************************************/

	/***********************************************
	 * ATMs
	 ************************************************/

	/***********************************************
	 * AJAX Request to Fetch PRoducts
	 ************************************************/
	public function prod_ret_ajax()
	{
		$this->form_validation->set_rules('productid', 'Product ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "Product Retrieve Failed";
		} else {

			# Loading Model
			$this->load->model('Universal_Retrieval');

			$data = ['ProductCode' => base64_decode($this->input->post('productid'))];

			$result = $this->Universal_Retrieval->ret_data_with_s_cond('full_product_details', 'ProductCode', $data);

			if (!empty($result)) {
				foreach ($result as $product) {
					// retrieving description details
					if ($product->Promo_Price && $product->Promo_Qty) {
						$promo_msg = "<span class='label label-warning'><span class='sign'>" . @$product->Promo_Qty . "&nbsp OR More, </span><b></b>GHȻ " . $product->Promo_Price . " Each</span>";
					}
					print "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12' style='padding-bottom:10px;'>
            	<a class='card card-banner card-green-light' onclick='addtotally(this)' data-prodname='$product->Item_Name' data-prodid='" . $product->Product_ID . "' data-unitprice='" . $product->Unit_Price . "'  data-promounitprice='" . $product->Promo_Price . "' data-promounitqty='" . $product->Promo_Qty . "'>
            	<div class='card-body'>
            	<i class='icon fa ".$product->DescriptionIcon." fa-4x'></i>
            	<div class='content'><div class='title'>" . $product->Item_Name . "</div>
            	" . @$promo_msg . "
            	<div class='value'><b><span class='sign'></span></b>GHȻ " . $product->Unit_Price . "</div></div></div></a></div>";
				}
			}
		}
	}

	/***********************************************
	 * AJAX Request to Fetch PRoducts
	 ************************************************/
	public function save_pos_inv($savedata)
	{
		$this->form_validation->set_rules('comb_prod_ids', 'Combined Product ID', 'required|trim');
		$this->form_validation->set_rules('comb_qtys', 'Combined Quantities', 'required|trim');
		$this->form_validation->set_rules('comb_units', 'Combined Unis', 'required|trim');
		$this->form_validation->set_rules('totalcost', 'Total Cost', 'required|trim');
		$this->form_validation->set_rules('sold_by', 'Sold By', 'required|trim');
		$this->form_validation->set_rules('customer', 'Customer Name', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('dashboard/pos');
		} else {

			# Loading Model
			$this->load->model('Universal_Insertion');

			$data = [
			  'prod_id' => $this->input->post('comb_prod_ids'),
			  'description' => "customer",
			  'qty_withdrawn' => $this->input->post('comb_qtys'),
			  'unit_price' => $this->input->post('comb_units'),
			  'tot_cost' => $this->input->post('totalcost'),
			  'tax' => $_SESSION['settings']['tax'],
			  'sold_by' => $this->input->post('sold_by'),
			  'customer' => $this->input->post('customer'),
			  'employee_id' => $_SESSION['employee_id']
			];

			$result = $this->Universal_Insertion->DataInsert('product_transact', $data);

			if ($result)
				$this->session->set_flashdata('success', "Transaction Successful");

			else
				$this->session->set_flashdata('error', "Transaction Failed");

			redirect('dashboard/pos');

		}
	}

	/***********************************************
	 * AJAX Request to Fetch PRoducts
	 ************************************************/
	public function prod_ret_ajax_cat()
	{
		$this->form_validation->set_rules('desc_id', 'Category ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "Product Retrieve Failed";
		} else {
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$description_id = $this->input->post('desc_id');

			$result = $this->Universal_Retrieval->retrieve_price_list($description_id);

			if (!empty($result)) {
				foreach ($result as $product) {
					if ($product->Promo_Price && $product->Promo_Qty) {
						$promo_msg = "<span class='label label-warning'><span class='sign'>" . @$product->Promo_Qty . "&nbsp OR More, </span><b></b>GHȻ " . $product->Promo_Price . " Each</span>";
					}
					print "<div class='col-lg-4 col-md-6 col-sm-6 col-xs-12' style='padding-bottom:10px;'>
            	<a class='card card-banner card-green-light' onclick='addtotally(this)' data-prodname='$product->Item_Name' data-prodid='" . $product->Product_ID . "' data-unitprice='" . $product->Unit_Price . "' data-promounitprice='" . $product->Promo_Price . "' data-promounitqty='" . $product->Promo_Qty . "'>
            	<div class='card-body'>
            	<i class='icon fa ".$product->DescriptionIcon." fa-4x'></i>
            	<div class='content'><div class='title'>" . $product->Item_Name . "</div>
            	" . @$promo_msg . "
            	<div class='value'><b><span class='sign'></span></b>GHȻ " . $product->Unit_Price . "</div></div></div></a></div>";
				}
			}
		}
	}

	/***********************************************
	 * AJAX Request to Return Row Data
	 ************************************************/
	public function return_row_data()
	{
		$this->form_validation->set_rules('id', 'ID', 'required|trim');
		$this->form_validation->set_rules('tablename', 'tablename', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "/";
		} else {
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$data = ['id' => base64_decode($this->input->post('id'))];
			$tablename = $this->input->post('tablename');

			$result = $this->Universal_Retrieval->ret_data_with_s_cond_row($tablename, 'id', $data);

			if ($result)
				print $result->acc_desc . "/" . $result->acc_ref_no;
		}
	}

	/***********************************************
	 * AJAX Request to Return Row Data
	 ************************************************/
	public function return_daily_salesreport()
	{
		$this->form_validation->set_rules('trans_id', 'Transaction ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "/";
		} else {
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$data = ['id' => $this->input->post('trans_id')];

			$result = $this->Universal_Retrieval->ret_data_with_s_cond('product_transact', 'id', $data);

			if ($result) {
				$counter = 1;

				foreach ($result As $res) {
					$prods = explode('|', $res->prod_id);
					$qty = explode('|', $res->qty_withdrawn);
					$unit = explode('|', $res->unit_price);

					$prods_size = sizeof($prods);

					for ($a = 0; $a < $prods_size; $a++) {
						if (!empty($prods[$a])) :

							/***** Retrieving Product Name ********/
							$data = ['prod_id' => $prods[$a]];

							$product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes', 'prod_id', $data, $dbres = "packer_admin");

							if (!empty($product->prod_name))
								$productname = $product->prod_name;

							else
								$productname = "";
							/***** Retrieving Product Name ********/
							$qty_single = $qty[$a];

							$unit_single = $unit[$a];

							$display = "<tr><td>$counter</td><td>$productname</td><td>$res->description</td><td>$qty_single</td><td>$unit_single</td><td>" . $qty_single * $unit_single . "</td></tr>";

							$counter++;

							print $display;
						endif;
					}
				}
			} else {
			}
		}
	}

	public static function GRA_Calautions($totalCost)
	{
		$initialSubTotal = 1;
		$getfundRate = ($_SESSION['settings']['getfund'] / 100);
		$nhilRate = ($_SESSION['settings']['nhil'] / 100) ;
		$c19Rate = ($_SESSION['settings']['covid_levy'] / 100) ;
		$vatRate = ($_SESSION['settings']['vat'] / 100);
		$totalLevyRate = $getfundRate + $nhilRate + $c19Rate;
		$precision1 = 2;
		$precision2 = 3;

		$taxableAmt = $totalLevyRate + $initialSubTotal;
		$vatAmt = $vatRate * $taxableAmt;
		$total = $taxableAmt + $vatAmt;

		$actualSubTotal = $totalCost / $total;
		$actualTaxableAmt = ($totalLevyRate * $actualSubTotal) + $actualSubTotal;
		$actualVatAmt = $vatRate * $actualTaxableAmt;

		// finding the subtotal
		return $responseData = [
		  'subTotal' => round($actualSubTotal, $precision1),
			'getfundAmt' => round($getfundRate * $actualSubTotal, $precision2),
			'nhilAmt' => round($nhilRate * $actualSubTotal, $precision2),
			'covid19Amt' => round($c19Rate * $actualSubTotal, $precision2),
			'taxableAmt' => round($actualTaxableAmt, $precision1),
			'vat' => round($actualVatAmt, $precision1)
		];
	}

	public static function getSubTotal($unitPrice, $qty = 1)
	{
		$initialSubTotal = 1;
		$getfundRate = ($_SESSION['settings']['getfund'] / 100);
		$nhilRate = ($_SESSION['settings']['nhil'] / 100) ;
		$c19Rate = ($_SESSION['settings']['covid_levy'] / 100) ;
		$vatRate = ($_SESSION['settings']['vat'] / 100);
		$totalLevyRate = $getfundRate + $nhilRate + $c19Rate;
		$precision1 = 2;

		$taxableAmt = $totalLevyRate + $initialSubTotal;
		$vatAmt = $vatRate * $taxableAmt;
		$total = $taxableAmt + $vatAmt;

		$actualSubTotal = $unitPrice / $total;

		// finding the subtotal
		return round($actualSubTotal * $qty, $precision1);
	}


}//End of Class
