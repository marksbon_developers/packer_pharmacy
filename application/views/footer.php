<footer class="app-footer">
  <div class="row">
    <div class="col-xs-12">
      <div class="footer-copyright"><hr></hr>
        <center> Powered by: Marksbon. Copyright ©  <?=date('Y')?>  </center>
      </div>
    </div>
 </div>
</footer>
<script type="text/javascript" src="<?= base_url() ?>resources/js/vendor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/app.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/datepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>resources/js/datepicker/daterangepicker.js"></script>
             
<!--*********** Notification    *********************-->
<script type="text/javascript">
<!-- Date Picker -->
$('.datepicker').daterangepicker({
  "showDropdowns": true,
  "autoApply": true,
  ranges: {
     'Today': [moment(), moment()],
     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
     // i want last 7 days as default
     'Last 7 Days': [moment().subtract(6, 'days'), moment()],
     'Last 30 Days': [moment().subtract(29, 'days'), moment()],
     'This Month': [moment().startOf('month'), moment().endOf('month')],
     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  locale: {
    format: "DD-MM-YYYY",
    separator: "  To  "
  },
  "startDate": "01-01-2018",
  "endDate": "31-01-2018"
}, function(start, end, label) {
console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});

                /******** Click Event For Daily Sales Report *********/
                $('.datatable').on('click','.salesdetails',function() {
                    var transid = $(this).data('trans');
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url() ?>dashboard/return_daily_salesreport',
                        data: {trans_id: transid},
                        success: function(response){
                            if(response)
                            {
                                $('#salesrep_details').html(response);
                                $('#salesrep_modal').modal('show');
                            }
                            else
                                alert("Failed");
                        }
                    });

                });
                /******** Click Event For Daily Sales Report *********/

					  <?php if(!empty($_SESSION['success'])) : ?>
                 /**** Success Notification ****/
                 var permanotice, tooltip, _alert;
                 $(function () {
                     new PNotify({
                         title: 'Process Successful',
                         text: '<?= $this->session->flashdata("success") ?>',
                         type: 'success'
                     });
                 });
                 /**** Success Notification ****/
					  <?php elseif (!empty($_SESSION['error'])) : ?>
                 /**** Error Notification ****/
                 var permanotice, tooltip, _alert;
                 $(function () {
                     new PNotify({
                         title: 'An Error Occurred',
								 <?php $this->session->set_flashdata("error",@$_SESSION[error]); ?>
                         text: '<?= $this->session->flashdata("error") ?>',
                         type: 'error'
                     });
                 });
                 /**** Error Notification ****/
							<?php elseif (validation_errors()) : ?>
                 var permanotice, tooltip, _alert;
                 $(function () {
                     new PNotify({
                         title: 'An Error Occurred',
                         text: '<?= validation_errors() ?>',
                         type: 'error'
                     });
                 });
                 /**** Error Notification ****/
                 /**** Warning Notification ****/
							<?php elseif (!empty($_SESSION['warning'])) : ?>
                 var permanotice, tooltip, _alert;
                 $(function () {
                     new PNotify({
                         title: 'No Record(s) Found',
                         text: '<?= $this->session->flashdata("warning") ?>',
                         type: 'warning'
                     });
                 });
                 /**** Warning Notification ****/
					  <?php endif; ?>
             </script>
             <!-- Warning Notification -->
				 <?php unset($_SESSION['error']);?>
             <!--*********** Notification    *********************-->
				 <?php
				 if(isset($scripts)) :
					  foreach($scripts as $script) :
							print "<script type='text/javascript' src='{$script}'></script>";
					  endforeach;
				 endif;
				 ?>

             <!-- issue Modals -->
             <div class="modal fade" id='salesrep_modal' role='dialog' aria-hidden='true' >
                 <div class="modal-dialog">
                     <div class="modal-content">
                         <form id="" action="" method="post">
                             <input type="hidden" name="resulturl" />
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                 <h4 class="modal-title">Sales Details</h4>
                             </div>
                             <div class="row">
                                 <div class="modal-body">
                                     <table class="table">
                                         <thead>
                                         <tr>
                                             <th>#</th>
                                             <th>Products</th>
                                             <th>Description</th>
                                             <th>Quantity</th>
                                             <th>Unit</th>
                                             <th>Sum</th>
                                         </tr>
                                         </thead>
                                         <tbody id="salesrep_details">

                                         </tbody>
                                     </table>

                                 </div>
                             </div>
                         </form>
                         <div class="modal-footer">
                             <div class="pull-right">

                                 <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                             </div>
                         </div>

                     </div><!-- /.modal-content -->
                 </div><!-- /.modal-dialog -->
             </div><!-- /.modal -->
             <!-- issue Modals -->

         </div>
     </div>
    </body>
</html>