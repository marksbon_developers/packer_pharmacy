<div class="row">
    <div class="col-lg-12">
      <div class="card card-tab">
        <div class="card-header">
          <ul class="nav nav-tabs">
            <li role="tab2" class="active">
              <a href="#newprod" aria-controls="tab2" role="tab" data-toggle="tab">New Product</a>
            </li>
            <li role="tab4">
              <a href="#newdesc" aria-controls="tab4" role="tab" data-toggle="tab">Add Category</a>
            </li>
          </ul>
        </div>
        <div class="card-body no-padding tab-content">
          <div role="tabpanel" class="tab-pane active" id="newprod">
            <div class="row">
              <form action="Register_Single_Product" method="post" enctype="multipart/form-data">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Category</label>
                    <div class="col-md-12">
                      <select class="select2" name="desc_id" id="catdesc" data-placeholder="--- Select One ----" required>
                        <option></option>
                        <?php
                          if(!empty($Description_info)) 
                          {
                            $counter = 1;
                            foreach($Description_info As $desc) 
                            {
                              print "<option value='$desc->desc_id'>$desc->desc</option>";
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-6 control-label">Product Name</label>
                    <div class="col-md-12">
                      <input type="text" class="form-control" placeholder="Enter Here........" name="prod_name" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-6 control-label">Product Code</label>
                    <div class="col-md-12">
                      <input type="text" class="form-control" value="<?= $next_product_code ?>" name="productID" required readonly>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-md-6 control-label">Unit Quantity</label>
                    <div class="col-md-12">
                      <input type="number" min="0" class="form-control" placeholder="1" name="unit_qty" required>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                
                  <div class="form-group">
                    <div class="col-md-6">
                      <label class="control-label">Cost Price(Tax Exclusive)</label>
                      <input type="text" min="0" class="form-control" placeholder="1" name="cost_price" step="0.01">
                    </div>
                    <div class="col-md-6">
                      <label class="control-label">Selling Price(Tax Inclusive)</label>
                      <input type="number" min="0" class="form-control" placeholder="1" name="unit_price" step="0.01" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-6">
                      <label class="col-md-12 control-label" style="padding-left:0px">Promo Quantity</label>
                      <input type="number" min="0" step="0.01" class="form-control" placeholder="promo limit" name="promo_qty" >
                    </div>
                    <div class="col-md-6">
                      <label class="col-md-12 control-label" style="padding-left:0px">Promo Price</label>
                      <input type="number" step="0.01"  class="form-control" placeholder="promo Price" name="promo_price">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12 control-label">Available Stock Quantity</label>
                    <div class="col-md-12">
                      <input type="number" min="1" class="form-control" placeholder="Quantity" name="cur_qty" required>
                    </div>
                  </div> 
                    <!-- <div class="form-group">
                    <label class="col-md-12 control-label">File Upload</label>
                    <div class="col-md-12">
                      <input type="file" class="form-control" placeholder="doc.,pdf," name="icon" >
                    </div>
                  </div>  -->
                </div>
                <div class="col-md-12">
                  <div class="form-footer">
                    <div class="form-group">
                      <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" name="single_add">Save</button>
                        <button type="button" class="btn btn-danger">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="newcat">
            <div class="row">
              <div class="col-md-5">
                <form action="Register_Category" method="post">
                  <div class="form-group">
                    <label class="col-md-12 control-label">Category Name</label>
                      <input type="text" class="form-control" placeholder="Enter Here ......" name="category" style="width:100% !important;">
                  </div>
                  <div class="form-footer">
                    <div class="form-group">
                      <div class="col-md-9 col-md-offset-3">
                        <button type="submit" name="cat_submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger">Cancel</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-md-7">
                <div class="card">
                  <div class="card-header"></div>
                  <div class="card-body no-padding">
                    <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th style="width:1%"># ID</th>
                          <th style="width:69%">Name</th>
                          <th style="width:30%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          # Displaying All Category Info
                          if(!empty($category_info)) {
                            #
                            $counter = 1;
                            foreach($category_info As $cat) {
                        ?>
                        <tr>
                          <td><?= $counter; ?></td>
                          <td><?= $cat->cat_name; ?></td> 
                          <td>
                            <a title="Edit" class="btn btn-success btn-xs editcat" data-toggle="modal" data-editname="<?= $cat->cat_name; ?>" data-editid="<?= base64_encode($cat->cat_id); ?>" data-url="Update_Category">
                              <i class="fa fa-pencil"></i> Edit
                            </a>
                            <a title="Delete" class="btn btn-danger btn-xs deletebtn" data-toggle="modal" data-delname="<?= $cat->cat_name; ?>" data-delid="<?= base64_encode($cat->cat_id); ?>" data-formurl="Delete_Category">
                              <i class="fa fa-trash"></i> Delete
                            </a>
                          </td> 
                        </tr> 
                        <?php
                          #
                              $counter++; }
                            }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="newdesc">
            <div class="row">
              <div class="col-md-5">
                <form action="register_description" method="post">
                  <div class="form-group">
                    <label class="col-md-4 control-label">Category</label>
                    <div class="col-md-12">
                      <input type="text" class="form-control" placeholder="Enter Here......" name="description">
                    </div>
                  </div>
                  <div class="form-footer">
                    <div class="form-group">
                      <div class="col-md-9 col-md-offset-3">
                        <button type="submit" name="desc_submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger">Cancel</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-md-7">
                <div class="card">
                  <div class="card-header"></div>
                  <div class="card-body no-padding">
                    <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th style="width:1%"># ID</th>
                          <th style="width:35%">Name</th>
                          <th style="width:30%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          # Displaying All Category Info
                          if(!empty($Description_info)) {
                            #
                            $counter = 1;
                            foreach($Description_info As $desc) {
                        ?>
                        <tr>
                          <td><?= $counter; ?></td>
                          <td><?= $desc->desc; ?></td>  
                          <td>
                            <a title="Edit" class="btn btn-success btn-xs editcat" data-toggle="modal" data-editname="<?= $desc->desc; ?>" data-editid="<?= base64_encode($desc->desc_id); ?>" data-url="Update_Description">
                              <i class="fa fa-pencil"></i> Edit
                            </a>
                            <a title="Delete" class="btn btn-danger btn-xs deletebtn" data-toggle="modal" data-delname="<?= $desc->desc; ?>" data-delid="<?= base64_encode($desc->desc_id); ?>" data-formurl="Update_Description"
                              <i class="fa fa-trash"></i> Delete
                            </a>
                          </td> 
                        </tr> 
                        <?php
                          #
                              $counter++; }
                            }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>