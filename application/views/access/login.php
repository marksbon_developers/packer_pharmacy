<!DOCTYPE html>
<html>
<head>
  <title>Login ||Packer </title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>resources/images/favicon.png">
  <!-- Theme -->
  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">

</head>
<body>
  <div class="app app-default">

<div class="app-container app-login">
  <div class="flex-center">
    <div class="app-header"></div>
    <div class="app-body">
      <div class="loader-container text-center">
          <div class="icon">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
              </div>
            </div>
          <div class="title">Logging in...</div>
      </div>
      <div class="app-block">
      <div class="app-form">
        <div class="form-header">
          <div class="app-brand"><span class="highlight">PACK</span>ER</div>
        </div>
        <div class="app-brand">
          <?php 

            if($this->session->flashdata('error'))
              print "<h3 class='label label-warning' style='color:red'>".@$this->session->flashdata('error')."</h3>";

            elseif($this->session->flashdata('attemptleft') > 0)
              print "<h3 class='label label-warning' style='color:red'>Invalid Login Credentials. <b>Login Attempt Left: ".@$this->session->flashdata('attemptleft').".</b></h3>";
            
            else
              print "<h3 class='label label-warning' style='color:red'>Authorized Personnels Only</h3>";

          ?>
          </div>  
        <form method="post" action="<?=base_url()?>access/login_validation">
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">
                <i class="fa fa-user" aria-hidden="true"></i></span>
              <input type="text" class="form-control" placeholder="Username" name="username" aria-describedby="basic-addon1" required>
            </div>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon2">
                <i class="fa fa-key" aria-hidden="true"></i></span>
              <input type="password" class="form-control" placeholder="Password" name="passwd" aria-describedby="basic-addon2" required>
            </div>
            <div class="text-center">
                <input type="submit" class="btn btn-success btn-submit" name="login" value="Login">
            </div>
        </form>

      
      </div>
      </div>
    </div>
    <div class="app-footer">
    </div>
  </div>
</div>

  </div>
  
  <script type="text/javascript" src="<?= base_url() ?>resources/js/vendor.js"></script>
  

</body>
</html>