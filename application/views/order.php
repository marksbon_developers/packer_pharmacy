<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active"><a href="#fresh" aria-controls="home" role="tab" data-toggle="tab">New Purchase Order</a></li>
            
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel " class="tab-pane active" id="fresh">
                <form action="<?= base_url().'backoffice/invoiceprint'?>" method="post">
                  <!--Column left -->
                  <div  class="col-xs-4">
                    <div class="control-label">
                      <label> Date </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" name="date_created" value="<?= gmdate('l d M, Y')?>" required readonly/>
                      </div><!-- /.input group -->
                    </div>
                    <div style="margin-top:5px;">
                      <label>Prepared By </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control"  placeholder="Kwame Mintah" name="prepared_by" value="<?= $_SESSION['fullname']?>" required readonly/>
                      </div><!-- /.input group -->
                    </div>
                  </div>
                  <!-- ./ Column Left -->
                  <!--Column middle -->
                  <div  class="col-xs-4">
                    <div>
                      <label>Supplier </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-truck"></i>
                        </div>
                        <select class="form-control usrselect" name="sup_id" data-placeholder="Select Supplier" readonly="">
                           <option label="---- Select One ----"></option>
                           <?php
                              if(!empty($suppliers_info)) {
                                foreach ($suppliers_info As $sup) {
                           ?>
                           <option value="<?= base64_encode($sup->sup_id); ?>"><?= $sup->name; ?></option>
                           <?php
                                       
                                }
                              }
                           ?>
                        </select>
                      </div><!-- /.input group -->
                    </div>
                  </div>
                  <!-- ./Column middle -->
                  <!--Column right -->
                  <div  class="col-xs-4">
                    <div>
                      <label>Purchase Order No. </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <strong>#</strong>
                        </div>
                        <input type="text" class="form-control"  placeholder="GF009856" name="inv_no" required value="<?= $next_order_id ?>" readonly />
                      </div><!-- /.input group -->
                    </div>
                    <div style="margin-top:5px;">
                       <label>Total Cost</label>
                       <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-money"></i>
                          </div>
                          <input id="totalcost" type="number" class="form-control"  placeholder="5000" min="1" name="tot_cost" required readonly/>
                      </div><!-- /.input group -->
                    </div>
                  </div>
                  <div class="col-md-12" >
                    <table class="table table-striped table-hover" id="purchase_list">
                      <thead style="background-color:#29c75f;color:white">
                        <tr>
                          <th>ID</th>
                          <th>Product Name</th>
                          <th>Quantity</th>
                          <th>Cost Price</th>
                          <th>Sub Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          if(!empty($_SESSION['products'])) :
                            $counter = 1;
                            foreach($_SESSION['products'] As $prod) :
                                $exploded = explode('-', base64_decode($prod));
                        ?>
                        <tr>
                          <td><?= $counter ?>.</td>
                          <td><input type="hidden" name="productname[]" value="<?=$exploded[0]?>" /><?=$exploded[0]?></td>
                          <td><input class="qty" type="text" name="qty[]" style="border:0px;padding-left:10px" placeholder="Enter Quantity" oninput="compute(this)" /></td>
                          <td><input class="unit" type="text" name="price[]" style="border:0px;padding-left:10px" placeholder="Cost Price" value="<?=$exploded[1]?>" readonly/></td>
                          <td><input class="totalamt" type="text" name="total[]" readonly style="border:0px;padding-left:10px"/></td>
                        </tr>
                        <?php
                              $counter++;
                            endforeach;
                          endif;
                        ?>
                      </tbody>
                    </table> 
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-md-4 control-label">Remark</label>
                      <div class="col-md-12">
                        <textarea rows="6" style="resize:none;width:100%" name="remarks"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-md-4 control-label">Paid</label>
                      <div class="col-md-12">
                        <input id="pay" type="text" class="form-control" placeholder="" name="pay" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-4 control-label">Balance</label>
                      <div class="col-md-12">
                        <input id="balance" type="text" class="form-control" placeholder="" name="bal" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12" >
                    <center>
                      <div class="form-footer">
                        <div class="form-group">
                          <div class="col-md-9 col-md-offset-3" style="float:right;">
                            <button type="submit" class="btn btn-success" style="text-decoration:none;" target="_blink" ><i class="fa fa-print"></i> Print</button>
                            <a href="<?= base_url()."backoffice/manage_product" ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                          </div>
                        </div>
                      </div>
                    </center>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>