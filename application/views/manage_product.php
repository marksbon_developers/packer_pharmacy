  <div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#inventory" data-toggle="tab">All Stock</a></li>
                <li><a href="#avail" data-toggle="tab">Least Available</a></li>
                 <li><a href="#update" data-toggle="tab">Stock Update</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="inventory">
                  <div class="card">
                      <div class="card-body no-padding">
                        <form action="Create_Purchase_Order" method="post">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th> Id # </th>
                              <th> Item Name </th>
                              <th> Category </th>
                              <th> Quantity </th>
                              <th> Unit </th>
                              <th> Price </th>
                              <th> Remarks </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                              if(!empty($all_prod)) 
                              {
                                $counter = 1;
                                foreach ($all_prod As $prod_det) 
                                {
                                  
                                  # Definning All Criticals
                                  if( $prod_det->Current_Qty <= 0 )
                                    $remarks  = "<span class='label label-danger'> Out Of Stock </span>";
                                  
                                  # Definning All Warning
                                  elseif( $prod_det->Current_Qty > 1 && $prod_det->Current_Qty < 20 )
                                    $remarks  = "<span class='label label-warning'> Low Stock </span>";
                              ?>
                                  <tr style="<?= @$style; ?>">
                                    <td><?= $counter; ?></td>
                                    <td><?= $prod_det->Item_Name; ?></td>
                                    <td><?= $prod_det->Description; ?></td> 
                                    <td><?= $prod_det->Current_Qty; ?></td> 
                                    <td><?= $prod_det->Unit_Qty; ?></td> 
                                    <td>GHȻ 
                                      <?= $prod_det->Unit_Price; ?></td> 
                                    <td>
                                        <strong><em><?= @$remarks; ?></em></strong>
                                    </td>
                                    <td>
                                        <!--<input type="checkbox" name="pur_ord[]" value="<?/*= base64_encode($prod_det->Item_Name) */?>" />-->
                                      <a title="Delete" class="btn btn-primary btn-xs editproduct" onclick=" return editproduct(this)" data-productid="<?= base64_encode($prod_det->Product_ID) ?>" data-prodcode="<?= $prod_det->ProductCode  ?>" data-prodname="<?= $prod_det->Item_Name ?>" data-descid="<?= base64_encode($prod_det->DescriptionID) ?>" data-proddesc="<?= $prod_det->Description ?>" data-produnitqty="<?= $prod_det->Unit_Qty ?>" data-produnitprice="<?= $prod_det->Unit_Price ?>" data-prodcostprice="<?= @$prod_det->Cost_Price ?>" data-prodcurqty="<?= $prod_det->Current_Qty ?>" data-promoqty="<?= $prod_det->Promo_Qty ?>" data-promoprice="<?= $prod_det->Promo_Price ?>">
                                        <i class="fa fa-pencil"></i> Edit
                                      </a>
                                      <?php if(!empty($prod_det->Promo_Qty) && !empty($prod_det->Promo_Price)) : ?>
                                        <a href="#" class="btn btn-warning btn-xs prod-details-mod" data-productid="<?= $prod_det->Product_ID ?>" data-prodname="<?= $prod_det->Item_Name ?>" data-proddesc="<?= $prod_det->Description ?>" data-produnitqty="<?= $prod_det->Unit_Qty ?>" data-produnitprice="<?= $prod_det->Unit_Price ?>" data-prodcurrqty="<?= $prod_det->Current_Qty ?>">
                                          <i class="fa fa-th"></i> Promo
                                        </a>
                                      <?php endif; ?>
                                      
                                    </td>
                                   </tr> 
                            <?php
                                    unset($style);
                                    unset($remarks);
                                    $counter++;
                                  }
                                }
                            ?>
                          </tbody>
                        </table>
                           <!-- <div class="form-footer">
                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3" style="float:right !important;">
                          <button type="submit" class="btn btn-warning" name="">Create purchase Order</button>
                        </div>
                      </div>
                    </div> -->
                    </form>
                      </div>
                    </div>
                </div>

                  <div class="tab-pane" id="avail">
                  <form action="create_purchase_order" method="post">
                  <table id="example4" class="datatable table table-striped primary">
                      <thead>
                        <th style="width:7%"> Id # </th>
                        <th style="width:20%"> Item Name </th>
                        <th style="width:15%"> Category </th>
                        <th style="width:15%"> Avail. Stock </th>
                        <th style="width:12%"> Remarks </th>
                        <th style="width:20%"> Action </th>
                      </thead>
                      <tbody>
                        <?php
                          if(!empty($all_prod)) 
                          {
                            $counter = 1;
                            foreach ($all_prod As $prod_det) 
                            {
                              # Definning All Criticals
                              if( $prod_det->Current_Qty <= 0 ) 
                                $remarks  = "<span class='label label-danger'> Out Of Stock </span>";
                              
                              elseif( $prod_det->Current_Qty > 1 && $prod_det->Current_Qty < 20 )
                                $remarks  = "<span class='label label-warning'> Low Stock</span>";

                              else
                                continue;
                        ?>
                          <tr>
                              <td><?= $counter; ?></td>
                              <td><?= $prod_det->Item_Name; ?></td> 
                              <td><?= $prod_det->Description; ?></td>
                              <td><?= $prod_det->Current_Qty; ?></td>
                              <td>
                                  <strong><em><?= @$remarks; ?></em></strong>
                              </td>
                              <td>
                                <input type="checkbox" name="pur_ord[]" value="<?= base64_encode($prod_det->Item_Name."-".$prod_det->Cost_Price) ?>" />
                              </td>
                          </tr> 
                          <?php
                                  # Resetting Variables
                                  unset($remarks);
                                  $counter++;
                                  }
                              }
                          ?>
                      </tbody>
                  </table>
                  <div class="col-md-12">
                    <div class="form-footer">
                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3" style="float:right !important;">
                          <button type="submit" class="btn btn-warning" name="">Create purchase Order</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  </form>
                </div>

                  <div class="tab-pane" id="update">
                    <form action="<?= base_url() ?>backoffice/save_stock" method="post">
                    <!--Column left --
                     <div  class="col-xs-4">
                        <div class="control-label">
                           <label> Date </label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control datepicker" name="date_recv" required/>
                          </div>
                        </div>
                        <div style="margin-top:5px;">
                           <label>Vendor's Name </label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-user"></i>
                              </div>
                              <input type="text" class="form-control"  placeholder="Kwame Mintah" name="vendor" required/>
                          </div>
                        </div>
                     </div>
                  <!-- ./ Column Left -->
                  <!--Column middle --
                     <div  class="col-xs-4">
                        <div>
                           <label>Supplier </label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-truck"></i>
                              </div>
                              <select class="form-control usrselect" name="sup_id" required>
                                 <option label="----- Select Supplier -----"></option>
                                 <?php
                                    if(!empty($suppliers_info)) {
                                        foreach ($suppliers_info As $sup) {
                                 ?>
                                 <option value="<?= base64_decode($sup->sup_id) ?>"><?= $sup->name; ?></option>
                                 <?php
                                             
                                        }
                                    }
                                 ?>
                              </select>
                          </div>
                        </div>
                     </div>
                <!-- ./Column middle -->
                <!--Column right --
                     <div  class="col-xs-4">
                        <div>
                           <label>Invoice No. </label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                <strong>#</strong>
                              </div>
                              <input type="text" class="form-control"  placeholder="GF009856" name="inv_no" value="<?= @$next_invoice_id ?>" required readonly/>
                          </div>
                        </div>
                        <div style="margin-top:5px;">
                           <label>Total Cost</label>
                           <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                              </div>
                              <input type="number" class="form-control" placeholder="5000" min="1" name="tot_cost" id="totcost"  required/>
                          </div>
                        </div>
                     </div>
                <!-- ./Column right -->
                <div class="col-md-9">
                  <table class="table table-striped table-hover table-responsive" id="newstock">
                      <thead >
                          <th style="width: 30%"> Item Name </th>
                          <!--<th style="width: 30%"> Item Description </th>-->
                          <!-- <th style="width:7%"> Cost Price </th>-->
                          <th> Item Qty </th>
                          <th> Unit Price </th>
                          <!-- <th style="width:7%">Total Price </th> -->
                          <th> Action </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                              <form action="Ajax_Load_ITM_Name" method="post">
                                <!--<select id="category" class="form-control select2" name="prod_id[]"/>-->
                                <select class="select2" name="prod_id[]" id="category" data-placeholder="Select Product" required style="width: 100%; height: 33px !important;">
                                  <option></option>
                                  <?php
                                      if(!empty($productnames))
                                      {
                                         foreach ($productnames As $prod)
                                         {
                                            print "<option value='".base64_encode($prod->prod_id)."'>".$prod->prod_name."</option>";
                                         }
                                      }
                                  ?>
                                </select>
                              </form>
                          </td>
                           <!--<td>
                             <select class="form-control" data-placeholder="Select Description" name="desc_id[]" required />
                                  <option></option>
                                  <?php
/*                                      if(!empty($Description_info))
                                      {
                                        foreach ($Description_info As $desc)
                                        {
                                            print "<option value='".base64_encode($desc->desc_id)."'>".$desc->desc."</option>";
                                         }
                                      }
                                  */?>
                              </select>
                          </td>-->
                          <td>
                             <input id="qty" type="number" class="form-control qty" name="qty[]" placeholder="00" style="height:33px !important;" min="0"/>
                          </td>
                          <td>
                             <input id="unit" type="number" class="form-control unit" name="unit[]" placeholder="00" step="0.01" style="height:33px !important;" min="0"/>
                          </td>
                           <!-- <td>
                             <input id="totalprice" type="number" class="form-control totalamt" name="tot_sub[]" placeholder="20" required style="height:33px !important;" />
                          </td>  -->
                          <td>
                            <a href="" title="Add New" class="btn btn-primary btn-xs addmore" data-toggle="modal" data-target="view" data-dismiss="modal">
                              <i class="fa fa-plus"></i>
                            </a>
                          </td>
                        </tr>
                      </tbody>
                  </table> <br/>
                  <button type="submit" name="update_stock" class="btn btn-success pull-right"><i class="fa fa-database"></i> Update </button>
                </div>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>