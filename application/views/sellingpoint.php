  <div class="btn-floating" id="help-actions">
  <div class="btn-bg"></div>
  <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions">
    <i class="icon fa fa-plus"></i>
    <span class="help-text">Shortcut</span>
  </button>
  <div class="toggle-content">
    <ul class="actions">
      <li><a href="" data-toggle="modal" data-target="#Account">ACCOUNTS</a></li>
      <li><a href="<?= base_url()?>dashboard/salesreport">Daily Ledger</a></li>
      <!-- <li><a href="" data-toggle="modal" data-target="#issue">Issues</a></li> -->
      <li><a href="<?= base_url()?>dashboard/customersattach" >Customers</a></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-lg-8 ">
    <div class="card">
      <div class="card-header">
        <a Link="#443b52 ! important" vlink="red" href="<?=base_url()?>dashboard/pos"><i class="fa fa-refresh fa-2x"></i></a>
        <div class="col-md-12">
          <select class="select2" data-placeholder="Select Description" onchange="fetch_itms(this.value)">
            <option></option>
            <?php
              if(!empty($allproduct))
              {
                foreach($allproduct As $product) :
                  print "<option value='".base64_encode($product->ProductCode)."'>$product->Item_Name - $product->Description</option>";
                endforeach;
              }
            ?>
          </select>
        </div>
      </div>
      <div class="card-body">
        <div class="row" id="ProductDisplay">
          <?php
            if(!empty($desc_info)) :

              foreach($desc_info As $desc) :
          ?>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" style="padding-bottom:10px;">
            <a id="cat-<?=$desc->desc_id?>" class="card card-banner card-blue-light" onclick="fetch_itms_from_category(<?= $desc->desc_id ?>, <?=sizeof($desc_info)?>)">
              <div class="card-body" style="height: 100px">
                <i class="icon fa <?=$desc->icon?> fa-4x"></i>
                <div class="content" style="padding: 1.75em">
                  <div class="value" id="poscategory"><?= $desc->desc ?></div>
                </div>
              </div>
            </a>
          </div>
          <?php
              endforeach;

            endif;
          ?>
        </div>
      </br>
      </div>
    </div>
  </div>
  <form action="" method="post">
  <div class="col-md-6 col-lg-4 ">
    <div class="card">
      <div class="card-header">
        <div class="card-title">TallyBoard</div>
        <ul class="card-action">
          <li class="dropdown">
            <a href="/" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-cogs" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#">Save</a></li>
              <li><a href="#">Discard</a></li>
              <li><a href="#">Order</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <table class="table" id="tallytable" style="margin-left:-20px;">
          <thead>
            <th>#</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Unit</th>
            <th >sum</th>
          </thead>
          <tbody id="tallytbl">
            <tr></tr>
          </tbody>
        </table>
        <input type="hidden" name="tot_costs" class="totcost">
        <div class="col-lg-12 col-md-6 ">
          <a class="card card-banner card-blue-light checkout" target="_blank">
            <div class="card-body">
              <i class="icon fa fa-thumbs-o-up fa-4x"></i>
              <div class="content">
                <div class="title">Check Out</div>
                <div class="value"><span class="sign">GHȻ</span><b class="checkoutamt"></b> </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>

