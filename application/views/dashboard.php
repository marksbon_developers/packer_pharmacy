<div class="row">
    <div class="col-md-3">
        <a class="card card-banner card-green-light" href="<?= base_url() ?>dashboard/salesreport">
            <div class="card-body">
                <i class="icon fa fa-calculator fa-3x"></i>
                <div class="content">
                    <div class="title">Total Sales</div>
                    <div class="value" style="font-weight: bolder">
                        <span class="sign">GHȻ</span><?php (@$totalSales) ? print $totalSales : print "0"; ?>
                    </div>
                </div>
            </div>
        </a>

    </div>
    <div class="col-md-2" style="width: 22% !important;">
        <a class="card card-banner card-blue-light" href="<?= base_url() ?>dashboard/salesreport">
            <div class="card-body">
                <i class="icon fa fa-money fa-4x"></i>
                <div class="content">
                    <div class="title">Cash-In-Hand</div>
                    <div class="value" style="font-weight: bolder">
                        <span class="sign">GHȻ</span><?php (@$cashsales) ? print $totalSales : print "0"; ?>
                    </div>
                </div>
            </div>
        </a>

    </div>
    <div class="col-md-2"  style="width: 22% !important;">
        <a class="card card-banner card-yellow-light" href="<?= base_url() ?>dashboard/salesreport">
            <div class="card-body">
                <i class="icon fa fa-mobile fa-4x"></i>
                <div class="content">
                    <div class="title">MoMo Total</div>
                    <div class="value" style="font-weight: bolder">
                        <span class="sign">GHȻ</span><?php (@$momosales) ? print $totalSales : print "0"; ?>
                    </div>
                </div>
            </div>
        </a>

    </div>
    <div class="col-md-2" style="width: 22% !important;">
        <a class="card card-banner card-orange-light">
            <div class="card-body">
                <i class="icon fa fa-minus fa-4x"></i>
                <div class="content">
                    <div class="title">Expenses</div>
                    <div class="value"><span class="sign">GHȻ</span><?= 0 ?></div>
                </div>
            </div>
        </a>

    </div>
    <!--<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light" href="<?/*= base_url() */?>dashboard/salesreport">
            <div class="card-body">
                <i class="icon fa fa-cart-plus fa-4x"></i>
                <div class="content">
                    <div class="title">Transactions</div>
                    <div class="value"><span
                                class="sign"></span><?php /*(@$total_transactions) ? print $total_transactions : print "0"; */?>
                    </div>
                </div>
            </div>
        </a>

    </div>-->
</div>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
        <div class="card card-tab card-mini">
            <div class="card-header">
                <ul class="nav nav-tabs tab-stats">
                    <li role="tab1" class="active">
                        <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Stock Chart</a>
                    </li>
                    <li role="tab2">
                        <a href="#tab1" aria-controls="tab2" role="tab" data-toggle="tab">Cash Flow</a>
                    </li>
                    <li role="tab2">
                        <a href="#tab1" aria-controls="tab3" role="tab" data-toggle="tab">Indicators</a>
                    </li>
                </ul>
            </div>
            <div class="card-body tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab1">
                    <div class="row">
                        <div class=" col-lg-4 col-sm-8">
                            <div class="chart ct-chart-browser ct-perfect-fourth"></div>
                        </div>
                        <div class="col-sm-4">
                            <ul class="chart-label">
                                <li class="ct-label ct-series-a">Total Stock Cash</li>
                                <li class="ct-label ct-series-b">Transactions</li>
                                <li class="ct-label ct-series-c">Total Profit</li>
                                <li class="ct-label ct-series-d">Tax Payable</li>
                                <li class="ct-label ct-series-e">Deductions</li>
                            </ul>
                        </div>
                        <!-- <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								  <div class="section">
									 <div class="section-body">
										<div class="ct-chart ct-perfect-fourth"></div>
									 </div>
								  </div>
								</div> -->
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab2">
                    <div class="row">
                        <div class="col-lg-4 col-sm-8">
                            <div class="chart ct-chart-os ct-perfect-fourth"></div>
                        </div>
                        <div class="col-sm-4">
                            <ul class="chart-label">
                                <li class="ct-label ct-series-a">Total Stock Cash</li>
                                <li class="ct-label ct-series-b">Capital Cash</li>
                                <li class="ct-label ct-series-c">Total Profit</li>
                                <li class="ct-label ct-series-d">Tax Payable</li>
                                <li class="ct-label ct-series-e">Others</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab3">
                </div>
            </div>
        </div>
    </div>
</div>