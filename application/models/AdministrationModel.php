<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class AdministrationModel extends CI_Model
{
	public function getTaxInfo()
	{
		$tablename = "tax_system";
		$queryResult =  $this->db->get($tablename);

		return $queryResult->row();
	}
}