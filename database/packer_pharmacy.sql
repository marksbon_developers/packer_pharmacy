-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for hpx703cr_packa-pharma
CREATE DATABASE IF NOT EXISTS `hpx703cr_packa-pharma` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hpx703cr_packa-pharma`;

-- Dumping structure for table hpx703cr_packa-pharma.access_login_failed
CREATE TABLE IF NOT EXISTS `access_login_failed` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL,
  `access_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.access_login_failed: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_login_failed` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_login_failed` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.access_login_success
CREATE TABLE IF NOT EXISTS `access_login_success` (
  `login_id` int(20) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(20) NOT NULL,
  `time_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_out` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online` int(1) NOT NULL DEFAULT '1',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.access_login_success: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_login_success` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_login_success` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.access_roles_priviledges_group
CREATE TABLE IF NOT EXISTS `access_roles_priviledges_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.access_roles_priviledges_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_roles_priviledges_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_roles_priviledges_group` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.access_roles_priviledges_user
CREATE TABLE IF NOT EXISTS `access_roles_priviledges_user` (
  `employee_id` varchar(50) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `group_id` int(11) NOT NULL,
  UNIQUE KEY `employee_id_UNIQUE` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.access_roles_priviledges_user: ~13 rows (approximately)
/*!40000 ALTER TABLE `access_roles_priviledges_user` DISABLE KEYS */;
INSERT INTO `access_roles_priviledges_user` (`employee_id`, `roles`, `priviledges`, `group_id`) VALUES
	('1', 'STATISTICS|POS|SUPCUST|PROD|CASH|USERS|ROLES|REPORT|ORDER|SETTINGS|INV|GNL|', '', 0),
	('13', 'STATISTICS|POS|SUPCUST|PROD|CASH|USERS|ROLES|REPORT|SETTINGS|', '', 0),
	('14', 'USERS|', '', 0),
	('15', 'STATISTICS|POS|', '', 0),
	('16', 'STATISTICS|POS|SUPCUST|PROD|CASH|USERS|ROLES|REPORT|ORDER|SETTINGS|INV|GNL|', '', 0),
	('17', 'STATISTICS|POS|', '', 0),
	('18', 'STATISTICS|POS|', '', 0),
	('19', 'STATISTICS|POS|', '', 0),
	('20', 'STATISTICS|POS|SETTINGS|', '', 0),
	('21', 'POS|', '', 0),
	('22', 'POS|', '', 0),
	('23', 'STATISTICS|POS|REPORT|GNL|', '', 0),
	('24', 'STATISTICS|POS|REPORT|', '', 0);
/*!40000 ALTER TABLE `access_roles_priviledges_user` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.access_users
CREATE TABLE IF NOT EXISTS `access_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(20) NOT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `login_attempt` tinyint(1) NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.access_users: ~3 rows (approximately)
/*!40000 ALTER TABLE `access_users` DISABLE KEYS */;
INSERT INTO `access_users` (`id`, `employee_id`, `username`, `passwd`, `active`, `login_attempt`) VALUES
	(1, 'KAD/SYS/OO1', 'sysadmin', '$2y$10$0pf2nnT2mLOVHDbW4jpNI.LsvU0KT0HwhNi.5azyVyQAvEqkjnxDW', 1, 5),
	(23, 'KAD/TEMP/023', 'USER1', '$2y$10$r/T.faN4ZescpOS2pwEFeevgYZRao00eZpcamkLooKC8tjbMh1z9a', 1, 5),
	(24, 'KAD/TEMP/024', 'agyemang', '$2y$10$DUVsUhtzBZ39iBrNwUn4Pee3IvjO5p4VhV9mJW.h9BEolskW/H0mO', 1, 5);
/*!40000 ALTER TABLE `access_users` ENABLE KEYS */;

-- Dumping structure for view hpx703cr_packa-pharma.access_user_details
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `access_user_details` (
	`id` INT(11) NOT NULL,
	`Username` VARCHAR(20) NOT NULL COLLATE 'latin1_general_cs',
	`Password` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`Account_Status` TINYINT(1) NOT NULL,
	`Employee_id` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`User_Roles` TEXT NULL COLLATE 'latin1_swedish_ci',
	`User_Priviledges` TEXT NULL COLLATE 'latin1_swedish_ci',
	`Group_ID` INT(11) NULL,
	`Group_Name` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`Group_Roles` TEXT NULL COLLATE 'latin1_swedish_ci',
	`Group_Priviledges` TEXT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for table hpx703cr_packa-pharma.accounts_transactions
CREATE TABLE IF NOT EXISTS `accounts_transactions` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_type_id` int(11) NOT NULL,
  `debit_amt` int(11) DEFAULT '0',
  `credit_amt` int(11) DEFAULT '0',
  `balance` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `customer_id` varchar(20) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.accounts_transactions: ~2 rows (approximately)
/*!40000 ALTER TABLE `accounts_transactions` DISABLE KEYS */;
INSERT INTO `accounts_transactions` (`auto_id`, `acc_type_id`, `debit_amt`, `credit_amt`, `balance`, `purpose`, `customer_id`, `employee_id`, `date_created`) VALUES
	(1, 1, 20, 0, -20, 'FOR FOOD', '', 'KAD/TEMP/019', '2017-05-10 15:25:43'),
	(2, 1, 20, 0, -20, 'DAILY EXPENCES', '', 'KAD/TEMP/015', '2017-05-25 07:18:52');
/*!40000 ALTER TABLE `accounts_transactions` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.account_types
CREATE TABLE IF NOT EXISTS `account_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_name` varchar(50) NOT NULL,
  `acc_ref_no` varchar(20) NOT NULL,
  `acc_tax` varchar(5) DEFAULT NULL,
  `acc_desc` varchar(255) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.account_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `account_types` DISABLE KEYS */;
INSERT INTO `account_types` (`id`, `acc_name`, `acc_ref_no`, `acc_tax`, `acc_desc`, `created_by`) VALUES
	(1, 'EXPENCES', 'REF#001', 'Yes', 'EXPENCES', 'KAD/SYS/OO1');
/*!40000 ALTER TABLE `account_types` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.dashboard_tabs
CREATE TABLE IF NOT EXISTS `dashboard_tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `bg` varchar(50) NOT NULL,
  `priviledges` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.dashboard_tabs: ~0 rows (approximately)
/*!40000 ALTER TABLE `dashboard_tabs` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_tabs` ENABLE KEYS */;

-- Dumping structure for view hpx703cr_packa-pharma.full_product_details
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `full_product_details` (
	`Item_Name` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`ProductCode` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`Description` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`Unit_Qty` INT(5) NOT NULL,
	`Unit_Price` FLOAT NOT NULL,
	`Cost_Price` FLOAT NULL,
	`Promo_Qty` INT(5) NULL,
	`Promo_Price` FLOAT NULL,
	`Current_Qty` BIGINT(20) NULL,
	`Expiry` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`Product_ID` INT(11) NOT NULL,
	`DescriptionID` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for table hpx703cr_packa-pharma.general_last_ids
CREATE TABLE IF NOT EXISTS `general_last_ids` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `employee_id` tinyint(5) DEFAULT NULL,
  `sup_id` tinyint(2) DEFAULT NULL,
  `prod_id` tinyint(5) DEFAULT NULL,
  `cat_id` tinyint(5) DEFAULT NULL,
  `desc_id` tinyint(5) DEFAULT NULL,
  `invoice_no` tinyint(5) DEFAULT NULL,
  `pur_order_no` tinyint(5) DEFAULT NULL,
  `acct_type_no` tinyint(5) DEFAULT NULL,
  `refcode` varchar(10) DEFAULT NULL,
  `product_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.general_last_ids: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_last_ids` DISABLE KEYS */;
INSERT INTO `general_last_ids` (`id`, `employee_id`, `sup_id`, `prod_id`, `cat_id`, `desc_id`, `invoice_no`, `pur_order_no`, `acct_type_no`, `refcode`, `product_code`) VALUES
	(1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0);
/*!40000 ALTER TABLE `general_last_ids` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.group_roles_priviledges
CREATE TABLE IF NOT EXISTS `group_roles_priviledges` (
  `group_id` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.group_roles_priviledges: ~0 rows (approximately)
/*!40000 ALTER TABLE `group_roles_priviledges` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_roles_priviledges` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.hr_companyinfo
CREATE TABLE IF NOT EXISTS `hr_companyinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `tel_1` varchar(20) NOT NULL,
  `tel_2` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `location` varchar(50) NOT NULL,
  `logo_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.hr_companyinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `hr_companyinfo` DISABLE KEYS */;
INSERT INTO `hr_companyinfo` (`id`, `name`, `tel_1`, `tel_2`, `fax`, `email`, `website`, `address`, `location`, `logo_path`) VALUES
	(2, 'B.O. Agyeman Chemical Shop', '0558677677', '0245074416 ', '', 'agyeman@yahoo.com', '', 'P O Box 17633', 'Kumasi', 'resources/images/fotostore.jpg');
/*!40000 ALTER TABLE `hr_companyinfo` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.hr_countries
CREATE TABLE IF NOT EXISTS `hr_countries` (
  `cou_code` char(2) NOT NULL DEFAULT '',
  `cou_name` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`cou_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table hpx703cr_packa-pharma.hr_countries: ~239 rows (approximately)
/*!40000 ALTER TABLE `hr_countries` DISABLE KEYS */;
INSERT INTO `hr_countries` (`cou_code`, `cou_name`) VALUES
	('AD', 'Andorra'),
	('AE', 'United Arab Emirates'),
	('AF', 'Afghanistan'),
	('AG', 'Antigua and Barbuda'),
	('AI', 'Anguilla'),
	('AL', 'Albania'),
	('AM', 'Armenia'),
	('AN', 'Netherlands Antilles'),
	('AO', 'Angola'),
	('AQ', 'Antarctica'),
	('AR', 'Argentina'),
	('AS', 'American Samoa'),
	('AT', 'Austria'),
	('AU', 'Australia'),
	('AW', 'Aruba'),
	('AZ', 'Azerbaijan'),
	('BA', 'Bosnia and Herzegovina'),
	('BB', 'Barbados'),
	('BD', 'Bangladesh'),
	('BE', 'Belgium'),
	('BF', 'Burkina Faso'),
	('BG', 'Bulgaria'),
	('BH', 'Bahrain'),
	('BI', 'Burundi'),
	('BJ', 'Benin'),
	('BM', 'Bermuda'),
	('BN', 'Brunei Darussalam'),
	('BO', 'Bolivia'),
	('BR', 'Brazil'),
	('BS', 'Bahamas'),
	('BT', 'Bhutan'),
	('BV', 'Bouvet Island'),
	('BW', 'Botswana'),
	('BY', 'Belarus'),
	('BZ', 'Belize'),
	('CA', 'Canada'),
	('CC', 'Cocos (Keeling) Islands'),
	('CD', 'Congo, the Democratic Republic of the'),
	('CF', 'Central African Republic'),
	('CG', 'Congo'),
	('CH', 'Switzerland'),
	('CI', 'Cote D\'Ivoire'),
	('CK', 'Cook Islands'),
	('CL', 'Chile'),
	('CM', 'Cameroon'),
	('CN', 'China'),
	('CO', 'Colombia'),
	('CR', 'Costa Rica'),
	('CS', 'Serbia and Montenegro'),
	('CU', 'Cuba'),
	('CV', 'Cape Verde'),
	('CX', 'Christmas Island'),
	('CY', 'Cyprus'),
	('CZ', 'Czech Republic'),
	('DE', 'Germany'),
	('DJ', 'Djibouti'),
	('DK', 'Denmark'),
	('DM', 'Dominica'),
	('DO', 'Dominican Republic'),
	('DZ', 'Algeria'),
	('EC', 'Ecuador'),
	('EE', 'Estonia'),
	('EG', 'Egypt'),
	('EH', 'Western Sahara'),
	('ER', 'Eritrea'),
	('ES', 'Spain'),
	('ET', 'Ethiopia'),
	('FI', 'Finland'),
	('FJ', 'Fiji'),
	('FK', 'Falkland Islands (Malvinas)'),
	('FM', 'Micronesia, Federated States of'),
	('FO', 'Faroe Islands'),
	('FR', 'France'),
	('GA', 'Gabon'),
	('GB', 'United Kingdom'),
	('GD', 'Grenada'),
	('GE', 'Georgia'),
	('GF', 'French Guiana'),
	('GH', 'Ghana'),
	('GI', 'Gibraltar'),
	('GL', 'Greenland'),
	('GM', 'Gambia'),
	('GN', 'Guinea'),
	('GP', 'Guadeloupe'),
	('GQ', 'Equatorial Guinea'),
	('GR', 'Greece'),
	('GS', 'South Georgia and the South Sandwich Islands'),
	('GT', 'Guatemala'),
	('GU', 'Guam'),
	('GW', 'Guinea-Bissau'),
	('GY', 'Guyana'),
	('HK', 'Hong Kong'),
	('HM', 'Heard Island and Mcdonald Islands'),
	('HN', 'Honduras'),
	('HR', 'Croatia'),
	('HT', 'Haiti'),
	('HU', 'Hungary'),
	('ID', 'Indonesia'),
	('IE', 'Ireland'),
	('IL', 'Israel'),
	('IN', 'India'),
	('IO', 'British Indian Ocean Territory'),
	('IQ', 'Iraq'),
	('IR', 'Iran, Islamic Republic of'),
	('IS', 'Iceland'),
	('IT', 'Italy'),
	('JM', 'Jamaica'),
	('JO', 'Jordan'),
	('JP', 'Japan'),
	('KE', 'Kenya'),
	('KG', 'Kyrgyzstan'),
	('KH', 'Cambodia'),
	('KI', 'Kiribati'),
	('KM', 'Comoros'),
	('KN', 'Saint Kitts and Nevis'),
	('KP', 'Korea, Democratic People\'s Republic of'),
	('KR', 'Korea, Republic of'),
	('KW', 'Kuwait'),
	('KY', 'Cayman Islands'),
	('KZ', 'Kazakhstan'),
	('LA', 'Lao People\'s Democratic Republic'),
	('LB', 'Lebanon'),
	('LC', 'Saint Lucia'),
	('LI', 'Liechtenstein'),
	('LK', 'Sri Lanka'),
	('LR', 'Liberia'),
	('LS', 'Lesotho'),
	('LT', 'Lithuania'),
	('LU', 'Luxembourg'),
	('LV', 'Latvia'),
	('LY', 'Libyan Arab Jamahiriya'),
	('MA', 'Morocco'),
	('MC', 'Monaco'),
	('MD', 'Moldova, Republic of'),
	('MG', 'Madagascar'),
	('MH', 'Marshall Islands'),
	('MK', 'Macedonia, the Former Yugoslav Republic of'),
	('ML', 'Mali'),
	('MM', 'Myanmar'),
	('MN', 'Mongolia'),
	('MO', 'Macao'),
	('MP', 'Northern Mariana Islands'),
	('MQ', 'Martinique'),
	('MR', 'Mauritania'),
	('MS', 'Montserrat'),
	('MT', 'Malta'),
	('MU', 'Mauritius'),
	('MV', 'Maldives'),
	('MW', 'Malawi'),
	('MX', 'Mexico'),
	('MY', 'Malaysia'),
	('MZ', 'Mozambique'),
	('NA', 'Namibia'),
	('NC', 'New Caledonia'),
	('NE', 'Niger'),
	('NF', 'Norfolk Island'),
	('NG', 'Nigeria'),
	('NI', 'Nicaragua'),
	('NL', 'Netherlands'),
	('NO', 'Norway'),
	('NP', 'Nepal'),
	('NR', 'Nauru'),
	('NU', 'Niue'),
	('NZ', 'New Zealand'),
	('OM', 'Oman'),
	('PA', 'Panama'),
	('PE', 'Peru'),
	('PF', 'French Polynesia'),
	('PG', 'Papua New Guinea'),
	('PH', 'Philippines'),
	('PK', 'Pakistan'),
	('PL', 'Poland'),
	('PM', 'Saint Pierre and Miquelon'),
	('PN', 'Pitcairn'),
	('PR', 'Puerto Rico'),
	('PS', 'Palestinian Territory, Occupied'),
	('PT', 'Portugal'),
	('PW', 'Palau'),
	('PY', 'Paraguay'),
	('QA', 'Qatar'),
	('RE', 'Reunion'),
	('RO', 'Romania'),
	('RU', 'Russian Federation'),
	('RW', 'Rwanda'),
	('SA', 'Saudi Arabia'),
	('SB', 'Solomon Islands'),
	('SC', 'Seychelles'),
	('SD', 'Sudan'),
	('SE', 'Sweden'),
	('SG', 'Singapore'),
	('SH', 'Saint Helena'),
	('SI', 'Slovenia'),
	('SJ', 'Svalbard and Jan Mayen'),
	('SK', 'Slovakia'),
	('SL', 'Sierra Leone'),
	('SM', 'San Marino'),
	('SN', 'Senegal'),
	('SO', 'Somalia'),
	('SR', 'Suriname'),
	('ST', 'Sao Tome and Principe'),
	('SV', 'El Salvador'),
	('SY', 'Syrian Arab Republic'),
	('SZ', 'Swaziland'),
	('TC', 'Turks and Caicos Islands'),
	('TD', 'Chad'),
	('TF', 'French Southern Territories'),
	('TG', 'Togo'),
	('TH', 'Thailand'),
	('TJ', 'Tajikistan'),
	('TK', 'Tokelau'),
	('TL', 'Timor-Leste'),
	('TM', 'Turkmenistan'),
	('TN', 'Tunisia'),
	('TO', 'Tonga'),
	('TR', 'Turkey'),
	('TT', 'Trinidad and Tobago'),
	('TV', 'Tuvalu'),
	('TW', 'Taiwan'),
	('TZ', 'Tanzania, United Republic of'),
	('UA', 'Ukraine'),
	('UG', 'Uganda'),
	('UM', 'United States Minor Outlying Islands'),
	('US', 'United States'),
	('UY', 'Uruguay'),
	('UZ', 'Uzbekistan'),
	('VA', 'Holy See (Vatican City State)'),
	('VC', 'Saint Vincent and the Grenadines'),
	('VE', 'Venezuela'),
	('VG', 'Virgin Islands, British'),
	('VI', 'Virgin Islands, U.s.'),
	('VN', 'Viet Nam'),
	('VU', 'Vanuatu'),
	('WF', 'Wallis and Futuna'),
	('WS', 'Samoa'),
	('YE', 'Yemen'),
	('YT', 'Mayotte'),
	('ZA', 'South Africa'),
	('ZM', 'Zambia'),
	('ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `hr_countries` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.hr_emp_pers_info
CREATE TABLE IF NOT EXISTS `hr_emp_pers_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(20) DEFAULT NULL,
  `fullname` varchar(100) NOT NULL,
  `dob` varchar(20) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `residence` varchar(100) DEFAULT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `eme_name` varchar(50) DEFAULT NULL,
  `eme_tel` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`employee_id`,`tel1`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.hr_emp_pers_info: ~1 rows (approximately)
/*!40000 ALTER TABLE `hr_emp_pers_info` DISABLE KEYS */;
INSERT INTO `hr_emp_pers_info` (`id`, `employee_id`, `fullname`, `dob`, `gender`, `residence`, `tel1`, `tel2`, `email`, `eme_name`, `eme_tel`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 'KAD/SYS/OO1', 'System Developer', '28-03-2017', 'N', 'Amasaman', '0541786220', '0245626487', 'marksbon@gmail.com', 'System Developer', '0541786220', NULL, NULL, '2019-10-03 23:36:32', '2019-10-03 23:36:32');
/*!40000 ALTER TABLE `hr_emp_pers_info` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_audit
CREATE TABLE IF NOT EXISTS `product_audit` (
  `invoice_no` varchar(50) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `sup_id` varchar(20) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `date_recv` varchar(50) NOT NULL,
  `comb_prod_id` text NOT NULL,
  `comb_unit_price` text NOT NULL,
  `comb_qty` text NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `comb_cost_p` varchar(255) NOT NULL,
  `comb_desc_id` varchar(255) NOT NULL,
  `comb_unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_audit: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_audit` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`cat_id`),
  UNIQUE KEY `cat_name_UNIQUE` (`cat_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_codes
CREATE TABLE IF NOT EXISTS `product_codes` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(50) NOT NULL,
  PRIMARY KEY (`prod_id`),
  UNIQUE KEY `prod_id_UNIQUE` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_codes: ~302 rows (approximately)
/*!40000 ALTER TABLE `product_codes` DISABLE KEYS */;
INSERT INTO `product_codes` (`prod_id`, `prod_name`) VALUES
	(1, 'PARACETAMOL'),
	(2, 'Bafan Mixture'),
	(3, 'Victory Garlic Mixs'),
	(4, 'Sparnis Garlic Mix'),
	(5, 'Adom Ladies Mix'),
	(6, 'Yaakson Mix'),
	(7, 'Adusa Heb Mix'),
	(8, 'Zahara Heb Mix'),
	(9, 'Entera -5'),
	(10, 'Abbey Heb. Mix'),
	(11, 'Adinkra Heb Mix'),
	(12, 'Tinatett Venecare'),
	(13, 'Solak'),
	(14, 'Prostacure'),
	(15, 'Bethel GPC Mix'),
	(16, 'Herbatine Heb'),
	(17, 'Hepa Plus'),
	(18, 'Time'),
	(19, 'Taabea'),
	(20, 'Malitone'),
	(21, 'Adutwumuwaa Bitters'),
	(22, 'Adom Bi Heb'),
	(23, 'Ahenekan Heb'),
	(24, 'Imboost Heb'),
	(25, 'Zahara Man Cap'),
	(26, 'Adusa Heb Cap'),
	(27, 'Kafeefe Heb. Cap.'),
	(28, 'Givers Koo Cap'),
	(29, 'K & G Heb Cap'),
	(30, 'Today Koo'),
	(31, 'Adom W&G Cap'),
	(32, 'Zahara Gee'),
	(33, 'Crystal Man Power'),
	(34, 'Forkuo Men\'s'),
	(35, 'Zipman Cap'),
	(36, 'Fresh Ladi Cap'),
	(37, 'Tremendous Cap'),
	(38, 'Taabea Cap'),
	(39, 'Bafan Cap'),
	(40, 'Adom Koo Ointiment'),
	(41, 'Bethel GPC Cap'),
	(42, 'Kill II'),
	(43, 'Heaven'),
	(44, 'Propa Pad'),
	(45, 'Yazz Pad'),
	(46, 'Gauze Bandage'),
	(47, 'Camel Antiseptic'),
	(48, 'Truman'),
	(49, 'Prosluv'),
	(50, 'Pa-kum'),
	(51, 'Givers P'),
	(52, 'Genecure'),
	(53, 'Vene- Cap'),
	(54, 'Hi-Lady'),
	(55, 'Lucky Heb'),
	(56, 'Mr Q'),
	(57, 'Kindom Garlic'),
	(58, 'Temple of Heaven Inhaler'),
	(59, 'Nyame Nsa Wo Mu'),
	(60, 'Sibi Women'),
	(61, 'Sibi Men'),
	(62, 'Adutwumwaa Cap'),
	(63, 'V-Firm'),
	(64, 'Summer\'s Eve'),
	(65, 'Huichin Cap'),
	(66, 'Foknnol'),
	(67, 'Forkuo'),
	(68, 'Madam Catherine'),
	(69, 'Gifas'),
	(70, 'Alhaji Yakubu'),
	(71, 'Lawson Heb'),
	(72, 'Adutwumwaa Tonic'),
	(73, 'Roc Mixture'),
	(74, 'Cocho Shower Gel Soon'),
	(75, 'Choho Shower Gel 750'),
	(76, 'Madar Zone'),
	(77, 'Mercy Soap'),
	(78, 'Joy Soap'),
	(79, 'Chocho  Soap'),
	(80, 'Adom Tonic'),
	(81, 'V-Mixture'),
	(82, 'Lumarel'),
	(83, 'Hycid-20'),
	(84, 'Odymin'),
	(85, 'Leena'),
	(86, 'IbuCap'),
	(87, 'Becoactin'),
	(88, 'Coldrilif'),
	(89, 'Ronfit Junior'),
	(90, 'Eyecopen'),
	(91, 'Letecam'),
	(92, 'Dialum'),
	(93, 'Loperamide'),
	(94, 'Cyprodine'),
	(95, 'Zeenglo'),
	(96, 'TabuCap'),
	(97, 'Abyvita'),
	(98, 'No. 10'),
	(99, 'Martins'),
	(100, 'ORS'),
	(101, 'Lexsporin'),
	(102, 'Vaginax'),
	(103, 'Clotrimazole'),
	(104, 'Silverzine'),
	(105, 'Peladol Gel'),
	(106, 'Infa-V'),
	(107, 'Lofnac Gel'),
	(108, 'Pain Heat'),
	(109, 'Dicloron'),
	(110, 'Infa -V Ointment'),
	(111, 'Anmol 250'),
	(112, 'Anmol 125'),
	(113, 'Lofnac 100'),
	(114, 'Gacet 250'),
	(115, 'Gacet 125'),
	(116, 'Xyron'),
	(117, 'Hydro cortisone Acetate'),
	(118, 'Robb'),
	(119, 'Ultra Pleasure'),
	(120, 'Kiss'),
	(121, 'Tetracycline Ointment'),
	(122, 'Jet 2'),
	(123, 'As-II Inhaler'),
	(124, 'perfectil'),
	(125, 'Well Woman'),
	(126, 'Well Man'),
	(127, 'Evecare'),
	(128, 'Liv-52'),
	(129, 'Mentat'),
	(130, 'Mycolex Cream'),
	(131, 'Surfaz'),
	(132, 'Antasil'),
	(133, 'Mediart Forte'),
	(134, 'Cartef'),
	(135, 'Folic Acid'),
	(136, 'Multivitamin'),
	(137, 'B-Co'),
	(138, 'Metrolex-F'),
	(139, 'Metro -Z'),
	(140, 'Lonart'),
	(141, 'Cyfen'),
	(142, 'Happyrona'),
	(143, 'Ibuprofenenafeno'),
	(144, 'Efpac'),
	(145, 'Kwik Action'),
	(146, 'Parafenac'),
	(147, 'Prednisolone'),
	(148, 'Cafalgin'),
	(149, 'Peladol'),
	(150, 'Phopain'),
	(151, 'APC'),
	(152, 'Citro-C'),
	(153, 'Zulu'),
	(154, 'Menstak'),
	(155, 'Dymol'),
	(156, 'Rapinol'),
	(157, 'Stopkof'),
	(158, 'Zubes'),
	(159, 'Menthox'),
	(160, 'Ronffnac'),
	(161, 'Mixtab'),
	(162, 'Prednisolone edo'),
	(163, 'Gebedol'),
	(164, 'Samalin'),
	(165, 'Gebediclo'),
	(166, 'M2-Tone'),
	(167, 'Mosegor'),
	(168, 'Zintab 10'),
	(169, 'Zintab 20'),
	(170, 'Rhizin'),
	(171, 'Asmanol'),
	(172, 'Histazine'),
	(173, 'Wormox'),
	(174, 'Wormron'),
	(175, 'Wormplex'),
	(176, 'Zincovit'),
	(177, 'Dynewell'),
	(178, 'Paracod'),
	(179, 'Pawafenac'),
	(180, 'Trafan'),
	(181, 'Phenobarbitone'),
	(182, 'Panacin'),
	(183, 'Bisacodyl'),
	(184, 'Strobin'),
	(185, 'Combantrin'),
	(186, 'FAC'),
	(187, 'Mist Senna Co'),
	(188, 'Expect SED'),
	(189, 'Vitamins'),
	(190, 'Ascovite Cee'),
	(191, 'Letaplex B'),
	(192, 'Citrotamin'),
	(193, 'Pot.Cit'),
	(194, 'Magnesium Trisilicate'),
	(195, 'Stopkof Baby'),
	(196, 'Carbocisteine Jenior'),
	(197, 'Malin Adult'),
	(198, 'Magacid'),
	(199, 'Nugel'),
	(200, 'Nugel O'),
	(201, 'Zubes-'),
	(202, 'Zubes Junior'),
	(203, 'Linctus'),
	(204, 'Zedex'),
	(205, 'Teedar'),
	(206, 'Rhizin-'),
	(207, 'Calcium B12'),
	(208, 'Heamoglobin'),
	(209, 'Lact Ulose Solution'),
	(210, 'Virol'),
	(211, 'Liverplex-B'),
	(212, 'Bonaplex'),
	(213, 'Tot\' Hema'),
	(214, 'Quinine'),
	(215, 'Lumatrona'),
	(216, 'Lonart-'),
	(217, 'Cartef-'),
	(218, 'Menthodex 100ml'),
	(219, 'Menthodex 200ml'),
	(220, 'Nexcofer'),
	(221, 'Astyfer'),
	(222, 'Tres-Orix'),
	(223, '3-Fer'),
	(224, 'Cafalgin Junior'),
	(225, 'Zincovit Drop'),
	(226, 'Blue Aid'),
	(227, 'Cororange Drops'),
	(228, 'Abidec Drops'),
	(229, 'Jarifan-2 Drops'),
	(230, 'Zincovit-'),
	(231, 'Zincpfer'),
	(232, 'Zipferon'),
	(233, 'Becoactin-'),
	(234, 'Calamine Lotion'),
	(235, 'Hydrogen Peroxide'),
	(236, 'Vermox'),
	(237, 'Wormron-'),
	(238, 'Neo-Hycolex'),
	(239, 'Ephedrine Nasal Drop'),
	(240, 'Exclofen Eye Drops'),
	(241, 'Normal Saline Drops'),
	(242, 'Ciprofloxacin Eye/Ear Drops'),
	(243, 'Exetomol Eye Drops'),
	(244, 'Nostamine Eye/Nose Drops'),
	(245, 'Gentamicin Eye/Ear Drops'),
	(246, 'Dexatrol Eye/Ear Susp.'),
	(247, 'Eye Clear'),
	(248, 'Hypromellose Ophthamic Soln.'),
	(249, 'Major Nasal Drops'),
	(250, 'Normo-Tears'),
	(251, 'Blue Aid-'),
	(252, 'Lumether'),
	(253, 'Jarifan-2'),
	(254, 'City Blood Tonic'),
	(255, 'Gudapet'),
	(256, 'Baby Linctus'),
	(257, 'Cororange'),
	(258, 'Minamind'),
	(259, 'Vigorix'),
	(260, 'Wokadine'),
	(261, 'Kamaclox Mouth Wash'),
	(262, 'Carboceisteine Adult'),
	(263, 'Nestrim'),
	(264, 'Polyfer 50ml'),
	(265, 'Polyfer 150ml'),
	(266, 'Vitamind'),
	(267, 'Heptopep'),
	(268, 'Feroglobin'),
	(269, 'Cofron'),
	(270, 'Auntie Mary\'s Grape Water'),
	(271, 'Metrolex-F-'),
	(272, 'Promethazine'),
	(273, 'Salo Cold'),
	(274, 'Coldrid'),
	(275, 'Coldrilif-'),
	(276, 'Samalin Adult'),
	(277, 'Samalin Junior'),
	(278, 'Menthox-'),
	(279, 'Kidics'),
	(280, 'Kidivite 125ml'),
	(281, 'Vitarich'),
	(282, 'Aptizoom'),
	(283, 'Haem Up'),
	(284, 'Cyprodine-'),
	(285, 'Enafen'),
	(286, 'Efpac Junior'),
	(287, 'Crepe Bandage'),
	(288, 'Cotton Wool Small'),
	(289, 'Gauze Bandage Small'),
	(290, 'Cotton Wool Big'),
	(291, 'Adult Diapers'),
	(292, 'Chocho Cream'),
	(293, 'Mercy Cream'),
	(294, 'Test Kit'),
	(295, 'Pylirona'),
	(296, 'Plastic Chairs'),
	(297, 'Metal Chairs'),
	(298, 'Tables'),
	(299, 'Fridge (Table Top)'),
	(300, 'Television (Flat Screen)'),
	(301, 'Office Chair'),
	(302, 'PARA DICLOFENAC');
/*!40000 ALTER TABLE `product_codes` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_customers
CREATE TABLE IF NOT EXISTS `product_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) NOT NULL,
  `mobile_number_1` varchar(20) NOT NULL,
  `mobile_number_2` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive','deleted','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_customers` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_desc
CREATE TABLE IF NOT EXISTS `product_desc` (
  `desc_id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(50) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `status` enum('active','inactive','deleted','') NOT NULL,
  PRIMARY KEY (`desc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_desc: ~11 rows (approximately)
/*!40000 ALTER TABLE `product_desc` DISABLE KEYS */;
INSERT INTO `product_desc` (`desc_id`, `desc`, `cat_id`, `status`) VALUES
	(1, 'Tablets', 0, 'active'),
	(2, 'Capsules', 0, 'active'),
	(3, 'Syrups/Mix/Drops', 0, 'active'),
	(4, 'Herbals', 0, 'active'),
	(5, 'General', 0, 'active'),
	(6, 'Anti-Septics', 0, 'deleted'),
	(7, 'Mood Stabilizers', 0, 'deleted'),
	(8, 'Contraceptives', 0, 'deleted'),
	(9, 'Stimulants', 0, 'deleted'),
	(10, 'Tranquilizers', 0, 'deleted'),
	(11, 'Accessories', 0, 'active');
/*!40000 ALTER TABLE `product_desc` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_details
CREATE TABLE IF NOT EXISTS `product_details` (
  `prod_id` varchar(20) NOT NULL,
  `product_id` varchar(20) DEFAULT NULL,
  `desc_id` int(20) NOT NULL,
  `unit_qty` int(5) NOT NULL,
  `cost_price` float DEFAULT NULL,
  `unit_price` float NOT NULL,
  `avail_qty` bigint(20) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `promo_qty` int(5) DEFAULT NULL,
  `promo_price` float DEFAULT NULL,
  `img_path` varchar(100) DEFAULT NULL,
  UNIQUE KEY `UNIQUE` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_details: ~315 rows (approximately)
/*!40000 ALTER TABLE `product_details` DISABLE KEYS */;
INSERT INTO `product_details` (`prod_id`, `product_id`, `desc_id`, `unit_qty`, `cost_price`, `unit_price`, `avail_qty`, `expiry`, `promo_qty`, `promo_price`, `img_path`) VALUES
	('1', 'PROD#0001', 3, 1, 0.12, 0.5, 1095, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('2', 'PROD#0002', 4, 1, 14, 14, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('3', 'PROD#0003', 4, 1, 6, 9, 13, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('4', 'PROD#0004', 4, 1, 9, 9, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('5', 'PROD#0005', 4, 1, 10, 10, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('6', 'PROD#0006', 4, 1, 10, 10, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('7', 'PROD#0007', 4, 1, 14, 14, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('8', 'PROD#0008', 4, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('9', 'PROD#0009', 4, 1, 9, 9, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('10', 'PROD#00010', 4, 1, 15, 15, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('11', 'PROD#0011', 4, 1, 9, 9, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('12', 'PROD#0012', 4, 1, 20, 20, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('13', 'PROD#0013', 4, 1, 14, 14, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('14', 'PROD#0014', 4, 1, 25, 25, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('15', 'PROD#0015', 4, 1, 14, 14, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('16', 'PROD#0016', 4, 1, 20, 20, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('17', 'PROD#0017', 4, 1, 9, 9, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('18', 'PROD#0018', 4, 1, 10, 10, 11, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('19', 'PROD#0019', 4, 1, 10, 10, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('20', 'PROD#0020', 4, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('21', 'PROD#0021', 4, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('22', 'PROD#0022', 4, 1, 15, 15, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('23', 'PROD#0023', 4, 1, 18, 18, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('24', 'PROD#0024', 4, 1, 15, 15, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('25', 'PROD#0025', 4, 1, 15, 15, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('26', 'PROD#0026', 4, 1, 14, 14, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('27', 'PROD#0027', 4, 1, 14, 14, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('28', 'PROD#0028', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('29', 'PROD#0029', 4, 1, 16, 16, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('30', 'PROD#0030', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('31', 'PROD#0031', 4, 1, 16, 16, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('32', 'PROD#0032', 4, 1, 15, 15, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('33', 'PROD#0033', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('34', 'PROD#0034', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('35', 'PROD#0035', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('36', 'PROD#0036', 4, 1, 15, 15, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('37', 'PROD#0037', 4, 1, 16, 16, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('38', 'PROD#0038', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('39', 'PROD#0039', 4, 1, 14, 14, 999999999999997, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('40', 'PROD#0040', 4, 1, 5, 5, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('41', 'PROD#0041', 4, 1, 14, 14, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('42', 'PROD#0042', 4, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('43', 'PROD#0043', 4, 1, 10, 10, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('44', 'PROD#0044', 4, 1, 5, 5, 11, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('45', 'PROD#0045', 4, 1, 5, 5, 35, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('46', 'PROD#0046', 4, 1, 2, 2, 39, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('47', 'PROD#0047', 4, 1, 11, 11, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('48', 'PROD#0048', 4, 1, 15, 15, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('49', 'PROD#0049', 4, 1, 20, 20, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('50', 'PROD#0050', 4, 1, 15, 15, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('51', 'PROD#0051', 4, 1, 16, 16, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('52', 'PROD#0052', 4, 1, 12, 12, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('53', 'PROD#0053', 4, 1, 15, 15, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('54', 'PROD#0054', 4, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('55', 'PROD#0055', 4, 1, 12, 12, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('56', 'PROD#0056', 4, 1, 3, 3, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('57', 'PROD#0057', 4, 1, 16, 16, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('58', 'PROD#0058', 4, 1, 2, 2, 15, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('59', 'PROD#0059', 4, 1, 4, 4, 79, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('60', 'PROD#0060', 4, 1, 15, 15, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('61', 'PROD#0061', 4, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('62', 'PROD#0062', 4, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('63', 'PROD#0063', 4, 1, 20, 20, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('64', 'PROD#0064', 4, 1, 40, 40, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('65', 'PROD#0065', 4, 1, 14, 14, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('66', 'PROD#0066', 4, 1, 14, 14, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('67', 'PROD#0067', 4, 1, 14, 14, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('68', 'PROD#0068', 4, 1, 9, 9, 39, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('69', 'PROD#0069', 4, 1, 15, 15, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('70', 'PROD#0070', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('71', 'PROD#0071', 4, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('72', 'PROD#0072', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('73', 'PROD#0073', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('74', 'PROD#0074', 4, 1, 10, 10, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('75', 'PROD#0075', 4, 1, 11, 11, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('76', 'PROD#0076', 4, 1, 7, 7, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('77', 'PROD#0077', 4, 1, 4, 4, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('78', 'PROD#0078', 4, 1, 4, 4, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('79', 'PROD#0079', 4, 1, 4, 4, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('80', 'PROD#0080', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('81', 'PROD#0081', 4, 1, 10, 10, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('1', 'PROD#0082', 3, 1, 0.12, 0.5, 1095, NULL, 0, 0, NULL),
	('82', 'PROD#0083', 1, 1, 5, 5, 18, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('83', 'PROD#0084', 2, 1, 3, 3, 35, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('84', 'PROD#0085', 2, 1, 4, 4, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('85', 'PROD#0086', 2, 1, 3, 3, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('86', 'PROD#0087', 2, 1, 1, 1, 60, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('87', 'PROD#0088', 2, 1, 8, 8, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('88', 'PROD#0089', 2, 1, 2, 2, 99, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('89', 'PROD#0090', 2, 1, 2, 2, 20, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('90', 'PROD#0091', 2, 1, 20, 20, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('91', 'PROD#0092', 2, 1, 0.5, 0.5, 80, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('92', 'PROD#0093', 2, 1, 1, 1, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('93', 'PROD#0094', 2, 1, 1, 1, 100, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('94', 'PROD#0095', 2, 1, 25, 25, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('95', 'PROD#0096', 2, 1, 10, 10, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('96', 'PROD#0097', 2, 1, 2, 2, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('97', 'PROD#0098', 2, 1, 3, 3, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('98', 'PROD#0099', 5, 1, 1, 1, 100, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('99', 'PROD#00100', 5, 1, 0.5, 0.5, 75, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('100', 'PROD#101', 5, 1, 1, 1, 200, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('101', 'PROD#102', 5, 1, 20, 20, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('102', 'PROD#103', 5, 1, 5, 5, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('103', 'PROD#104', 5, 1, 5, 5, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('104', 'PROD#105', 5, 1, 9, 9, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('105', 'PROD#106', 5, 1, 5, 5, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('106', 'PROD#107', 5, 1, 20, 20, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('107', 'PROD#108', 5, 1, 5, 5, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('108', 'PROD#109', 5, 1, 5, 5, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('109', 'PROD#110', 5, 1, 5, 5, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('110', 'PROD#111', 5, 1, 20, 20, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('111', 'PROD#112', 5, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('112', 'PROD#113', 5, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('113', 'PROD#114', 5, 1, 10, 10, 18, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('114', 'PROD#115', 5, 1, 10, 10, 14, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('115', 'PROD#116', 5, 1, 10, 10, 11, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('116', 'PROD#117', 5, 1, 10, 10, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('117', 'PROD#118', 5, 1, 6, 6, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('118', 'PROD#119', 5, 1, 0.5, 0.5, 23, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('119', 'PROD#120', 5, 1, 0.5, 0.5, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('120', 'PROD#121', 5, 1, 2, 2, 72, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('121', 'PROD#122', 5, 1, 4, 4, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('122', 'PROD#123', 5, 1, 6, 6, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('123', 'PROD#124', 5, 1, 2, 2, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('124', 'PROD#125', 5, 1, 70, 70, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('124', 'PROD#126', 5, 1, 70, 70, 1, NULL, 0, 0, NULL),
	('125', 'PROD#127', 5, 1, 55, 55, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('126', 'PROD#128', 5, 1, 55, 55, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('127', 'PROD#129', 5, 1, 40, 40, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('128', 'PROD#130', 5, 1, 40, 40, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('129', 'PROD#131', 5, 1, 20, 20, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('130', 'PROD#132', 5, 1, 15, 15, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('131', 'PROD#133', 5, 1, 8, 8, 14, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('132', 'PROD#134', 1, 1, 0.5, 0.5, 66, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('133', 'PROD#135', 1, 1, 10, 10, 65, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('134', 'PROD#136', 1, 1, 5, 5, 91, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('135', 'PROD#137', 1, 1, 0.5, 0.5, 350, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('136', 'PROD#138', 1, 1, 0.25, 0.25, 400, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('137', 'PROD#139', 1, 1, 0.25, 0.25, 450, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('138', 'PROD#140', 1, 1, 10, 10, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('139', 'PROD#141', 1, 1, 5, 5, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('140', 'PROD#142', 1, 1, 15, 15, 60, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('141', 'PROD#143', 1, 1, 1.2, 1.2, 29, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('142', 'PROD#144', 1, 1, 2, 2, 33, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('97', 'PROD#145', 1, 1, 3, 3, 11, NULL, 0, 0, NULL),
	('143', 'PROD#146', 1, 1, 1, 1, 100, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('144', 'PROD#147', 1, 1, 1.5, 1.5, 350, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('145', 'PROD#148', 1, 1, 0.5, 0.5, 350, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('146', 'PROD#149', 1, 1, 2, 2, 30, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('147', 'PROD#150', 1, 1, 0.5, 0.5, 70, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('148', 'PROD#151', 1, 1, 1, 1, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('149', 'PROD#152', 1, 1, 2, 2, 60, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('150', 'PROD#153', 1, 1, 0.5, 0.5, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('151', 'PROD#154', 1, 1, 0.5, 0.5, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('152', 'PROD#155', 1, 1, 1.5, 1.5, 100, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('153', 'PROD#156', 1, 1, 4, 4, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('154', 'PROD#157', 1, 1, 2, 2, 20, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('155', 'PROD#158', 1, 1, 5, 5, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('156', 'PROD#159', 1, 1, 0.5, 0.5, 119, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('157', 'PROD#160', 1, 1, 5, 5, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('158', 'PROD#161', 1, 1, 1, 1, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('159', 'PROD#162', 1, 1, 1, 1, 97, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('160', 'PROD#163', 1, 1, 1.5, 1.5, 20, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('161', 'PROD#164', 1, 1, 0.5, 0.5, 46, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('162', 'PROD#165', 1, 1, 1, 1, 100, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('163', 'PROD#166', 1, 1, 1.5, 1.5, 54, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('164', 'PROD#167', 1, 1, 4, 4, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('165', 'PROD#168', 1, 1, 1.5, 1.5, 80, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('166', 'PROD#169', 1, 1, 50, 50, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('167', 'PROD#170', 1, 1, 30, 30, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('168', 'PROD#171', 1, 1, 1, 1, 40, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('169', 'PROD#172', 1, 1, 1, 1, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('170', 'PROD#173', 1, 1, 1, 1, 1300, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('171', 'PROD#174', 1, 1, 0.7, 0.75, 275, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('172', 'PROD#175', 1, 1, 1, 1, 300, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('173', 'PROD#176', 1, 1, 8, 8, 62, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('174', 'PROD#177', 1, 1, 2, 2, 109, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('175', 'PROD#178', 1, 1, 5, 5, 43, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('176', 'PROD#179', 1, 1, 20, 20, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('177', 'PROD#180', 1, 1, 0.5, 0.5, 31, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('178', 'PROD#181', 1, 1, 5, 5, 200, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('179', 'PROD#182', 1, 1, 2, 2, 280, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('180', 'PROD#183', 1, 1, 1.5, 1.5, 31, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('181', 'PROD#184', 1, 1, 1, 1, 16, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('182', 'PROD#185', 1, 1, 1, 1, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('183', 'PROD#186', 1, 1, 0.5, 0.5, 148, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('184', 'PROD#187', 1, 1, 1, 1, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('185', 'PROD#188', 1, 1, 180, 180, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('186', 'PROD#189', 3, 1, 4, 4, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('187', 'PROD#190', 3, 1, 4, 4, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('188', 'PROD#191', 3, 1, 4, 4, 18, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('189', 'PROD#192', 3, 1, 3, 3, 24, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('190', 'PROD#193', 3, 1, 5, 5, 23, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('191', 'PROD#194', 3, 1, 3, 3, 15, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('192', 'PROD#195', 3, 1, 8, 8, 11, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('193', 'PROD#196', 3, 1, 4, 4, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('194', 'PROD#197', 3, 1, 4, 4, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('195', 'PROD#198', 3, 1, 8, 8, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('196', 'PROD#199', 3, 1, 8, 8, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('197', 'PROD#200', 3, 1, 8, 8, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('141', 'PROD#201', 3, 1, 10, 10, 19, NULL, 0, 0, NULL),
	('85', 'PROD#202', 3, 1, 10, 10, 14, NULL, 0, 0, NULL),
	('198', 'PROD#203', 3, 1, 7, 7, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('199', 'PROD#204', 3, 1, 15, 15, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('200', 'PROD#205', 3, 1, 18, 18, 21, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('158', 'PROD#206', 3, 1, 12, 12, 15, NULL, 0, 0, NULL),
	('201', 'PROD#207', 3, 1, 12, 12, 15, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('202', 'PROD#208', 3, 1, 10, 10, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('203', 'PROD#209', 3, 1, 7, 7, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('204', 'PROD#210', 3, 1, 15, 15, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('205', 'PROD#211', 3, 1, 7, 7, 14, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('170', 'PROD#212', 3, 1, 5, 5, 21, NULL, 0, 0, NULL),
	('206', 'PROD#213', 3, 1, 5, 5, 21, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('207', 'PROD#214', 3, 1, 5, 5, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('208', 'PROD#215', 3, 1, 6, 6, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('209', 'PROD#216', 3, 1, 18, 18, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('210', 'PROD#217', 3, 1, 9, 9, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('211', 'PROD#218', 3, 1, 10, 10, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('212', 'PROD#219', 3, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('213', 'PROD#220', 3, 1, 60, 60, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('214', 'PROD#221', 3, 1, 6, 6, 38, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('215', 'PROD#222', 3, 1, 6, 6, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('140', 'PROD#223', 3, 1, 12, 12, 13, NULL, 0, 0, NULL),
	('216', 'PROD#224', 3, 1, 12, 12, 13, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('134', 'PROD#225', 3, 1, 6, 6, 39, NULL, 0, 0, NULL),
	('217', 'PROD#226', 3, 1, 6, 6, 39, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('218', 'PROD#227', 3, 1, 15, 15, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('219', 'PROD#228', 3, 1, 20, 20, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('220', 'PROD#229', 3, 1, 12, 12, 16, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('221', 'PROD#230', 3, 1, 15, 15, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('222', 'PROD#231', 3, 1, 20, 20, 14, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('223', 'PROD#232', 3, 1, 10, 10, 14, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('224', 'PROD#233', 3, 1, 7, 7, 12, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('225', 'PROD#234', 3, 1, 10, 10, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('226', 'PROD#235', 3, 1, 17, 17, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('227', 'PROD#236', 3, 1, 8, 8, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('228', 'PROD#237', 3, 1, 50, 50, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('229', 'PROD#238', 3, 1, 8, 8, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('230', 'PROD#239', 4, 1, 15, 15, 13, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('231', 'PROD#240', 3, 1, 15, 15, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('232', 'PROD#241', 3, 1, 10, 10, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('87', 'PROD#242', 3, 1, 10, 10, 27, NULL, 0, 0, NULL),
	('233', 'PROD#243', 3, 1, 10, 10, 27, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('234', 'PROD#244', 3, 1, 3, 3, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('235', 'PROD#245', 3, 1, 3, 3, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('236', 'PROD#246', 3, 1, 14, 14, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('237', 'PROD#247', 3, 1, 3, 3, 12, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('238', 'PROD#248', 3, 1, 15, 15, 12, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('239', 'PROD#249', 3, 1, 4, 4, 20, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('240', 'PROD#250', 3, 1, 10, 10, 23, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('241', 'PROD#251', 3, 1, 4, 4, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('242', 'PROD#252', 3, 1, 5, 5, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('243', 'PROD#253', 3, 1, 10, 10, 12, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('244', 'PROD#254', 3, 1, 15, 15, 13, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('245', 'PROD#255', 3, 1, 3, 3, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('246', 'PROD#256', 3, 1, 15, 15, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('247', 'PROD#257', 3, 1, 10, 10, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('248', 'PROD#258', 3, 1, 10, 10, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('249', 'PROD#259', 3, 1, 8, 8, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('250', 'PROD#260', 3, 1, 12, 12, 12, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('251', 'PROD#261', 3, 1, 20, 20, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('252', 'PROD#262', 3, 1, 6, 6, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('253', 'PROD#263', 3, 1, 10, 10, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('254', 'PROD#264', 3, 1, 10, 10, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('255', 'PROD#265', 3, 1, 10, 10, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('256', 'PROD#266', 3, 1, 6, 6, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('257', 'PROD#267', 3, 1, 10, 10, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('258', 'PROD#268', 3, 1, 30, 30, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('259', 'PROD#269', 3, 1, 10, 10, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('260', 'PROD#270', 3, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('261', 'PROD#271', 3, 1, 10, 10, 4, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('262', 'PROD#272', 3, 1, 8, 8, 11, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('263', 'PROD#273', 3, 1, 7, 7, 2, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('264', 'PROD#274', 3, 1, 5, 5, 36, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('265', 'PROD#275', 3, 1, 10, 10, 24, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('266', 'PROD#276', 3, 1, 20, 20, 23, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('267', 'PROD#277', 3, 1, 15, 15, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('268', 'PROD#278', 3, 1, 35, 35, 15, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('269', 'PROD#279', 3, 1, 20, 20, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('270', 'PROD#280', 3, 1, 8, 8, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('138', 'PROD#281', 3, 1, 15, 15, 18, NULL, 0, 0, NULL),
	('271', 'PROD#282', 3, 1, 15, 15, 18, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('272', 'PROD#283', 3, 1, 5, 5, 22, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('273', 'PROD#284', 3, 1, 6, 6, 23, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('274', 'PROD#285', 3, 1, 6, 6, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('88', 'PROD#286', 3, 1, 8, 8, 19, NULL, 0, 0, NULL),
	('275', 'PROD#287', 3, 1, 8, 8, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('276', 'PROD#288', 3, 1, 5, 5, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('277', 'PROD#289', 3, 1, 5, 5, 20, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('159', 'PROD#290', 3, 1, 6, 6, 19, NULL, 0, 0, NULL),
	('278', 'PROD#291', 3, 1, 6, 6, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('279', 'PROD#292', 3, 1, 10, 10, 7, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('280', 'PROD#293', 3, 1, 6, 6, 26, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('281', 'PROD#294', 3, 1, 10, 10, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('282', 'PROD#295', 3, 1, 25, 25, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('283', 'PROD#296', 3, 1, 15, 15, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('284', 'PROD#297', 3, 1, 30, 30, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('285', 'PROD#298', 3, 1, 6, 6, 60, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('286', 'PROD#299', 3, 1, 8, 8, 19, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('287', 'PROD#300', 3, 1, 4, 4, 10, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('288', 'PROD#301', 3, 1, 12, 12, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('289', 'PROD#302', 3, 1, 1, 1, 25, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('290', 'PROD#303', 3, 1, 20, 20, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('291', 'PROD#304', 5, 1, 3, 3, 17, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('292', 'PROD#305', 3, 1, 4, 4, 8, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('293', 'PROD#306', 5, 1, 4, 4, 9, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('294', 'PROD#307', 5, 1, 3, 3, 50, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('295', 'PROD#308', 5, 1, 20, 20, 6, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('296', 'PROD#309', 11, 1, 50, 50, 5, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('297', 'PROD#310', 11, 1, 100, 100, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('298', 'PROD#311', 11, 1, 100, 100, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('299', 'PROD#312', 11, 1, 600, 600, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('300', 'PROD#313', 11, 1, 700, 700, 1, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('301', 'PROD#314', 11, 1, 70, 70, 3, NULL, 0, 0, 'uploads/product/default_logo.png'),
	('302', 'PROD#315', 1, 1, 1.5, 2, 1550, NULL, 0, 0, 'uploads/product/default_logo.png');
/*!40000 ALTER TABLE `product_details` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_suppliers
CREATE TABLE IF NOT EXISTS `product_suppliers` (
  `sup_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `addr` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `loc` varchar(50) NOT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `prod_type` text NOT NULL,
  PRIMARY KEY (`sup_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_suppliers: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_suppliers` DISABLE KEYS */;
INSERT INTO `product_suppliers` (`sup_id`, `name`, `tel1`, `tel2`, `addr`, `email`, `loc`, `pay_type`, `prod_type`) VALUES
	(1, 'vendor', '0000000000', '0000000000', 'mall', 'mall@fotostore.com', 'mall', 'cash', '5x5.8 inch');
/*!40000 ALTER TABLE `product_suppliers` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_suppliers_accounts
CREATE TABLE IF NOT EXISTS `product_suppliers_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_id` int(11) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `num` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_suppliers_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_suppliers_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_suppliers_accounts` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.product_transact
CREATE TABLE IF NOT EXISTS `product_transact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `prod_id` varchar(50) NOT NULL,
  `qty_withdrawn` varchar(50) NOT NULL,
  `unit_price` varchar(50) NOT NULL,
  `tot_cost` double NOT NULL DEFAULT '0',
  `tax` double DEFAULT NULL,
  `sold_by` varchar(150) NOT NULL,
  `customer` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_withdrawn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.product_transact: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_transact` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_transact` ENABLE KEYS */;

-- Dumping structure for view hpx703cr_packa-pharma.report_dailyledger
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `report_dailyledger` (
	`Autoid` INT(11) NOT NULL,
	`Description` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`CostPrice` FLOAT NULL,
	`Qty_Bought` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`SellingPrice` FLOAT NOT NULL,
	`T_Cost` DOUBLE NOT NULL,
	`Tax` DOUBLE NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for table hpx703cr_packa-pharma.report_purchaseorder
CREATE TABLE IF NOT EXISTS `report_purchaseorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_id` int(11) NOT NULL,
  `invoice` int(5) NOT NULL,
  `tot_cost` float NOT NULL,
  `products` text NOT NULL,
  `quantity` text NOT NULL,
  `prices` text NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `pay` float NOT NULL,
  `bal` float NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.report_purchaseorder: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_purchaseorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_purchaseorder` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.reserved_license_keys
CREATE TABLE IF NOT EXISTS `reserved_license_keys` (
  `license_id` int(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `end_date` datetime NOT NULL,
  PRIMARY KEY (`license_id`,`key`,`end_date`),
  UNIQUE KEY `UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.reserved_license_keys: ~0 rows (approximately)
/*!40000 ALTER TABLE `reserved_license_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserved_license_keys` ENABLE KEYS */;

-- Dumping structure for table hpx703cr_packa-pharma.reserved_registered_keys
CREATE TABLE IF NOT EXISTS `reserved_registered_keys` (
  `reg_key_id` int(20) NOT NULL AUTO_INCREMENT,
  `license_id` int(20) NOT NULL,
  `client_id` int(11) NOT NULL,
  `state` char(1) NOT NULL,
  PRIMARY KEY (`reg_key_id`),
  UNIQUE KEY `UNIQUE` (`license_id`,`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.reserved_registered_keys: ~0 rows (approximately)
/*!40000 ALTER TABLE `reserved_registered_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserved_registered_keys` ENABLE KEYS */;

-- Dumping structure for view hpx703cr_packa-pharma.suppliers_full_details
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `suppliers_full_details` (
	`sup_id` INT(11) NOT NULL,
	`name` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`addr` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`loc` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`tel1` VARCHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tel2` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`email` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`prod_type` TEXT NOT NULL COLLATE 'latin1_swedish_ci',
	`pay_type` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`bank` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`branch` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci',
	`acctname` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci',
	`num` VARCHAR(25) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for table hpx703cr_packa-pharma.tax_system
CREATE TABLE IF NOT EXISTS `tax_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `percentage` double NOT NULL,
  `getfund` float NOT NULL,
  `nhil` float NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hpx703cr_packa-pharma.tax_system: ~0 rows (approximately)
/*!40000 ALTER TABLE `tax_system` DISABLE KEYS */;
INSERT INTO `tax_system` (`id`, `percentage`, `getfund`, `nhil`, `status`, `date_created`) VALUES
	(1, 12.5, 2.5, 2.5, 'active', '2018-01-16 11:44:20');
/*!40000 ALTER TABLE `tax_system` ENABLE KEYS */;

-- Dumping structure for view hpx703cr_packa-pharma.vw_product_transact
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_product_transact` (
	`id` INT(11) NOT NULL,
	`description` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`prod_id` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`qty_withdrawn` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`unit_price` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`tot_cost` DOUBLE NOT NULL,
	`tax` DOUBLE NULL,
	`status` ENUM('active','inactive') NOT NULL COLLATE 'latin1_swedish_ci',
	`date_withdrawn` DATETIME NOT NULL,
	`employee_id` VARCHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`tax_value` DOUBLE NULL
) ENGINE=MyISAM;

-- Dumping structure for view hpx703cr_packa-pharma.v_account_details
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_account_details` (
	`Id` INT(11) NOT NULL,
	`Name` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`Refcon` VARCHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`Descrip` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`Debit_Amt` INT(11) NULL,
	`Credit_Amt` INT(11) NULL,
	`Balance` BIGINT(12) NULL
) ENGINE=MyISAM;

-- Dumping structure for view hpx703cr_packa-pharma.access_user_details
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `access_user_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `access_user_details` AS select `access_users`.`id` AS `id`,`access_users`.`username` AS `Username`,`access_users`.`passwd` AS `Password`,`access_users`.`active` AS `Account_Status`,`hr_emp_pers_info`.`employee_id` AS `Employee_id`,`access_roles_priviledges_user`.`roles` AS `User_Roles`,`access_roles_priviledges_user`.`priviledges` AS `User_Priviledges`,`access_roles_priviledges_user`.`group_id` AS `Group_ID`,`access_roles_priviledges_group`.`group_name` AS `Group_Name`,`access_roles_priviledges_group`.`roles` AS `Group_Roles`,`access_roles_priviledges_group`.`priviledges` AS `Group_Priviledges` from (((`access_users` left join `hr_emp_pers_info` on((`hr_emp_pers_info`.`id` = `access_users`.`id`))) left join `access_roles_priviledges_user` on((`access_roles_priviledges_user`.`employee_id` = `hr_emp_pers_info`.`id`))) left join `access_roles_priviledges_group` on((`access_roles_priviledges_group`.`group_id` = `access_roles_priviledges_user`.`group_id`)));

-- Dumping structure for view hpx703cr_packa-pharma.full_product_details
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `full_product_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `full_product_details` AS select `product_codes`.`prod_name` AS `Item_Name`,`product_details`.`product_id` AS `ProductCode`,`product_desc`.`desc` AS `Description`,`product_details`.`unit_qty` AS `Unit_Qty`,`product_details`.`unit_price` AS `Unit_Price`,`product_details`.`cost_price` AS `Cost_Price`,`product_details`.`promo_qty` AS `Promo_Qty`,`product_details`.`promo_price` AS `Promo_Price`,`product_details`.`avail_qty` AS `Current_Qty`,`product_details`.`expiry` AS `Expiry`,`product_codes`.`prod_id` AS `Product_ID`,`product_desc`.`desc_id` AS `DescriptionID` from ((`product_codes` join `product_details` on((`product_details`.`prod_id` = `product_codes`.`prod_id`))) join `product_desc` on((`product_desc`.`desc_id` = `product_details`.`desc_id`)));

-- Dumping structure for view hpx703cr_packa-pharma.report_dailyledger
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `report_dailyledger`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `report_dailyledger` AS (select `product_transact`.`id` AS `Autoid`,`product_transact`.`description` AS `Description`,`product_details`.`cost_price` AS `CostPrice`,`product_transact`.`qty_withdrawn` AS `Qty_Bought`,`product_details`.`unit_price` AS `SellingPrice`,(`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) AS `T_Cost`,((`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) * 0.175) AS `Tax` from (`product_transact` join `product_details` on((`product_details`.`prod_id` = `product_transact`.`prod_id`))));

-- Dumping structure for view hpx703cr_packa-pharma.suppliers_full_details
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `suppliers_full_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `suppliers_full_details` AS (select `product_suppliers`.`sup_id` AS `sup_id`,`product_suppliers`.`name` AS `name`,`product_suppliers`.`addr` AS `addr`,`product_suppliers`.`loc` AS `loc`,`product_suppliers`.`tel1` AS `tel1`,`product_suppliers`.`tel2` AS `tel2`,`product_suppliers`.`email` AS `email`,`product_suppliers`.`prod_type` AS `prod_type`,`product_suppliers`.`pay_type` AS `pay_type`,`product_suppliers_accounts`.`bank` AS `bank`,`product_suppliers_accounts`.`branch` AS `branch`,`product_suppliers_accounts`.`name` AS `acctname`,`product_suppliers_accounts`.`num` AS `num` from (`product_suppliers` left join `product_suppliers_accounts` on((`product_suppliers_accounts`.`sup_id` = `product_suppliers`.`sup_id`))));

-- Dumping structure for view hpx703cr_packa-pharma.vw_product_transact
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_product_transact`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_product_transact` AS select `a`.`id` AS `id`,`a`.`description` AS `description`,`a`.`prod_id` AS `prod_id`,`a`.`qty_withdrawn` AS `qty_withdrawn`,`a`.`unit_price` AS `unit_price`,`a`.`tot_cost` AS `tot_cost`,`a`.`tax` AS `tax`,`a`.`status` AS `status`,`a`.`date_withdrawn` AS `date_withdrawn`,`a`.`employee_id` AS `employee_id`,((`a`.`tax` * `a`.`tot_cost`) / 100) AS `tax_value` from `product_transact` `a`;

-- Dumping structure for view hpx703cr_packa-pharma.v_account_details
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_account_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_account_details` AS select `account_types`.`id` AS `Id`,`account_types`.`acc_name` AS `Name`,`account_types`.`acc_ref_no` AS `Refcon`,`account_types`.`acc_desc` AS `Descrip`,`accounts_transactions`.`debit_amt` AS `Debit_Amt`,`accounts_transactions`.`credit_amt` AS `Credit_Amt`,(`accounts_transactions`.`credit_amt` - `accounts_transactions`.`debit_amt`) AS `Balance` from (`account_types` left join `accounts_transactions` on((`accounts_transactions`.`acc_type_id` = `account_types`.`id`)));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
