  <div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#home" aria-controls="home" role="tab" data-toggle="tab">All Cash Account</a>
                </li>
                <li role="presentation">
                  <a href="#account" aria-controls="account" role="tab" data-toggle="tab">Accounts</a>
                </li>
                <li role="presentation">
                  <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Create New</a>
                </li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body no-padding">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Name</th>
                                  <th>Ref#</th>
                                  <th>Description</th>
                                  <th>Debit(GHȻ)</th>
                                  <th>Credit(GHȻ)</th>
                                  <th>Balance(GHȻ)</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php if(!empty($acc_summary)) : $counter=1; foreach($acc_summary As $accouninfo) : ?>
                              <tr>
                                  <td><?= $counter ?></td>
                                  <td><?= $accouninfo['name'] ?></td>
                                  <td><?= $accouninfo['refcode'] ?></td>
                                  <td><?= substr($accouninfo['desc'], 0,15) ?></td>
                                  <td><?= $accouninfo['debit'] ?></td>
                                  <td><?= $accouninfo['credit'] ?></td>
                                  <td><?= $accouninfo['balance'] ?></td>
                                  <td><button class="btn btn-info btn-xs viewcash" data-id="<?=$accouninfo['id']?>"><i class="fa fa-th"></i> Details</button></td>
                              </tr>
                            <?php $counter++; endforeach; endif; ?>
                          </tbody>
                        </table> 
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="account">
                  <div class="row">
                    <div class="col-xs-12">
                      <form action="<?= base_url() ?>Dashboard/Save_Expenses" method="post">
        <div class="modal-body">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <label class="control-label">Select Account</label>
                  <select class="form-control" name="acc_type" data-placeholder="--- Select One ---" style="height:44px" onchange="fetch_acct_desc(this.value)" required>
                    <option></option>

                    <?php if(!empty($accounts)) :  foreach($accounts As $acc) :?>
                    <option value="<?= base64_encode($acc->id) ?>"><?= $acc->acc_name ?></option>
                    <?php endforeach; endif; ?>

                  </select>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Refenece Code</label>
                    <input type="text" class="form-control acct_code" readonly>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Account Description</label>
                    <input type="text" class="form-control acct_desc" readonly>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Debit Amount</label>
                    <input type="text" class="form-control" name="deb_amt">
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Credit Amount</label>
                    <input type="text" class="form-control" name="cred_amt">
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Purpose</label>
                    <input type="text" class="form-control" name="purpose" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="resulturl" id="resulturl">
        <center>
          <div class="modal-footer" style="background-color:rgba(90, 61, 61, 0.13)!important;">
            <button type="submit" class="btn btn-sm btn-success" name="save_expenses" ><i class="fa fa-database"></i> Save</button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </center>
      </form>
                    </div>
                  </div>
                </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <div class="col-md-5">
            <form action="<?= base_url() ?>BackOffice/Create_Account" method="post">
              <div class="form-group">
                <label class="control-label">Ref# Code</label>
                <input type="text" class="form-control" placeholder="" value="<?= @$ref_code ?>" name="ref_code" required readonly>
              </div>
              <div class="form-group">
                <label class="control-label">Account's Name</label>
                <input type="text" class="form-control" placeholder="" name="acc_name" required>
              </div>
              <div class="form-group">
                <label class="control-label">Description</label>
                <textarea style="resize:none" class="form-control" placeholder="" name="description"> </textarea>
              </div>
              <div class="form-group">
                <label class="control-label">Tax</label>
                <div class="checkbox">
                <input type="checkbox" id="role_statistics" name="tax" value="Yes">
                <label for="role_statistics">
                    Yes Enable Tax 
                </label>
              </div>
              </div>
              <center>
                <div class="form-footer">
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="cash_create" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger">Cancel</button>
                    </div>
                  </div>
                </div>
              </center>
            </form>
          </div>
        <div class="col-md-12" >
          
        </div>
        </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages"> </div>
        <div role="tabpanel" class="tab-pane" id="settings"></div>
    </div>
</div>
    </div>
  </div>

  </div>
</div>

  </div>