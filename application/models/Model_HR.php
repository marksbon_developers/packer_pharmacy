<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Model_HR extends CI_Model 
{
  /******************************** Data Retrieval ******************************************/
    /***********************************************
      Employee Info
    ************************************************/
    public function employee_personal_data($employee_autoId)
    {
      $tablename = "hr_emp_pers_info";
      
      $where = array('id'=>$employee_autoId);
      
      $query = $this->db->get_where($tablename,$where);
      
      return(($query->num_rows() == 1) ? $query->row() : FALSE ); 
    }

  /******************************** Data Retrieval ******************************************/

  /******************************** Data Insertion ******************************************/
		
  	/***********************************************
        Last Employee ID
    ************************************************/
    public function last_employee_id_increase($lastid) 
    {
      $tablename = "general_last_ids";

      $this->db->set('employee_id',$lastid);

      $this->db->where('id','1');
          
      $query = $this->db->update($tablename);

      $result = $this->db->affected_rows(); 
        
      return(($result > 0) ? true : false);
    }

    /***********************************************
  			Register Company
  	************************************************/
  	public function register_comp($data) 
    {
      $query = $this->db->count_all('companyinfo');
          
          if($query <= 0 ) {

  		$query = $this->db->insert('companyinfo',$data);
  		
  		return (($query) ? true : false );
          
          } else {
              
              $_SESSION['form_link'] = "Edit_Companyinfo";
              return false;
          }
          
  	}

    /***********************************************
            User Roles
    ************************************************/
    public function set_user_roles_priviledges($data){
        
        $tablename = "user_roles_priviledges";
        
        $this->db->where('employee_id',$data['employee_id'] );

        $query = $this->db->update($tablename, $data);
        
        $result = $this->db->affected_rows(); 
        
        return(($result > 0) ? true : false);
        
    }

    
/*********************************************************************************************************************************************
****************************************************** Data Retrival **************************************************************************
***********************************************************************************************************************************************/
    
    /***********************************************
			Retrieve Company Info
	  ************************************************/
    public function retrieve_companyinfo()
    {
      $tablename = "companyinfo";
      
    }

    /***********************************************
            Retrieving Last Employee ID
    ************************************************/
    public function retrieve_emp_id()
    {
      $this->db->select('employee_id');

      $this->db->order_by('employee_id','DESC');

      $query = $this->db->get('employees');
        
      return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

    /***********************************************
            Retrieving Last Employee Temp ID
    ************************************************/
    public function ret_last_employee_id()
    {
      $tablename = "general_last_ids";

      $query = $this->db->get($tablename);
       
      return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

/***********************************************************************************************************************************************
****************************************************** Data Edit+Update *********************************************************
***********************************************************************************************************************************************/
  /***********************************************
		Employee Info Update
	************************************************/
  public function Update_Employee_info($data,$id)
  {
    $tablename = "hr_emp_pers_info";

    $this->db->where('id',$id );

    $query = $this->db->update($tablename, $data);

    $result = $this->db->affected_rows();
        
    return(($result > 0) ?  TRUE :  FALSE);
  }
    
  /***********************************************
			Update Company Info
	************************************************/
  public function update_companyinfo($data,$id) 
  {
    $tablename = "companyinfo";

    $this->db->where('id',$id );

    $query = $this->db->update($tablename, $data);

    $result = $this->db->affected_rows();
        
    return(($result > 0) ?  true :  false);
  }

    
/*******************************************************************************************************************************************
********************************************************** Data Delete *********************************************************************
********************************************************************************************************************************************/

    /***********************************************
            Deleting Department
    ************************************************/
    
    public function delete_department($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('departments');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /***********************************************
            Deleting Site
    ************************************************/
    
    public function delete_site($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('sites');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /***********************************************
            Deleting Position
    ************************************************/
    
    public function delete_position($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('position');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /*******************************************************************************************************************************************************/
    /*******************************************************************************************************************************************************/

    
	
}//End of class
