<head>
    <title>Invoice</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

    <!-- Theme -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">

</head>

<!--onload="possubmit()"-->
<body>
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-4 invoice-col">
                    <!-- <h2 class="page-header"> -->
                    <img src="<?= base_url() . @$_SESSION['company_logo'] ?>"
                         style="margin-left:0em;width:150px;height:60px;" class="">
                    </h2>
                    <!--</div>-->
                </div>
                <div class="col-xs-4 invoice-col">
                    <label style="font-weight: 600">TIN : </label> C0005374863 <br>
                    <label style="font-weight: 600">REG No: </label> CA-20829<br>
                    <label style="font-weight: 600">Inv
                        #: </label> <?= str_pad(@$printpreview['last_id'] + 1, 6, "0", STR_PAD_LEFT) ?>
                </div>
                <div class="col-xs-4 invoice-col">
                    <address style="height:60px;">
                        <strong><?= @$_SESSION['company_name'] ?></strong><br>
							  <?= @$_SESSION['company_address'] ?><br>
							  <?= @$_SESSION['company_location'] ?><br>
							  <?= @$_SESSION['company_tel'] ?> /
							  <?= @$_SESSION['company_alttel'] ?><br>
                    </address>
                </div><!-- /.col -->

            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <hr style="margin: 0px 0px 5px 0px">
        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12">
                <table class="table-striped primary" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Unit Price</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
						  <?php
						  if (!empty($printpreview)) :
							  $counter = 1;
							  for ($a = 0; $a < sizeof($printpreview['prodname']); $a++) :
								  ?>
                           <tr>
                               <td><?= $counter ?></td>
                               <td><?= $printpreview['prodname'][$a] ?></td>
                               <td><?= $printpreview['prodqty'][$a] ?><br/></td>
                               <td><?php
											 $total_tax = 1 + ($_SESSION['settings']['vat'] + $_SESSION['settings']['getfund'] + $_SESSION['settings']['nhil']) / 100;
											 $new_unit_price = number_format($printpreview['produnit'][$a] / $total_tax, 2);
											 print $new_unit_price;
											 ?>
                               </td>
                               <td><?php
											 $sum_of_subtotals[] = $printpreview['prodqty'][$a] * $new_unit_price;
											 print $printpreview['prodqty'][$a] * $new_unit_price;
											 ?></td>
                           </tr>
								  <?php
								  $counter++;
							  endfor;
						  endif;
						  $sum_of_unit_prices = array_sum($sum_of_subtotals);
						  ?>
                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->
        <hr>
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-4">
                <div class="table-responsive">
                    <div class="col-xs-12 invoice-col">
                        <b>Billed to: </b><?= $printpreview['customer'] ?><br>
                        <b>Sold by: </b> <?= $printpreview['sold_by'] ?><br>
                        <!-- <?= @$_SESSION['company_email'] ?><br>
              <?= @$_SESSION['company_website'] ?><br> -->
                        <b>Date:</b> <?= gmdate("d F, Y") ?>
                    </div><!-- /.col -->
                </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <div class="pull-right">
                    <table class="">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>GHȻ <?= $sum_of_units = round($sum_of_unit_prices, 2) ?></td>
                        </tr>
                        <tr>
                            <th>GetFund + NHIL <!-- (<?= $_SESSION['settings']['tax'] ?>%) --></th>
                            <td>GHȻ
										 <?php
										 $value = ($_SESSION['settings']['getfund'] + $_SESSION['settings']['nhil']) / 100;
										 $getfund_nhil_levy = round($value * round($sum_of_unit_prices, 2), 2);
										 $taxable_value = $sum_of_units + $getfund_nhil_levy;
										 print $getfund_nhil_levy;
										 ?></td>
                        </tr>
                    </table>
                </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <div class="pull-right">
                    <table class="">
                        <tr>
                            <th>Taxable Amt</th>
                            <td>GHȻ <?= $taxable_value ?></td>
                        </tr>
                        <tr>
                            <th>VAT <!-- (<?= $_SESSION['settings']['tax'] ?>%) --></th>
                            <td>GHȻ
										 <?= $vat = round($taxable_value * $_SESSION['settings']['vat'] / 100, 2) ?></td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>GHȻ <?= number_format(floor($taxable_value + $vat), 2) ?></td>
                        </tr>
                    </table>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- ./wrapper -->

<!-- AdminLTE App -->
<script type="text/javascript">
    /*window.onafterprint = function () {
        var url = '<?=base_url()?>dashboard/pos';
        window.location = url;
    };*/
    //window.print();
</script>
</body>
