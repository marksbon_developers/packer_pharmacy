<!-- Modals -->
<div class="modal fade" id='checkoutmodal' role='dialog' aria-hidden='true' >
   <div class="modal-dialog" style="width: 700px">
      <div class="modal-content">
         <form action="<?= base_url() ?>dashboard/posprint" method="post" id="checkout_confirmed">
            <div class="modal-header" style="padding: 15px">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
               <h4 class="modal-title">Confirm Checkout </h4>
            </div>
            <div class="modal-body" id="checkouttable"></div>
            <div class="modal-footer">
               <div class="row">
                  <div class="col col-md-3">
                     <input type="text" name="sold_by" class="form-control" placeholder="Sold By" required style="background: white">
                  </div>
                  <div class="col col-md-4" style="margin-left: -20px">
                     <input type="text" name="customer_name" class="form-control" placeholder="Customer Name" required style="background: white">
                  </div>
                  <div class="col col-md-2" style="margin-left: -20px">
                     <select class="form-control" name="paymentmode" style="height: 45px;" id="paymentmode" required>
                        <option value="cash" selected>Cash</option>
                        <option value="momo">MoMo</option>
                     </select>
                  </div>
                   <div class="col col-md-4" id="amountTxtBox" style="margin-left: -20px;">
                       <input type="number" name="cash_received" min="0" class="form-control cash_received" placeholder="Amount Paid" step="0.01" required style="background: white">
                   </div>
                   <div class="col col-md-4 momoNumTxtBox" style="display: none; margin-left: -20px;">
                       <select class="form-control" name="paymentmode" style="height: 45px;" id="paymentmode">
                           <option value="cash" selected>MTN</option>
                           <option value="momo">TIGO</option>
                           <option value="momo">VODAFONE</option>
                       </select>
                   </div>
                   <div class="col col-md-4 momoNumTxtBox" style="display: none; margin-left: -20px;">
                       <input type="text" name="momoNum" class="form-control cash_received" placeholder="Mobile No." style="background: white">
                   </div>
                   <div class="col col-md-3">
                       <p class="pull-right"><small>Total: </small> GHȻ <b class="checkoutamt" style="font-size: 22px">0.00</b></p>
                   </div>
                  <div class="col col-md-3">
                     <div class="value"><p class="pull-left" ><small>Bal : </small> GHȻ <b class="changeamt" style="font-size: 20px">0.00</b> </p></div>
                  </div>
               </div>
               <!--<div class="row" style="display: block" id="momoVerifyBtn">
                  <div class="col-md-offset-3">
                     <button class="btn btn-success" type="button" id="verifyBtn"><i class="fa fa-check"></i> Request MoMo</button>
                  </div>
               </div>-->
                <div class="row">
                    <div class="col-md-8">
                        <button class="btn btn-success" type="submit" id="confirmcheckout" onclick="document.getElementById('confirmcheckout').disabled = true;document.getElementById('confirmcheckout').style.opacity='0.5';"><i class="fa fa-check"></i> Checkout</button>
                        <button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                    <div class="col-md-3 verifyMomoBtn" style="display: none; margin-left: -20px">
                        <!--<button class="btn btn-success" type="button" id="verifyBtn"><i class="fa fa-check"></i> Request MoMo</button>-->
                        <div class="mobile-money-qr-payment"
                             data-api-user-id="2a903d3f-9a94-43ac-b385-638884651227"
                             data-amount="10"
                             data-currency="EUR"
                             data-external-id="Receipt-2112">
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="tot_cost" id="totcost">
            <input type="hidden" id="change" name="change">
         </form>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div>