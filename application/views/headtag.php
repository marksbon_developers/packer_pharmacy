<!DOCTYPE html>
<html>

<head>
    <title><?= $title ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>resources/images/favicon.png">
    <!-- Theme -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/js/datepicker/daterangepicker.css">

	<!-- MTN Collection Widget-->

	<script>
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
// add a zero in front of numbers<10<br>
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function () {
                startTime()
            }, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        function displayDate() {
            document.getElementById("demo").innerHTML = Date();
        }
    </script>
    <style>
        #poscategory {
            font-size: 25px !important;
            font-weight: 900;
            padding-left: 30%;
            margin-top: -15px;
            color: rgb(84 170 114 / 75%)
        }
        #poscategory:hover{
            color: white;
            font-style: italic;
        }
    </style>
</head>

<body onload="startTime()">
<div class="app app-default">
	<?php include_once "nav.php"; ?>

    <script type="text/ng-template" id="sidebar-dropdown.tpl.html">
        <div class="dropdown-background">
            <div class="bg"></div>
        </div>
        <div class="dropdown-container">
            {{list}}
        </div>
    </script>

    <div class="app-container">
		 <?php include_once "header.php"; ?>

