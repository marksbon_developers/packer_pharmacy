<script type="text/javascript">
	function fetch_roles(userid)
      {
        if(userid)
        {
          $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>access/setroles_retrieve',
            data: {userid: userid},
            success: function(roles_response){ console.log(roles_response);
              if(roles_response)
              {
                  roles_exploded = roles_response.split('|');
                  
                  if($.inArray('STATISTICS',roles_exploded) != '-1')
                    $('#role_statistics').prop('checked', true);
                  else
                    $('#role_statistics').prop('checked', false);
                  
                  if($.inArray('POS',roles_exploded) != '-1')
                    $('#role_pos').prop('checked', true);
                  else
                    $('#role_pos').prop('checked', false);
                  
                  if($.inArray('SUPCUST',roles_exploded) != '-1')
                    $('#role_sup_cust').prop('checked', true);
                  else
                    $('#role_sup_cust').prop('checked', false);
                  
                  if($.inArray('PROD',roles_exploded) != '-1')
                    $('#role_prod').prop('checked', true);
                  else
                    $('#role_prod').prop('checked', false);
                  
                  if($.inArray('CASH',roles_exploded) != '-1')
                    $('#role_cash').prop('checked', true);
                  else
                    $('#role_cash').prop('checked', false);
                  
                  if($.inArray('USERS',roles_exploded) != '-1')
                    $('#role_user').prop('checked', true);
                  else
                    $('#role_user').prop('checked', false);
                  
                  if($.inArray('ROLES',roles_exploded) != '-1')
                    $('#role_priv').prop('checked', true);
                  else
                    $('#role_priv').prop('checked', false);
                  
                  if($.inArray('REPORT',roles_exploded) != '-1')
                    $('#role_rep').prop('checked', true);
                  else
                    $('#role_rep').prop('checked', false);

                  if($.inArray('ORDER',roles_exploded) != '-1')
                    $('#role_order').prop('checked', true);
                  else
                    $('#role_order').prop('checked', false);
                  
                  if($.inArray('SETTINGS',roles_exploded) != '-1')
                    $('#role_set').prop('checked', true);
                  else
                    $('#role_set').prop('checked', false);

                  if($.inArray('INV',roles_exploded) != '-1')
                    $('#role_iledger').prop('checked', true);
                  else
                    $('#role_iledger').prop('checked', false);

                  if($.inArray('GNL',roles_exploded) != '-1')
                    $('#role_gledger').prop('checked', true);
                  else
                    $('#role_gledger').prop('checked', false);
              }
              else 
              {
                $('#norolesmodal').modal('show');
              }
            }   
          });
        }
        else
        {
          $('#role_statistics').prop('checked', false);
          $('#role_pos').prop('checked', false);
          $('#role_sup_cust').prop('checked', false);
          $('#role_prod').prop('checked', false);
          $('#role_cash').prop('checked', false);
          $('#role_user').prop('checked', false);
          $('#role_priv').prop('checked', false);
          $('#role_rep').prop('checked', false);
          $('#role_set').prop('checked', false);
        }
      }
</script>

<!-- No Roles Modal -->
<div class="modal fade" id='norolesmodal' role='dialog' aria-hidden='true' >
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title">No Roles Set For User</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- No Roles Modal-->