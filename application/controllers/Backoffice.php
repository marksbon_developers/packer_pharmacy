<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BackOffice extends CI_Controller
{

	/************************************ Constructor ******************************/
	/*******************************
	 * Stores Index
	 *******************************/
	public function index()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			# Redirect to Dashboard
			redirect('Dashboard');
		} else {
			# Redirect to Access
			redirect('Access');
		}
	}

	/***************************************   Interfaces   ***********************************/
	/******************************
	 * Register Product
	 *******************************/
	public function registerproduct()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded'])) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_Stock');

				# Extracting Data For Display
				$whereCondition = ['status != ' => 'deleted'];
				$data['Description_info'] = $this->Universal_Retrieval->ret_data_with_s_cond_where('product_desc', $whereCondition);

				#################### Generating New Product Code ####################
				$list = ['id' => 1];

				$last_product_code = $this->Universal_Retrieval->ret_data_with_s_cond_row('general_last_ids', 'id', $list);

				if (!empty($last_product_code)) {
					if ($last_product_code->product_code == NULL || $last_product_code->product_code == 0)
						$data['next_product_code'] = "PROD#0001";

					elseif (strlen($last_product_code->product_code) == 1)
						$data['next_product_code'] = "PROD#000" . ($last_product_code->product_code + 1);

					elseif (strlen($last_product_code->product_code) == 2)
						$data['next_product_code'] = "PROD#00" . ($last_product_code->product_code + 1);

					elseif (strlen($last_product_code->product_code) >= 3)
						$data['next_product_code'] = "PROD#" . ($last_product_code->product_code + 1);
				} else {
					$data['next_product_code'] = "PROD#0001";
				}

				/********** Interface ***********************/
				$headertag['title'] = "Product Register";
				$this->load->view('headtag', $headertag);
				$this->load->view('productregister', $data);
				$this->load->view('backoffice/manageproduct_modals');
				$this->load->view('footer');
				$this->load->view('backoffice/manageproduct_pagesettings');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/******************************
	 * Vender
	 *******************************/
	public function Vendor()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');

				# Extracting Data For Display
				$data['allsuppliers'] = $this->Universal_Retrieval->All_Info('suppliers_full_details');
				$data['category_info'] = $this->Universal_Retrieval->All_Info('product_category');

				/********** Interface ***********************/
				$headertag['title'] = "Product Vendors";
				$this->load->view('headtag', $headertag);
				$this->load->view('Vender', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/******************************
	 * Vender
	 *******************************/
	public function invoiceprint()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				$this->form_validation->set_rules('prepared_by', 'Prepared By', 'trim|required');
				$this->form_validation->set_rules('sup_id', 'Supplier', 'trim');
				$this->form_validation->set_rules('inv_no', 'Invoice No.', 'trim|required');
				$this->form_validation->set_rules('tot_cost', 'Total Cost', 'trim');
				$this->form_validation->set_rules('productname[]', 'Product Name', 'trim|required');
				$this->form_validation->set_rules('qty[]', 'Quanity', 'trim');
				$this->form_validation->set_rules('price[]', 'Price', 'trim|required');
				$this->form_validation->set_rules('total[]', 'Price', 'trim');
				$this->form_validation->set_rules('remarks', 'Remarks', 'trim');
				$this->form_validation->set_rules('pay', 'Pay', 'trim');
				$this->form_validation->set_rules('bal', 'Balance', 'trim');

				# If Failed
				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('backoffice/order');
				} # If Passed
				else {
					# Loading models...
					$this->load->model('Universal_Retrieval');

					foreach ($this->input->post('productname[]') As $prod) :
						$tablename = 'product_codes';
						$IdField = 'prod_name';
						$data = ['prod_name' => $prod];
						$prod_id_ret = $this->Universal_Retrieval->ret_data_with_s_cond_row($tablename, $IdField, $data);
						@$comb_prod_id .= $prod_id_ret->prod_id . "|";
					endforeach;

					foreach ($this->input->post('qty[]') As $quan) :
						@$quantity .= $quan . "|";
					endforeach;

					foreach ($this->input->post('price[]') As $price) :
						@$prices .= $price . "|";
					endforeach;

					# Variable Declaration
					$data = [
					  'date_created' => gmdate('Y-m-d H:i:s'),
					  'sup_id' => $this->input->post('sup_id'),
					  'pur_ord_no' => $this->input->post('inv_no'),
					  'tot_cost' => $this->input->post('tot_cost'),
					  'products' => $comb_prod_id,
					  'quantity' => $quantity,
					  'prices' => $prices,
					  'remarks' => $this->input->post('remarks'),
					  'pay' => $this->input->post('pay'),
					  'bal' => $this->input->post('bal')
					];

					$data['printdata'] = [
					  'sup_id' => $this->input->post('sup_id'),
					  'pur_ord_no' => $this->input->post('inv_no'),
					  'products' => $this->input->post('productname[]'),
					  'quantity' => $this->input->post('qty[]'),
					  'prices' => $this->input->post('price[]'),
					  'totals' => $this->input->post('total[]'),
					  'tot_cost' => $this->input->post('tot_cost')
					];

				}

				/********** Interface ***********************/
				$headertag['title'] = "Product Vendors";
				$this->load->view('invoiceprint', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('dashboard');
			}
		} else {
			redirect('access/login');
		}
	}

	/******************************
	 * Order
	 *******************************/
	public function order()
	{
		if (isset($_SESSION['username'])) {
			if (TRUE) {
				# Loading models...
				$this->load->model('Universal_Retrieval');

				# Extracting Data For Display
				$data['category_info'] = $this->Universal_Retrieval->All_Info('product_category');
				$data['Description_info'] = $this->Universal_Retrieval->All_Info('product_desc');
				$data['suppliers_info'] = $this->Universal_Retrieval->All_Info('product_suppliers');
				$data['all_prod'] = $this->Universal_Retrieval->All_Info('full_product_details');

				/********** Generating New User Id ************/
				$list = ['id' => 1];

				$last_invoice_id = $this->Universal_Retrieval->ret_data_with_s_cond_row('general_last_ids', 'id', $list);

				if (!empty($last_invoice_id)) {
					if ($last_invoice_id->pur_order_no == NULL || $last_invoice_id->pur_order_no == 0)
						$data['next_order_id'] = "PKINV#0001";

					elseif (strlen($last_invoice_id->pur_order_no) == 1)
						$data['next_order_id'] = "PKINV#000" . ($last_invoice_id->pur_order_no + 1);

					elseif (strlen($last_invoice_id->pur_order_no) == 2)
						$data['next_order_id'] = "PKINV#00" . ($last_invoice_id->pur_order_no + 1);

					elseif (strlen($last_invoice_id->pur_order_no) >= 3)
						$data['next_order_id'] = "PKINV#" . ($last_invoice_id->pur_order_no + 1);
				} else {
					$this->session->set_flashdata('error', "Error In Retrieving Last Order ID");
					$data['next_order_id'] = "ERROR";
				}
				/********** Generating New User Id ************/

				/********** Interface ***********************/
				$headertag['title'] = "Purchase Order";
				$this->load->view('headtag', $headertag);
				$this->load->view('order', $data);
				$this->load->view('footer');
				$this->load->view('order_pagesettings.php');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/******************************
	 * Customers
	 *******************************/
	public function customers()
	{
		if (isset($_SESSION['username'])) {
			if (TRUE) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_Stock');

				# Extracting Data For Display
				$data['all_customers'] = $this->Universal_Retrieval->All_Info('product_customers');

				/********** Interface ***********************/
				$headertag['title'] = "Product Register";
				$this->load->view('headtag', $headertag);
				$this->load->view('customers', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/******************************
	 * Customers
	 *******************************/
	public function cashmanage()
	{
		if (isset($_SESSION['username'])) {
			if (in_array("CASH", $_SESSION['rows_exploded'])) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_Stock');

				# Extracting Data For Display
				$data['category_info'] = $this->Universal_Retrieval->All_Info('product_category');
				$data['Description_info'] = $this->Universal_Retrieval->All_Info('product_desc');
				$allaccounts = $this->Universal_Retrieval->All_Info('v_account_details');
				$data['accounts'] = $this->Universal_Retrieval->All_Info('account_types');

				/********** Account Workings ******************/

				if (!empty($allaccounts)) {
					foreach ($allaccounts as $accinfo) {
						switch ($accinfo) :
							case $accinfo:
								if (@array_key_exists($accinfo->Name, @$data['acc_summary'])) {
									//goto desc:
								} else {
									$data['acc_summary'][$accinfo->Name]['name'] = $accinfo->Name;
									//goto desc;
								}

							case 'id':
								if (@array_key_exists($accinfo->Id, @$data['acc_summary'][$accinfo->Name])) {
									//goto refcode;
								} else {
									$data['acc_summary'][$accinfo->Name]['id'] = $accinfo->Id;
									//goto refcode;
								}

							case 'desc':
								if (@array_key_exists($accinfo->Descrip, @$data['acc_summary'][$accinfo->Name])) {
									//goto refcode;
								} else {
									$data['acc_summary'][$accinfo->Name]['desc'] = $accinfo->Descrip;
									//goto refcode;
								}

							case 'refcode':
								if (@array_key_exists($accinfo->Refcon, @$data['acc_summary'][$accinfo->Name])) {
									//goto debit;
								} else {
									$data['acc_summary'][$accinfo->Name]['refcode'] = $accinfo->Refcon;
									//goto debit;
								}

							case 'debit':
								if (@array_key_exists($accinfo->Debit_Amt, @$data['acc_summary'][$accinfo->Name])) {
									//goto credit;
								} else {
									@$data['acc_summary'][$accinfo->Name]['debit'] += $accinfo->Debit_Amt;
									//goto credit;
								}

							case 'credit':
								if (@array_key_exists($accinfo->Credit_Amt, @$data['acc_summary'][$accinfo->Name])) {
									//goto balance;
								} else {
									@$data['acc_summary'][$accinfo->Name]['credit'] += $accinfo->Credit_Amt;
									//goto balance;
								}

							case 'balance':
								if (@array_key_exists($accinfo->Balance, @$data['acc_summary'][$accinfo->Name])) {
									//goto credit;
								} else {
									@$data['acc_summary'][$accinfo->Name]['balance'] += $accinfo->Balance;
									//goto credit;
								}

								break;

						endswitch;
					}
				}
				/********** Account Workings ******************/

				/********** Generating Ref Code ************/
				$last_refcode = $this->Universal_Retrieval->All_Info_Row('general_last_ids');

				if ($last_refcode->refcode) {
					if ($last_refcode->refcode == NULL || $last_refcode->refcode == 0)
						$data['ref_code'] = "REF#001";

					elseif (strlen($last_refcode->refcode) == 1)
						$data['ref_code'] = "REF#00" . ($last_refcode->refcode + 1);

					elseif (strlen($last_refcode->refcode) == 2)
						$data['ref_code'] = "REF#0" . ($last_refcode->refcode + 1);

					elseif (strlen($last_employee_id->employee_id) == 3)
						$data['ref_code'] = "REF#" . ($last_refcode->refcode + 1);
				} else
					$data['ref_code'] = "REF#001";
				/********** Generating New User Id ************/

				/********** Interface ***********************/
				$headertag['title'] = "Product Register";
				$this->load->view('headtag', $headertag);
				$this->load->view('cashmanage', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/***************************************
	 * New Stock - Check
	 ***************************************/
	public function New_Stock()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded'])) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');

				# Loading Functions
				$data['suppliers_info'] = $this->Universal_Retrieval->All_Info('suppliers');
				$data['category_info'] = $this->Universal_Retrieval->All_Info('product_category');
				$data['Description_info'] = $this->Universal_Retrieval->All_Info('product_desc');
				$data['item_names'] = $this->Universal_Retrieval->All_Info('product_desc');

				$id_unexploded = $this->Universal_Retrieval->GenerateID('product_category', 'cat_id');
				$id_unexploded = $this->Universal_Retrieval->GenerateID('product_desc', 'desc_id');

				#################### Generating New Category ID ####################
				if (!empty($id_unexploded)) {
					foreach ($id_unexploded as $ret_id) {
						$id_exploded = explode('/', $ret_id);
					}

					(($id_exploded[2] == NULL) ? $id_exploded[2] = 1 : $id_exploded[2] += 1);

					if ($id_exploded[2] < 10)
						$data['cat_id'] = "KAD/CAT/00" . $id_exploded[2];
					elseif ($id_exploded[2] < 99)
						$data['cat_id'] = "KAD/CAT/0" . $id_exploded[2];
					elseif ($id_exploded[2] >= 100)
						$data['cat_id'] = "KAD/CAT/" . $id_exploded[2];
				} else
					$data['cat_id'] = "KAD/CAT/001";
				#################### Generating New Desecription(ProductType) ID ####################
				if (!empty($id_unexploded)) {
					foreach ($id_unexploded as $ret_id) {
						# code...
						$id_exploded = explode('/', $ret_id);
					}

					(($id_exploded[2] == NULL) ? $id_exploded[2] = 1 : $id_exploded[2] += 1);

					if ($id_exploded[2] < 10)
						$data['desc_id'] = "KAD/DESC/00" . $id_exploded[2];
					elseif ($id_exploded[2] < 99)
						$data['desc_id'] = "KAD/DESC/0" . $id_exploded[2];
					elseif ($id_exploded[2] >= 100)
						$data['desc_id'] = "KAD/DESC/" . $id_exploded[2];
				} else
					$data['desc_id'] = "KAD/DESC/001";

				/********** Interface ***********************/
				$headertag['title'] = "New Stock";
				$this->load->view('headtag', $headertag);
				$this->load->view('header');
				$this->load->view('nav');
				$this->load->view('new_stock', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				# No Permission
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Dashboard');
			}
		} else {
			# redirect to Dashboard
			redirect('Dashboard');
		}
	}

	/***********************************************
	 * Sallsorder order
	 ************************************************/
	public function SalesOrder()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('Process Requests', $_SESSION['rows_exploded'])) {
				# Loading models...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');

				/********** Interface ***********************/
				$headertag['title'] = "Purchase Order";
				$this->load->view('headtag', $headertag);
				$this->load->view('header');
				$this->load->view('nav');
				$this->load->view('stores/sallsorder');
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			redirect('Access');
		}
	}

	/****************************************
	 * Manage Stock
	 *****************************************/
	public function manage_product()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded'])) {
				# Loading model...
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_Access');

				#Extracting Data For Display
				$data['all_prod'] = $this->Universal_Retrieval->retrieve_product_info('full_product_details');
				$data['Description_info'] = $this->Universal_Retrieval->retrieve_product_info('product_desc');
				$data['productnames'] = $this->Universal_Retrieval->retrieve_product_info('product_codes');
				$data['suppliers_info'] = $this->Universal_Retrieval->retrieve_product_info('product_suppliers');

				/********** Generating New User Id ************/
				$list = ['id' => 1];

				$last_invoice_id = $this->Universal_Retrieval->ret_data_with_s_cond_row('general_last_ids', 'id', $list);

				if (!empty($last_invoice_id)) {
					if ($last_invoice_id->invoice_no == NULL || $last_invoice_id->invoice_no == 0)
						$data['next_invoice_id'] = "INVN#0001";

					elseif (strlen($last_invoice_id->invoice_no) == 1)
						$data['next_invoice_id'] = "INVN#000" . ($last_invoice_id->invoice_no + 1);

					elseif (strlen($last_invoice_id->invoice_no) == 2)
						$data['next_invoice_id'] = "INVN#00" . ($last_invoice_id->invoice_no + 1);

					elseif (strlen($last_invoice_id->invoice_no) >= 3)
						$data['next_invoice_id'] = "INVN#" . ($last_invoice_id->invoice_no + 1);
				} else {
					$this->session->set_flashdata('error', "Error In Retrieving Last Order ID");
					$data['next_order_id'] = "ERROR";
				}
				/********** Generating New User Id ************/

				/********** Interface ***********************/
				$headertag['title'] = "Manage Stock";
				$this->load->view('headtag', $headertag);
				$this->load->view('manage_product', $data);
				$this->load->view('footer');
				$this->load->view('manageproduct_pagesettings');
				/********** Interface ***********************/
			} else {
				//No Permission
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Dashboard');
			}
		} else {
			# redirect to Dashboard
			redirect('Dashboard');
		}
	}

	/*****************************************
	 * Suppliers
	 ******************************************/
	public function Vendors()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				# Loading model...
				$this->load->model('Model_Access');/*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_HR');

				# Loading Functions
				$last_sup_id = $this->Universal_Retrieval->LastID('general_last_ids', 'sup_id');
				$data['retrieve_suppliers'] = "Yes";
				#################### Generating New Supplier Id ####################
				if (!empty($last_sup_id)) {
					if ($last_sup_id->sup_id == NULL || $last_sup_id->sup_id == 0)
						$data['next_sup_id'] = "KAD/SUP/001";

					elseif (strlen($last_sup_id->sup_id) == 1)
						$data['next_sup_id'] = "KAD/SUP/00" . ($last_sup_id->sup_id + 1);

					elseif (strlen($last_sup_id->sup_id) == 2)
						$data['next_sup_id'] = "KAD/SUP/0" . ($last_sup_id->sup_id + 1);

					elseif (strlen($last_sup_id->sup_id) == 3)
						$data['next_sup_id'] = "KAD/SUP/" . ($last_sup_id->sup_id + 1);
				} else {
					$this->session->set_flashdata('error', "Error In Retrieving Last Supplier ID");
					$data['next_sup_id'] = "ERROR";
				}
				/********** Interface ***********************/
				$headertag['title'] = "Suppliers";
				$this->load->view('headtag', $headertag);
				$this->load->view('header');
				$this->load->view('nav');
				$this->load->view('stores/suppliers', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				# Permission Denied
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Dashboard');
			}
		} else {
			redirect('Access');
		}
	}

	/*****************************************
	 * Suppliers
	 ******************************************/
	public function PointOfSales()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('Vendors', $_SESSION['rows_exploded'])) {
				# Loading model...
				$this->load->model('Model_Access');/*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');
				$this->load->model('Model_HR');

				# Loading Functions
				$last_sup_id = $this->Universal_Retrieval->LastID('general_last_ids', 'sup_id');
				$data['retrieve_suppliers'] = "Yes";
				#################### Generating New Supplier Id ####################
				if (!empty($last_sup_id)) {
					if ($last_sup_id->sup_id == NULL || $last_sup_id->sup_id == 0)
						$data['next_sup_id'] = "KAD/SUP/001";

					elseif (strlen($last_sup_id->sup_id) == 1)
						$data['next_sup_id'] = "KAD/SUP/00" . ($last_sup_id->sup_id + 1);

					elseif (strlen($last_sup_id->sup_id) == 2)
						$data['next_sup_id'] = "KAD/SUP/0" . ($last_sup_id->sup_id + 1);

					elseif (strlen($last_sup_id->sup_id) == 3)
						$data['next_sup_id'] = "KAD/SUP/" . ($last_sup_id->sup_id + 1);
				} else {
					$this->session->set_flashdata('error', "Error In Retrieving Last Supplier ID");
					$data['next_sup_id'] = "ERROR";
				}
				/********** Interface ***********************/
				$headertag['title'] = "Suppliers";
				$this->load->view('headtag', $headertag);
				$this->load->view('header');
				$this->load->view('nav');
				$this->load->view('stores/PointOfSales', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				# Permission Denied
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Dashboard');
			}
		} else {
			redirect('Access');
		}
	}

	public function Suppliers_API()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('Vendors', $_SESSION['rows_exploded'])) {
				# Loading model...
				$this->load->model('Universal_Retrieval');

				print $this->Universal_Retrieval->All_Info_API('product_suppliers');
			} else {
				# Permission Denied
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Dashboard');
			}
		} else {
			# Not Logged In
			redirect('Access');
		}
	}

	/*****************************************
	 * Register New Product
	 *****************************************/
	public function Register_Products()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (in_array('Register New', $_SESSION['rows_exploded'])) {
				# Loading model...
				$this->load->model('Model_Access'); /*Needed By header, nav*/
				$this->load->model('Universal_Retrieval');

				# Loading Functions
				$data['category_info'] = $this->Universal_Retrieval->All_Info('product_category');
				$data['Description_info'] = $this->Universal_Retrieval->All_Info('product_desc');

				/********** Generating New Product ID ************/
				$last_prod_id = $this->Universal_Retrieval->LastID('general_last_ids', 'prod_id');

				if (!empty($last_prod_id)) {
					if ($last_prod_id->prod_id == NULL || $last_prod_id->prod_id == 0)
						$data['next_prod_id'] = "PROD#001";

					elseif (strlen($last_prod_id->prod_id) == 1)
						$data['next_prod_id'] = "PROD#00" . ($last_prod_id->prod_id + 1);

					elseif (strlen($last_prod_id->prod_id) == 2)
						$data['next_prod_id'] = "PROD#0" . ($last_prod_id->prod_id + 1);

					elseif (strlen($last_prod_id->prod_id) == 3)
						$data['next_prod_id'] = "PROD#" . ($last_prod_id->prod_id + 1);
				} else {
					$this->session->set_flashdata('error', "Error In Retrieving Last Product ID");
					$data['next_prod_id'] = "ERROR";
				}
				/********** Generating New Product ID ************/

				/********** Interface ***********************/
				$headertag['title'] = "Register New";
				$this->load->view('headtag', $headertag);
				$this->load->view('header');
				$this->load->view('nav');
				$this->load->view('stores/register_product', $data);
				$this->load->view('footer');
				/********** Interface ***********************/
			} else {
				# Permission Denied
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('Dashboard');
			}
		} else {
			# Not Logged In
			redirect('Access');
		}
	}

	/*************************************** Interfaces  ***********************************/

	/*************************************** Data Insertion  ******************************/
	/****************************************
	 * New Supplier
	 *****************************************/
	public function New_Vendor()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (isset($_POST['sup_save']) && in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				# Setting validation rules
				$this->form_validation->set_rules('name', 'Supplier Name', 'required|trim|is_unique[product_suppliers.name]', array('is_unique' => 'Supplier Name Already Exists'));
				$this->form_validation->set_rules('tel1', 'Telephone No.', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('tel2', 'Alt Telephone No.', 'trim|min_length[10]');
				$this->form_validation->set_rules('addr', 'Address', 'trim');
				$this->form_validation->set_rules('email', 'Email', 'trim');
				$this->form_validation->set_rules('loc', 'Location', 'required|trim');
				$this->form_validation->set_rules('pay_type', 'Payment Terms', 'required|trim');
				$this->form_validation->set_rules('bank', 'Payment Terms', 'trim');
				$this->form_validation->set_rules('branch', 'Payment Terms', 'trim');
				$this->form_validation->set_rules('acc_name', 'Account Name', 'trim');
				$this->form_validation->set_rules('acc_no', 'Account Number', 'trim');
				$this->form_validation->set_rules('prod_type[]', 'Product Type', 'required|trim');

				# If Failed
				if ($this->form_validation->run() === FALSE) {
					#Redirecting With Error
					$this->session->set_flashdata('error', validation_errors());
					redirect('BackOffice/Vendor');
				} # If Passed
				else {
					# Loading model...
					$this->load->model('Universal_Insertion');

					$prod_type = $this->input->post('prod_type[]');

					if (!empty($prod_type)) {
						$sup_info = [
						  'name' => $this->input->post('name'),
						  'tel1' => $this->input->post('tel1'),
						  'tel2' => $this->input->post('tel2'),
						  'addr' => $this->input->post('addr'),
						  'email' => $this->input->post('email'),
						  'pay_type' => $this->input->post('pay_type'),
						  'loc' => $this->input->post('loc')
						];

						foreach ($prod_type As $prodtype) {
							@$sup_info['prod_type'] .= $prodtype . "|";
						}

					}

					# Transaction Begin
					$this->db->trans_start();
					$this->Universal_Insertion->DataInsert('product_suppliers', $sup_info);

					$last_sup_id = $this->db->insert_id();

					if ($this->input->post('pay_type') == "Cheque") :

						$account_Info = [
						  'sup_id' => $last_emp_id,
						  'bank' => $this->input->post('bank'),
						  'branch' => $this->input->post('branch'),
						  'name' => $this->input->post('acc_name'),
						  'num' => $this->input->post('acc_no')
						];

						$this->Universal_Insertion->DataInsert('product_suppliers_accounts', $account_Info);
					endif;
					$this->db->trans_complete();
					# Transaction End

					if ($this->db->trans_status() === FALSE)
						$this->session->set_flashdata('error', 'User Registration Failed');

					else
						$this->session->set_flashdata('success', 'User Successfully Added');

					redirect('BackOffice/Vendors');
				} # End of Validation Rule Check
			} else {
				$this->session->set_flashdata('error', "Permission Denied ! Contact Administrator");
				redirect('BackOffice/Vendors');
			}
		} else
			redirect('Access/Login');

	} # End of function


	/****************************************
	 * New Supplier
	 *****************************************/
	public function New_Customer()
	{
		# Permission Check
		if (isset($_SESSION['username'])) {
			if (isset($_POST['customer_save']) && in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				# Setting validation rules
				$this->form_validation->set_rules('name', 'Customer Name', 'required|trim');
				$this->form_validation->set_rules('tel1', 'Telephone No.', 'required|trim');
				$this->form_validation->set_rules('tel2', 'Alt Telephone No.', 'trim');
				$this->form_validation->set_rules('address', 'Address', 'trim');
				$this->form_validation->set_rules('email', 'Email', 'trim');
				$this->form_validation->set_rules('location', 'Location', 'required|trim');

				# If Failed
				if ($this->form_validation->run() === FALSE) {
					#Redirecting With Error
					$this->session->set_flashdata('error', validation_errors());
					redirect('BackOffice/customers');
				} # If Passed
				else {
					# Loading model...
					$this->load->model('Universal_Insertion');

					$sup_info = [
					  'fullname' => ucwords($this->input->post('name')),
					  'mobile_number_1' => ucwords($this->input->post('tel1')),
					  'mobile_number_2' => ucwords($this->input->post('tel2')),
					  'address' => ucwords($this->input->post('address')),
					  'email' => ucwords($this->input->post('email')),
					  'location' => ucwords($this->input->post('location'))
					];

					$result = $this->Universal_Insertion->DataInsert('product_customers', $sup_info);

					if ($result)
						$this->session->set_flashdata('success', 'Registration Successful');

					else
						$this->session->set_flashdata('error', 'Registration Failed');

					redirect('BackOffice/customers');
				} # End of Validation Rule Check
			} else {
				$this->session->set_flashdata('error', "Permission Denied ! Contact Administrator");
				redirect('BackOffice/customers');
			}
		} else
			redirect('Access/Login');

	} # End of function


	/****************************************
	 * New Category
	 *****************************************/
	public function Register_Category()
	{
		if (isset($_SESSION['username'])) {
			if (TRUE)//in_array('Register New-Can Add Prod / Cat / Desc',$_SESSION['priv_exploded']) )
			{
				if (isset($_POST['cat_submit'])) {
					$this->form_validation->set_rules('category', 'Category', 'required|trim|is_unique[product_category.cat_name]');

					if ($this->form_validation->run() === FALSE) {
						$this->session->set_flashdata('error', "Category Name Already Exists");
						redirect('backoffice/registerproduct');
					} else {
						$this->load->model('Universal_Insertion');

						$data['cat_name'] = ucwords($this->input->post('category'));

						$result = $this->Universal_Insertion->DataInsert('product_category', $data);

						if ($result)
							$this->session->set_flashdata('success', "Registration Successful");

						else
							$this->session->set_flashdata('error', "Registration Failed");

						redirect('backoffice/registerproduct');
					}
				} else {
					$this->session->set_flashdata('error', "Form Submit Failed");
					redirect('backoffice/registerproduct');
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('backoffice/registerproduct');
			}
		} else {
			redirect('Dashboard');
		}
	}

	/****************************************
	 * New Description
	 *****************************************/
	public function register_description()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded']))//in_array('Register New-Can Add Prod / Cat / Desc',$_SESSION['priv_exploded']) )
			{
				if (isset($_POST['desc_submit'])) {
					$this->form_validation->set_rules('description', 'Description', 'required|trim');

					if ($this->form_validation->run() === FALSE) {
						$this->session->set_flashdata('error', validation_errors());
						redirect('backoffice/registerproduct');
					} else {
						$this->load->model('Universal_Insertion');

						$data['desc'] = ucwords($this->input->post('description'));

						$result = $this->Universal_Insertion->DataInsert('product_desc', $data);

						if ($result)
							$this->session->set_flashdata('success', "Registration Successful");

						else
							$this->session->set_flashdata('error', "Registration Failed");

						redirect('backoffice/registerproduct');
					}
				} else {
					$this->session->set_flashdata('error', "Form Submit Failed");
					redirect('backoffice/registerproduct');
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('backoffice/registerproduct');
			}
		} else {
			redirect('Dashboard');
		}
	} # End of function

	/****************************************
	 * New Stock
	 *****************************************/
	public function save_stock()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded']) && isset($_POST['update_stock'])) {
				if (TRUE) {
					# Setting validation rules
					$this->form_validation->set_rules('qty[]', 'Quanity', 'trim');
					$this->form_validation->set_rules('unit[]', 'Unit Price', 'trim');

					$this->form_validation->set_rules('date_recv', 'Date Recieved', 'trim');
					$this->form_validation->set_rules('vendor', 'Vendor', 'trim');
					$this->form_validation->set_rules('sup_id', 'Supplier', 'trim');
					$this->form_validation->set_rules('inv_no', 'Invoice No.', 'trim');
					$this->form_validation->set_rules('tot_cost', 'Total cost', 'trim');
					$this->form_validation->set_rules('prod_id[]', 'Item Name', 'required|trim');
					$this->form_validation->set_rules('desc_id[]', 'Item Type', 'trim');
					$this->form_validation->set_rules('costP[]', 'Item Type', 'trim');
					#Checking Form Validation Rule Result
					# If Failed
					if ($this->form_validation->run() === FALSE) {
						#Redirecting With Error
						$this->session->set_flashdata('error', validation_errors());
						redirect('backoffice/manage_product');
					} # If Passed
					else {
						# loading Model
						$this->load->model('Universal_Insertion');
						$this->load->model('Universal_Update');
						$this->load->model('Universal_Retrieval');

						$prodids = $this->input->post('prod_id[]');
						/*$descids = $this->input->post('desc_id[]');
						$costP = $this->input->post('costP[]');*/
						$qtys = $this->input->post('qty[]');
						$units = $this->input->post('unit[]');

						# Combining All Category ID
						for ($a = 0; $a < sizeof($prodids); $a++) {
							@$prod_ids .= base64_decode($prodids[$a]) . "/";
							/*@$desc_ids .= base64_decode($descids[$a])."/";
							@$costPs .= $costP[$a]."/";*/
							@$qtyids .= $qtys[$a] . "/";
							@$unitss .= $units[$a] . "/";

							/******** Performing Real Update ********/
							# Calling the current qty
							$datas = ['prod_id' => base64_decode($prodids[$a])];
							$cur_qty = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_details', 'prod_id', $datas);

							$where_condition = ['prod_id' => base64_decode($prodids[$a])];

							$dat = ['avail_qty' => $qtys[$a] + $cur_qty->avail_qty, 'unit_price' => $units[$a]];

							$result = $this->Universal_Update->Multiple_Update('product_details', $dat, $where_condition);
						}
						//End of loop;

						if ($result) {
							$data = [
							  'comb_prod_id' => $prod_ids,
							  'comb_qty' => $qtyids,
							  'comb_unit' => $unitss,
							  'employee_id' => $_SESSION['employee_id']

								/*'invoice_no' =>  $this->input->post('inv_no'),
								'total_cost' => $this->input->post('tot_cost'),
								'sup_id' => $this->input->post('sup_id'),
								'vendor' => $this->input->post('vendor'),
								'date_recv' => $this->input->post('date_recv'),
								'comb_desc_id' => $desc_ids ,
								'comb_cost_p' => $costPs,*/
							];

							$result = $this->Universal_Insertion->DataInsert('product_audit', $data);

							if ($result)
								$this->session->set_flashdata('success', "Products Updated");

							else
								$this->session->set_flashdata('success', "Products Update Failed");
						} else
							$this->session->set_flashdata('error', "Update Failed");

						redirect('backoffice/manage_product');

					} # End of Validation Check
				} else {
					# Submit Button Not Clicked
					redirect('backoffice/manage_product');
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('BackOffice/Manage_Product');
				//print_r($_POST);
			} # End of priv
		} else {
			redirect('Access');
		} # End of row
	}

	/****************************************
	 * Register Single Product
	 *****************************************/
	public function Register_Single_Product()
	{
		if (isset($_SESSION['username'])) // in_array('Register New',$_SESSION['rows_exploded']) )
		{
			if (in_array('PROD', $_SESSION['rows_exploded'])) //in_array('Register New-Can Add Prod / Cat / Desc',$_SESSION['priv_exploded']) )
			{
				if (isset($_POST['single_add'])) {
					# Setting validation rules
					$this->form_validation->set_rules('desc_id', 'Description', 'required|trim');
					$this->form_validation->set_rules('prod_name', 'Product Name', 'required|trim');
					$this->form_validation->set_rules('productID', 'Product ID', 'required|trim|is_unique[product_details.product_id]');
					$this->form_validation->set_rules('unit_qty', 'Unit Quanity', 'required|trim');
					$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|trim');
					/*$this->form_validation->set_rules('cost_price','Cost Price','required|trim');*/
					$this->form_validation->set_rules('cur_qty', 'Current Quantity', 'required|trim');
					$this->form_validation->set_rules('promo_qty', ' Product Limit', 'trim');
					$this->form_validation->set_rules('promo_price', ' Product Limit', 'trim');

					#Checking Form Validation Rule Result
					if ($this->form_validation->run() === FALSE) {
						$this->session->set_flashdata('error', validation_errors());
						redirect('backoffice/registerproduct');
					} else {
						#Variables Assignment
						$product_codes = ['prod_name' => $this->input->post('prod_name')];

						# Loading Model
						$this->load->model('Universal_Insertion');
						$this->load->model('Universal_Retrieval');
						$this->load->model('Universal_Update');
						$this->load->helper('file_restriction');

						$itmarray = ['prod_name' => $this->input->post('prod_name')];
						$itmname = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes', 'prod_name', $itmarray);

						if (empty($itmname)) :

							# Working on File Upload $file_array,$mall_name,$img_type
							if (!empty($_FILES['icon']['tmp_name']) && !empty($this->input->post('prod_name'))) {
								$target_dir = "uploads/product/";
								$error_url = base_url() . "backoffice/registerproduct";

								if ($logo_path = pic_restriction($_FILES['icon'], $this->input->post('prod_name'), $target_dir, $error_url)) {
									if (!empty($logo_path))
										move_uploaded_file($_FILES['icon']['tmp_name'], $logo_path);
								} else
									redirect('backoffice/registerproduct');
							} else
								$logo_path = "uploads/product/default_logo.png";
							# End of Logo Upload

							# Transaction Begin
							$this->db->trans_start();
							$this->Universal_Insertion->DataInsert('product_codes', $product_codes);

							$last_prod_id = $this->db->insert_id();

							$product_details = [
							  'prod_id' => @$last_prod_id,
							  'product_id' => strtoupper($this->input->post('productID')),
							  'desc_id' => $this->input->post('desc_id'),
							  'unit_qty' => $this->input->post('unit_qty'),
							  'cost_price' => $this->input->post('cost_price'),
							  'unit_price' => $this->input->post('unit_price'),
							  'avail_qty' => $this->input->post('cur_qty'),
							  'promo_qty' => $this->input->post('promo_qty'),
							  'promo_price' => $this->input->post('promo_price'),
							  'img_path' => $logo_path
							];

							$this->Universal_Insertion->DataInsert('product_details', $product_details);

							$last_code = explode('#', $this->input->post('productID'));

							$update = ['product_code' => (int)$last_code[1], 'id' => 1]; //print_r($update);

							$code = $this->Universal_Update->OneFieldUpdate('general_last_ids', 'id', 'product_code', $update);

							$this->db->trans_complete();
						# Transaction End

						else:
							# Transaction Begin
							$this->db->trans_start();
							$product_details = [
							  'prod_id' => @$itmname->prod_id,
							  'product_id' => strtoupper($this->input->post('productID')),
							  'desc_id' => $this->input->post('desc_id'),
							  'unit_qty' => $this->input->post('unit_qty'),
							  'cost_price' => $this->input->post('cost_price'),
							  'unit_price' => $this->input->post('unit_price'),
							  'avail_qty' => $this->input->post('cur_qty'),
							  'promo_qty' => $this->input->post('promo_qty'),
							  'promo_price' => $this->input->post('promo_price'),
							  'img_path' => $logo_path
							];

							$this->Universal_Insertion->DataInsert('product_details', $product_details);

							$last_code = explode('#', $this->input->post('productID'));

							$update = ['product_code' => (int)$last_code[1], 'id' => 1]; //print_r($update);

							$code = $this->Universal_Update->OneFieldUpdate('general_last_ids', 'id', 'product_code', $update);

							$this->db->trans_complete();
							# Transaction End
						endif;


						if ($this->db->trans_status() === false)
							$this->session->set_flashdata('error', 'New Product Registration Failed');

						else
							$this->session->set_flashdata('success', 'New Product Added');

						redirect('backoffice/registerproduct');
					}
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator.");
				redirect('backoffice/registerproduct');
			} # End of priv
		} else {
			redirect('Access/Login');
		}
	}

	/***********************************************
	 * New Description
	 ************************************************/
	public function Register_Product_Bulk()
	{
		if (in_array('Register New', $_SESSION['rows_exploded'])) {
			if (in_array('Register New-Can Add Prod / Cat / Desc', $_SESSION['priv_exploded'])) {
				if (isset($_POST['bulk_add'])) {
					# Setting validation rules
					$this->form_validation->set_rules('prod_id', 'Product ID', 'required|trim');

					# Checking Form Validation Rule Result
					# If Failed
					if ($this->form_validation->run() === FALSE) {
						# Redirecting With Error
						$this->Register_Product();
					} # If Passed
					else {
						# Variables Assignment
						$data['prod_id'] = $this->input->post('prod_id');

						# Parsing File Data For Processing
						$this->file_restriction($_FILES['prod_file'], "Register Bulk Stock", $data['prod_id']);
					}
				} else {
					# Subnit Button Not Clicked
					redirect('Stores/Register_Product');
				}
			} else {
				# Permission Denied
				$_SESSION['error'] = "Permission Denied. Contact Administrator";
				redirect('Stores/Register_Product');
			}
		} else {
			# Permission Denied
			$_SESSION['error'] = "Permission Denied. Contact Administrator";
			redirect('Stores/Register_Product');
		}
	}

	/****************************************
	 * Create Accounts
	 *****************************************/
	public function Create_Account()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('CASH', $_SESSION['rows_exploded']) && isset($_POST['cash_create'])) {
				$this->form_validation->set_rules('acc_name', 'Account Name', 'required|trim');
				$this->form_validation->set_rules('ref_code', 'Refenece Code', 'required|trim');
				$this->form_validation->set_rules('tax', 'Tax', 'trim');
				$this->form_validation->set_rules('description', 'Description', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('BackOffice/CashManage');
				} else {
					$this->load->model('Universal_Insertion');
					$this->load->model('Universal_Update');

					$data = [
					  'acc_name' => $this->input->post('acc_name'),
					  'acc_ref_no' => $this->input->post('ref_code'),
					  'acc_tax' => $this->input->post('tax'),
					  'acc_desc' => $this->input->post('description'),
					  'created_by' => $_SESSION['employee_id']
					];

					$last_code = explode('#', $data['acc_ref_no']);

					$result = $this->Universal_Insertion->DataInsert('account_types', $data);

					if ($result) {
						$update = ['refcode' => (int)$last_code[1], 'id' => 1];

						$code = $this->Universal_Update->OneFieldUpdate('general_last_ids', 'id', 'refcode', $update);

						if ($code)
							$this->session->set_flashdata('success', "Registration Successful");

						else
							$this->session->set_flashdata('error', "Code Update Failed");
					} else
						$this->session->set_flashdata('error', "Registration Failed");

					redirect('BackOffice/CashManage');
				}

			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('BackOffice/CashManage');
			}
		} else {
			redirect('Access/Login');
		}
	}

	/*************************************** Data Insertion  ******************************/

	/*************************************** Data Update     ******************************/
	/******************************
	 * Supplier Update
	 ******************************/
	public function update_product()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded']) && isset($_POST['update_prod'])) {
				# Setting validation rules
				$this->form_validation->set_rules('edit_prod_id', 'Product ID', 'required|trim');
				$this->form_validation->set_rules('edit_desc_id', 'Description ID', 'required|trim');
				$this->form_validation->set_rules('edit_prod_name', 'Product Name', 'required|trim');
				$this->form_validation->set_rules('edit_prod_code', 'Product Code', 'required|trim');
				$this->form_validation->set_rules('edit_unit_qty', 'Unit Quanity', 'trim');
				$this->form_validation->set_rules('edit_unit_price', 'Unit Price', 'trim');
				$this->form_validation->set_rules('edit_cost_price', 'Unit Price', 'trim');
				$this->form_validation->set_rules('edit_unit_promoqty', 'Promo Quanity', 'trim');
				$this->form_validation->set_rules('edit_unit_promoprice', 'Promo Price', 'trim');

				#Checking Form Validation Rule Result
				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('BackOffice/Manage_Product');
				} else {
					#Variables Assignment
					$product_codes = ['prod_name' => $this->input->post('edit_prod_name')];

					# Loading Model
					$this->load->model('Universal_Update');
					$this->load->model('Universal_Retrieval');
					$this->load->helper('file_restriction');

					# Working on File Upload $file_array,$mall_name,$img_type
					if (!empty($_FILES['icon']['tmp_name']) && !empty($this->input->post('edit_prod_name'))) {
						$target_dir = "uploads/product/";
						$error_url = base_url() . "backoffice/manage_product";

						if ($logo_path = pic_restriction($_FILES['icon'], $this->input->post('prod_name'), $target_dir, $error_url)) {
							if (!empty($logo_path))
								move_uploaded_file($_FILES['icon']['tmp_name'], $logo_path);
						} else
							redirect('backoffice/manage_product');
					} else
						$logo_path = "";
					# End of Logo Upload

					$product_details = [
					  'desc_id' => base64_decode($this->input->post('edit_desc_id')),
					  'unit_qty' => $this->input->post('edit_unit_qty'),
					  'cost_price' => $this->input->post('edit_cost_price'),
					  'unit_price' => $this->input->post('edit_unit_price'),
					  'promo_qty' => $this->input->post('edit_unit_promoqty'),
					  'promo_price' => $this->input->post('edit_unit_promoprice'),
					];

					if (!empty($logo_path))
						$product_details = ['logo_path' => $logo_path];

					$where_condition = ['prod_id' => base64_decode($this->input->post('edit_prod_id'))];

					/**** Code Check ****/
					$code_check = ['product_id' => strtoupper($this->input->post('edit_prod_code'))];
					$prodcode_check = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_details', 'product_id', $code_check);

					if (strtoupper($this->input->post('edit_prod_code')) != $prodcode_check->product_id)
						$product_details = ['product_id' => strtoupper($this->input->post('edit_prod_code'))];

					/********* Product Name ******************/
					$code_check = ['prod_name' => strtoupper($this->input->post('edit_prod_name'))];
					$prodname_check = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes', 'prod_name', $code_check);
					//exit;

					if (strtoupper($this->input->post('edit_prod_name')) != $prodname_check->prod_name) :
						$product_codes = ['prod_name' => $this->input->post('edit_prod_name')];
						$nameresult = $this->Universal_Update->Multiple_Update('product_codes', $product_codes, $where_condition);
					endif;
					/**** Code Check ****/

					$result = $this->Universal_Update->Multiple_Update('product_details', $product_details, $where_condition);

					//print_r($nameresult); exit;

					if ($result || $nameresult)
						$this->session->set_flashdata('success', 'Product Edit Successful');

					else
						$this->session->set_flashdata('error', 'Product Edit Failed');

					redirect('backoffice/manage_product');
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator.");
				redirect('backoffice/manage_product');
			} # End of priv
		} else {
			redirect('access/login');
		}
	}

	/******************************
	 * Supplier Update
	 ******************************/
	public function Update_Vendor()
	{
		if (isset($_SESSION['username'])) {
			if (isset($_POST['update_sup']) && in_array('SUPCUST', $_SESSION['rows_exploded'])) {
				# Setting validation rules
				$this->form_validation->set_rules('edit_sup_id', 'Supplier Name', 'required|trim');
				$this->form_validation->set_rules('edit_sup_name', 'Supplier Name', 'required|trim');
				$this->form_validation->set_rules('edit_sup_tel1', 'Telephone No.', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('edit_sup_tel2', 'Alt Telephone No.', 'trim|min_length[10]');
				$this->form_validation->set_rules('edit_sup_email', 'Address', 'trim');
				$this->form_validation->set_rules('edit_sup_loc', 'Email', 'trim');
				$this->form_validation->set_rules('edit_sup_addr', 'Location', 'trim');
				$this->form_validation->set_rules('edit_sup_pay_type', 'Payment Terms', 'trim');
				$this->form_validation->set_rules('bank', 'Payment Terms', 'trim');
				$this->form_validation->set_rules('branch', 'Payment Terms', 'trim');
				$this->form_validation->set_rules('acc_name', 'Account Name', 'trim');
				$this->form_validation->set_rules('acc_no', 'Account Number', 'trim');
				$this->form_validation->set_rules('prod_type[]', 'Product Type', 'trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', "Validation Error Occured.");
					redirect('BackOffice/vendor');
				} else {
					# Loading model...
					$this->load->model('Universal_Update');

					$prod_type = $this->input->post('prod_type[]');

					$sup_info = [
					  'name' => $this->input->post('edit_sup_name'),
					  'tel1' => $this->input->post('edit_sup_tel1'),
					  'tel2' => $this->input->post('edit_sup_tel1'),
					  'addr' => $this->input->post('edit_sup_addr'),
					  'email' => $this->input->post('edit_sup_email'),
					  'pay_type' => $this->input->post('edit_sup_pay_type'),
					  'loc' => $this->input->post('edit_sup_loc')
					];

					if (!empty($prod_type)) {
						foreach ($prod_type As $prodtype) {
							@$sup_info['prod_type'] .= $prodtype . "|";
						}

					}

					if ($this->input->post('edit_sup_pay_type') == "Cheque") {
						$account_Info = [
						  'bank' => $this->input->post('bank'),
						  'branch' => $this->input->post('branch'),
						  'name' => $this->input->post('acc_name'),
						  'num' => $this->input->post('acc_no')
						];
					}

					$where_condition = ['sup_id' => base64_decode($this->input->post('edit_sup_id'))];

					$result = $this->Universal_Update->Multiple_Update('product_suppliers', $sup_info, $where_condition);

					$result_acct = $this->Universal_Update->Multiple_Update('product_suppliers_accounts', $account_Info, $where_condition);

					if ($result || $result_acct)
						$this->session->set_flashdata('success', 'Suppliers Update Successfully');

					else
						$this->session->set_flashdata('error', 'Suppliers Update Failed');
					//print_r($_SESSION['success']);
					redirect('BackOffice/Vendor');
				}
			} else {
				$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
				redirect('BackOffice/Vendors');
			}
		} else {
			redirect('Access');
		}
	}

	/****************************
	 * Category Update
	 ****************************/
	public function Update_Category()
	{
		if (isset($_POST['editbtn'])) {
			$this->form_validation->set_rules('ResetId', 'Category ID', 'required|trim');
			$this->form_validation->set_rules('new_name', 'Supplier Name', 'required|trim|is_unique[product_category.cat_name]', array('is_unique' => 'Category Already Exists'));

			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('error', "Category Name Alreday Exist");
				redirect('backoffice/registerproduct');
			} else {
				# Loading model...
				$this->load->model('Universal_Update');

				$setData = [
				  'dbField' => 'cat_name',
				  'updateVal' => $this->input->post('new_name')
				];

				$where_condition = ['cat_id' => base64_decode($this->input->post('ResetId'))];

				$result = $this->Universal_Update->Single_Field_Update('product_category', $setData, $where_condition);

				if ($result)
					$this->session->set_flashdata('success', "Category Updated");

				else
					$this->session->set_flashdata('error', "Updated Failed");

				redirect('backoffice/registerproduct');
			}
		} else {

			redirect('Stores/Register_Product');
		}
	}

	/****************************
	 * Description Update
	 ****************************/
	public function update_description()
	{
		if (isset($_SESSION['username'])) {
			/************** Description Info Update *******************/
			if (isset($_POST['editbtn']) && in_array('PROD', $_SESSION['rows_exploded'])) {
				$this->form_validation->set_rules('ResetId', 'Category ID', 'required|trim');
				$this->form_validation->set_rules('new_name', 'Category ID', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('backoffice/registerproduct');
				} else {
					$this->load->model('Universal_Update');

					$data = ['desc' => ucwords($this->input->post('new_name'))];

					$where_condition = ['desc_id' => base64_decode($this->input->post('ResetId'))];

					$result = $this->Universal_Update->Multiple_Update('product_desc', $data, $where_condition);

					if ($result)
						$this->session->set_flashdata('success', "Category Updated");

					else
						$this->session->set_flashdata('error', "Category Failed");

					redirect('backoffice/registerproduct');
				}
			}
			/************** Description Info Update *******************/

			/************** Description Info Delete *******************/
			if (isset($_POST['deletebutton']) && in_array('PROD', $_SESSION['rows_exploded'])) {
				$this->form_validation->set_rules('deleteid', 'Category ID', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', validation_errors());
					redirect('backoffice/registerproduct');
				} else {
					$this->load->model('Universal_Update');

					$data = ['status' => 'deleted'];

					$where_condition = ['desc_id' => base64_decode($this->input->post('deleteid'))];

					$result = $this->Universal_Update->Multiple_Update('product_desc', $data, $where_condition);

					if ($result)
						$this->session->set_flashdata('success', "Description Deleted");

					else
						$this->session->set_flashdata('error', "Delete Failed");

					redirect('backoffice/registerproduct');
				}
			}
			/************** Description Info Delete *******************/

		} else {

			redirect('Stores/Register_Product');
		}
	}

	/******************************************************   Data Delete   ****************************************/
	/********************************
	 * Delete Category
	 *********************************/
	public function Delete_Category()
	{
		if (isset($_SESSION['username'])) {
			if (isset($_POST['deletebutton'])) //in_array('Suppliers-Can Delete',$_SESSION['priv_exploded']) && isset($_POST['deletebutton']))
			{
				$this->form_validation->set_rules('deleteid', 'Delete', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', 'Delete Id Not Found');
					redirect('backoffice/registerproduct');
				} else {
					$this->load->model('Universal_Deletion');

					$data = [
					  'IdField' => "cat_id",
					  'deleteid' => base64_decode($this->input->post('deleteid'))
					];

					$result = $this->Universal_Deletion->Delete_Row('product_category', $data);

					if ($result)
						$this->session->set_flashdata('success', "Category Deleted");

					else
						$this->session->set_flashdata('error', 'Category Failed');

					redirect('backoffice/registerproduct');
				}
			} else {
				$this->session->set_flashdata('error', 'Permission Denied. Contact Administrator');
				redirect('backoffice/registerproduct');
			}
		} else
			redirect('Access');
	}

	/********************************
	 * Delete Vendors
	 *********************************/
	public function Delete_Vendor()
	{
		if (isset($_SESSION['username'])) {
			if (isset($_POST['deletebutton'])) //in_array('Suppliers-Can Delete',$_SESSION['priv_exploded']) && isset($_POST['deletebutton']))
			{
				$this->form_validation->set_rules('deleteid', 'Delete', 'required|trim');

				if ($this->form_validation->run() === FALSE) {
					$this->session->set_flashdata('error', 'Delete Id Not Found');
					redirect('BackOffice/Vendor');
				} else {
					$this->load->model('Universal_Deletion');

					$data = [
					  'IdField' => "sup_id",
					  'deleteid' => base64_decode($this->input->post('deleteid'))
					];

					$result = $this->Universal_Deletion->Delete_Row('product_suppliers', $data);

					if ($result)
						$this->session->set_flashdata('success', "Vendor Deleted");

					else
						$this->session->set_flashdata('error', 'Vendor Failed');

					redirect('BackOffice/Vendor');
				}
			} else {
				$this->session->set_flashdata('error', 'Permission Denied. Contact Administrator');
				redirect('BackOffice/Vendor');
			}
		} else
			redirect('Access');
	}

	/*********************** Other Relevant Function *********************/
	/***********************************************
	 * AJAX Request to Fetch Item Name
	 ************************************************/
	public function Ajax_Load_ITM_Name()
	{
		# Loading Model
		$this->load->model('Retrieval');

		# Assigning Data For Query
		$data['CategoryID'] = "KAD/CAT/" . $this->input->post('cat_id');

		$tablename = "prod_details";

		$IdField = "CategoryID";

		$result = $this->Retrieval->ret_data_with_s_cond($tablename, $IdField, $data);

		print "<option></option>";

		foreach ($result As $res) {
			$prodid = explode('/', $res->Product_ID);
			print "<option value='" . @$prodid[3] . "'>" . @$res->Item_Name . "</option>";
		}

		exit;
	}

	/***********************************************
	 * AJAX Request to Fetch Description
	 ************************************************/
	public function desc_ret_ajax()
	{
		$this->form_validation->set_rules('catid', 'Category ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "Description Retrieve Failed";
		} else {
			# Loading Model
			$this->load->model('Model_Stock');

			$result = $this->Model_Stock->cat_desc_retrieve($this->input->post('catid'));

			if (!empty($result)) {
				foreach ($result as $desc) {
					print "<option value='$desc->desc_id'>$desc->desc</option>";
				}
			}
		}
	}

	/***********************************
	 * User Roles Retrieve
	 ************************************/
	public function retrieve_prod_type()
	{
		$this->form_validation->set_rules('sup_id', 'Suppliers ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print "Retrieval Failed";
		} else {
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$data['sup_id'] = base64_decode($this->input->post('sup_id'));

			$result = $this->Universal_Retrieval->ret_data_with_s_cond_row('suppliers_full_details', 'sup_id', $data);

			if ($result) {
				$exploded = explode('|', $result->prod_type);

				foreach ($exploded as $type) {
					if (!empty($type)) {
						$cdata['cat_id'] = $type;

						$result = $this->Universal_Retrieval->ret_data_with_s_cond('product_category', 'cat_id', $cdata);

						foreach ($result as $catname) {
							if (!empty($catname)) {
								print "<div class='checkbox'><input type='checkbox' checked readonly class='checkbox1'><label for='checkbox1'> $catname->cat_name </label></div>";
							}
						}
					}
				}
			} else {
				print "";
			}
		}
	}

	/***********************************
	 * Create Purchase Order
	 ************************************/
	public function create_purchase_order()
	{
		if (isset($_SESSION['username'])) {
			if (in_array('PROD', $_SESSION['rows_exploded'])) {
				$this->form_validation->set_rules('pur_ord[]', 'Product Type', 'required|trim');

				# If Failed
				if ($this->form_validation->run() === FALSE) {
					#Redirecting With Error
					$this->session->set_flashdata('error', validation_errors());
					redirect('backoffice/manage_product');
				} else {
					if (!empty($this->input->post('pur_ord[]')))
						$_SESSION['products'] = $this->input->post('pur_ord[]');

					redirect('backoffice/order');
				}
			} else {
				$this->session->set_flashdata('error', 'Permission Denied. Contact Administrator');
				redirect('dashboard');
			}
		} else {
			redirect('access/login');
		}
	}

	/***********************************************
	 * AJAX Request to Return Row Data
	 ************************************************/
	public function return_cashreport()
	{
		$this->form_validation->set_rules('cashid', 'Transaction ID', 'required|trim');

		if ($this->form_validation->run() === FALSE) {
			print validation_errors();
		} else {
			# Loading Model
			$this->load->model('Universal_Retrieval');

			$whereCondition = ['Id' => $this->input->post('cashid')];

			$result = $this->Universal_Retrieval->ret_data_with_s_cond_where('v_account_details', $whereCondition);

			if ($result) {
				$counter = 1;

				foreach ($result As $res) {
					/***** Retrieving Product Name ********/
					$data = ['id' => $res->Id];

					print "<tr><td>$counter</td><td>$res->Name</td><td>$res->Debit_Amt</td><td>$res->Credit_Amt</td><td>$res->Balance</td></tr>";

					$counter++;
				}
			}
		}
	}

}//End of Class
