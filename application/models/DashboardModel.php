<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model
{
	public function getDailyPaymentModeTotal($paymentmode)
	{
		$tablename = "product_transact";
		$this->db->select("SUM(tot_cost) as total_amount");
		$this->db->where([
		  'paymentmode' => $paymentmode,
		  'employee_id' => $_SESSION['employee_id'],
		  'DATE(date_withdrawn)' => gmdate('Y-m-d')
		]);
		$queryResult = $this->db->get($tablename);

		return $queryResult->row();
	}
}