  <head>
  <title>Invoice</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

  <!-- Theme -->
 <!--  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css"> -->

</head>



<body style="margin: 0px !important" >
    <div class="wrapper" style="margin: 0px !important">
      <!-- Main content -->
      <section class="invoice"style="margin: 0px !important" >
        
        <!-- info row -->

        <!-- Table row -->
        <div class="row" style="margin: 0px !important">
          <div class="col-xs-12" style="margin: 0px !important">
          <div class=" invoice-col">
           <!-- <h2 class="page-header"> -->
            <!-- <img src="<?= base_url().@$_SESSION['company_logo'] ?>" style="margin-left:0em;width:150px;height:60px;" class=""> -->
             <address style="height:20px;">
              <strong><?= @$_SESSION['company_name'] ?></strong><br>
              <?= @$_SESSION['company_address'] .', ', @$_SESSION['company_location']?>.<br>
              <?= @$_SESSION['company_tel'] ?>
          <!--     <?= @$_SESSION['company_alttel'] ?><br> -->
              
            </address>
            </h2>
            <!--</div>-->
          </div>
            <div class=" invoice-col" >
            </div>
              <div class="col-xs-4 invoice-col">
                
           <hr></hr>
          <!--<h2 class="page-header"></h2>-->
          </div><!-- /.col -->
          
          </div><!-- /.col -->
            <table class="" border="1" >
              <thead>
                <tr>
                   <th>#</th>
                   <th>Item(s)</th>
                  <th>Qty</th>
                  <th>U.Price</th>
                  <th>Sub</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if(!empty($printpreview)) :
                    $counter = 1;
                    for($a = 0; $a < sizeof($printpreview['prodname']); $a++) :
                ?>
                <tr>
                  <td><?= $counter ?></td>
                  <td><?= $printpreview['prodname'][$a] ?><br/>
                      (<small style="font-size: x-small">GHS <?=$printpreview['produnit'][$a]?></small>)
                  </td>
                  <td><?= $printpreview['prodqty'][$a] ?></td>
                  <td><?php 
                    $total_tax = 1 + ($_SESSION['settings']['vat'] + $_SESSION['settings']['getfund'] + $_SESSION['settings']['nhil']) / 100;
                    $new_unit_price = number_format($printpreview['produnit'][$a] / $total_tax , 2);
                    print $new_unit_price;
                  ?></td>
                  <td><?php 
                    $sum_of_subtotals[] = $printpreview['prodqty'][$a] * $new_unit_price;
                    print $printpreview['prodqty'][$a] * $new_unit_price;
                  ?></td>
                </tr>
                <?php
                      $counter++;
                    endfor;
                  endif;
                  $sum_of_unit_prices = array_sum($sum_of_subtotals);
                ?>
              </tbody>
            </table>
         <!-- /.col -->
           <!-- accepted payments column -->
          <div class="">
            <div class="table-responsive">
             <div class=" invoice-col">
              <b>Billed to: </b><?=$printpreview['customer']?><br>
              <b>Sold by: </b> <?=$printpreview['sold_by']?><br>
              <b>Invoice#:</b> <?=str_pad($printpreview['last_id']+1, 6,"0",STR_PAD_LEFT)?><br>
              <!-- <?= @$_SESSION['company_email'] ?><br>
              <?= @$_SESSION['company_website'] ?><br> -->
            <b>Date:</b> <?= gmdate("d F, Y")?>
          </div><!-- /.col -->
            </div>
          </div><!-- /.col -->
          <div class="">
            <div class="">
              <table class="" style="font-style: 10px">
                <tr>
                  <th style="width:50%">Subtotal:</th>
                  <td>GHȻ <?= $sum_of_units = round($sum_of_unit_prices,2) ?></td>
                </tr>
                <tr>
                  <td>GetF + NHIL <!-- (<?=$_SESSION['settings']['tax']?>%) --></td>
                  <td>GHȻ
                    <?php  
                      $value = ($_SESSION['settings']['getfund'] + $_SESSION['settings']['nhil']) / 100;
                      $getfund_nhil_levy = round($value * round($sum_of_unit_prices,2) , 2);
                      $taxable_value = $sum_of_units + $getfund_nhil_levy;
                      print $getfund_nhil_levy;
                    ?></td>
                </tr>
                  <tr>
                      <td>Taxable Amt</td>
                      <td>GHȻ <?=$taxable_value?></td>
                  </tr>
                  <tr>
                      <td>VAT <!-- (<?=$_SESSION['settings']['tax']?>%) --></td>
                      <td>GHȻ
								  <?= $vat = round($taxable_value * $_SESSION['settings']['vat']/100,2) ?></td>
                  </tr>
                  <tr>
                      <th>Total Amt </th>
                      <th>GHȻ <?= number_format(($taxable_value + $vat),1) ?></th>
                  </tr>
              </table>
            </div>
          </div><!-- /.col -->
          <div class="">
            <div class="">
              <table class="">
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->
    <!-- AdminLTE App -->
    <script type="text/javascript">
       window.print();
        var delay = 100;
        var url = '<?=base_url()?>dashboard/pos'
        setTimeout(function(){ window.location = url; }, delay);
    </script>
   
  </body>
