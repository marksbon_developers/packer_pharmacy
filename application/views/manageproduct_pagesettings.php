<!-- Embedded Scripts -->
<script type="text/javascript">
	// Edit Product
  	function editproduct(element){
	    $('[name="edit_prod_id"]').val($(element).data('productid'));
	    $('[name="edit_prod_name"]').val($(element).data('prodname'));
	    $('[name="edit_prod_code"]').val($(element).data('prodcode'));
	    $('[name="edit_unit_qty"]').val($(element).data('produnitqty'));
	    $('[name="edit_cur_qty"]').val($(element).data('prodcurqty'));
	    $('[name="edit_cost_price"]').val($(element).data('prodcostprice'));
	    $('[name="edit_unit_price"]').val($(element).data('produnitprice'));
	    $('[name="edit_unit_promoqty"]').val($(element).data('promoqty'));
	    $('[name="edit_unit_promoprice"]').val($(element).data('promoprice'));
	    $('#editdesc option[value="'+ $(element).data('descid') + '"]').attr('selected', 'selected');
	    $('#updateprodmodal').modal('show');
    };

    /********** Add More To Stock Update ********/
    $(".table").on("click", ".addmore", function(){

        var products = "<?php if(!empty($productnames)) { foreach ($productnames As $prod) { print "<option value='".base64_encode($prod->prod_id)."'>".$prod->prod_name."</option>"; } } ?>";

        /*var category = "<?php if(!empty($Description_info)) { foreach ($Description_info As $desc) { print "<option value='".base64_encode($desc->desc_id)."'>".$desc->desc."</option>"; } } ?>";*/

        /* $('#newstock tr:last').after('<tr><td><select class="form-control" name="prod_id[]"><option></option>'+products+'</select></td><td><select class="form-control" name="cat_id[]"><option></option>'+category+'</select></td><td><input type="number" class="form-control" name="costP[]" placeholder="100" required style="height:33px !important;"/></td><td><input type="number" min="1" class="form-control qty" name="qty[]" placeholder="100" required style="height:33px !important;"/></td><td><input id="unit" type="number" class="form-control unit" name="unit[]" placeholder="20" required style="height:33px !important;"/></td><td><input id="totalprice" type="number" class="form-control totalamt" name="sup[]" placeholder="20" required style="height:33px !important;" disabled/></td><td><a href="" title="Add New" class="btn btn-primary btn-xs addmore"><i class="fa fa-plus"></i></a> <a href="" title="Delete" class="btn btn-danger btn-xs del_req_row"><i class="fa fa-trash"></i></a></td></tr>');*/

        $('#newstock tr:last').after('<tr><td><select class="form-control" name="prod_id[]"><option></option>'+products+'</select></td><td><input type="number" min="1" class="form-control qty" name="qty[]" placeholder="100" required style="height:33px !important;"/></td><td><input id="unit" type="number" step="0.1" class="form-control unit" name="unit[]" placeholder="20" required style="height:33px !important;"/></td><td><a href="#" title="Add New" class="btn btn-primary btn-xs addmore"><i class="fa fa-plus"></i></a> <a href="#" title="Delete" class="btn btn-danger btn-xs del_req_row"><i class="fa fa-trash"></i></a></td></tr>');

        $(".qty").on('input',compute);
        $(".unit").on('input',compute);

    });
    /********** Add More To Stock Update ********/

</script>

<!-- Modals -->
<div class="modal fade" id='updateprodmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog" style="width:990px !important;">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Product Edit</h4>
        </div>
        <div class="row">
          <form action="update_product" method="post" enctype="multipart/form-data">
            <input type="hidden" name="edit_prod_id">
            <div class="col-md-6">
              <div class="form-group">
                <label class="col-md-4 control-label">Category</label>
                <div class="col-md-12">
                  <select class="form-control" name="edit_desc_id" id="editdesc" data-placeholder="--- Select One ----" required>
                    <option></option>
                    <?php
                      if(!empty($Description_info)) {
                        $counter = 1;
                        foreach($Description_info As $desc) {
                          print "<option value='".base64_encode($desc->desc_id)."'>$desc->desc</option>";
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-6 control-label">Product Name</label>
                <div class="col-md-12" >
                  <input type="text" class="form-control" placeholder="Enter Here........" name="edit_prod_name" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-6 control-label">Product Code</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" placeholder="Enter Here........" name="edit_prod_code" required readonly>
                </div>
              </div>
                <div class="form-group">
                <label class="col-md-6 control-label">Unit Quantity</label>
                <div class="col-md-12">
                  <input type="number" min="0" class="form-control" placeholder="1" name="edit_unit_qty" required>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-12 control-label">Unit Price</label>
                  <input type="number" min="0" class="form-control" placeholder="1" name="edit_unit_price" step="0.1" required>
                </div>
                <div class="col-md-6">
                <label class="col-md-12 control-label">Cost Price</label>
                  <input type="number" min="0" class="form-control" placeholder="1" name="edit_cost_price" step="0.1">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-12 control-label" style="padding-left:0px">Promo Quantity</label>
                  <input type="number" min="0" class="form-control" placeholder="promo limit" name="edit_unit_promoqty" >
                </div>
                <div class="col-md-6">
                  <label class="col-md-12 control-label" style="padding-left:0px">Promo Price</label>
                  <input type="number" min="0" class="form-control" placeholder="promo Price" name="edit_unit_promoprice" step="0.0001">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-12 control-label">Available Quantity</label>
                <div class="col-md-12">
                  <input type="number" min="1" class="form-control" placeholder="Available Quantity" name="edit_cur_qty" readonly>
                </div>
              </div> 
            </div>
            <div class="col-md-12">
              <div class="form-footer">
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success" name="update_prod"><i class="fa fa-database"></i> Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>