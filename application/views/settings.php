<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#header" data-toggle="tab">Company Details</a></li>
                            <li><a href="#info" data-toggle="tab">Tax Settings</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="header">
                                <div class="row">
											  <?php

											  if (empty(@$companyinfo)) {
												  $action = "register_company";
												  $button = '<button type="submit" class="btn btn-success" name="reg_company"><i class="fa fa-database"></i> Register</button>';
											  } else {
												  $action = "update_company";
												  $button = '<button type="submit" class="btn btn-success" name="up_company"><i class="fa fa-database"></i> Update Info</button>';
											  }
											  ?>
                                    <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="id" value="<?= base64_encode($companyinfo->id) ?>">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Company Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Here........" name="companyname" required
                                                           value="<?= @$companyinfo->name ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Postal Address</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" name="paddr"
                                                           placeholder="Enter Postal Address Here" required
                                                           value="<?= @$companyinfo->address ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Residence Location</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Here........" name="raddr"
                                                           value="<?= @$companyinfo->location ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Primary Telephone</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Here........" name="tel1"
                                                           value="<?= @$companyinfo->tel_1 ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="border-right:2px solid #477">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Secondary Telephone</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" placeholder="Enter Here"
                                                           name="tel2" value="<?= @$companyinfo->tel_2 ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" class="form-control" placeholder="Enter Here"
                                                           name="email" value="<?= @$companyinfo->email ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Webiste</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" placeholder="Enter Here"
                                                           name="website" value="<?= @$companyinfo->website ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Fax</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Red / Green / Blue" name="fax"
                                                           value="<?= @$companyinfo->fax ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="<?= base_url() . @$companyinfo->logo_path ?>"
                                                 style="margin-left:2em;width:120px;height:120px;" class=""><br/>
                                            <strong>
															  <?php (!empty($companyinfo->name)) ? print $companyinfo->name . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->address)) ? print $companyinfo->address . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->location)) ? print $companyinfo->location . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->tel_1)) ? print "Office Tel : " . $companyinfo->tel_1 . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->tel_2)) ? print "Business Tel : " . $companyinfo->tel_2 . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->email)) ? print "Email : " . $companyinfo->email . "<br/>" : False; ?>
															  <?php (!empty($companyinfo->website)) ? print "Website : " . $companyinfo->website . "<br/>" : False; ?>
                                            </strong>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-footer">

                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Logo Upload</label>
                                                    <div class="col-md-8">
                                                        <input type="file" class="form-control" accept="image/*"
                                                               placeholder="" name="logo"
                                                               value="<?= $companyinfo->logo_path ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-9 col-md-offset-3">
																		 <?= $button ?>
                                                        <a href="<?= base_url() ?>dashboard" class="btn btn-danger"><i
                                                                    class="fa fa-times"></i> Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="info">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">VAT</label>
                                            <div class="col-md-12">
                                                <input type="number" class="form-control" placeholder="Enter Here........" step=".1"
                                                       name="mainTax" required value="<?=@$taxInfo->percentage?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">GetFund</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" placeholder="Enter Here........"
                                                       name="companyname" required value="<?=@$taxInfo->getfund?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">NHIL</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" placeholder="Enter Here........"
                                                       name="companyname" required value="<?=@$taxInfo->nhil?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Others</label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" placeholder="Enter Here........"
                                                       name="companyname" required value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-offset-10">
                                    <input type="submit" class="btn btn-primary form-control" name="setting_btn">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>