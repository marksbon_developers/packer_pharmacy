<?php
  # Background Colors
  $bg = [ 0 => "bg-blue",1 => "bg-green",2 => "bg-grey",3 => "bg-aqua",4 => "bg-yellow",5 => "bg-purple",6 => "bg-maroon"
  ];
?>    
<!-- Reset Modals -->
<div class="modal fade" id='resetmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="ResetPassword" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Reset Password</h4>
        </div>
        <div class="modal-body">
          <div class="control-label">
            <label> Username</label>
            <div class="input-group">
              <div class="input-group-addon">
                 <i class="fa fa-user"></i>
              </div>
              <input type="text" class="form-control"  id="resetname" name="date_recv" required readonly/>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> New Password</label>
            <div class="input-group">
              <div class="input-group-addon">
                 <i class="fa fa-key"></i>
              </div>
              <input type="password" class="form-control" name="newpasswd" required/>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> Confirm Password</label>
            <div class="input-group">
              <div class="input-group-addon">
                 <i class="fa fa-key"></i>
              </div>
              <input type="password" class="form-control" name="confirmpasswd" required/>
            </div><!-- /.input group -->
          </div>
          <input type="hidden" id="resetId" name="userResetId"/> 
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button class="btn btn-danger" type="submit" name="resetbtn"><i class="fa fa-database"></i> Update</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- Reset Modals -->

<!-- Employee Details Modals -->
<div class="modal fade" id='employee_modal' role='dialog' aria-hidden='true'>
  <div class="modal-dialog" style="width: 1028px">
    <div class="modal-content">
      <form action="Employee_Info_Update" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title"><i class="fa fa-user"></i> Employee's Full Details</h4>
        </div>
        <div class="row">
        <div class="modal-body">
          <div class="col-lg-6">
          <input type="hidden" class="form-control" name="last_id" value="<?= @$id ?>" readonly required />
          <label class="col-md-12 control-label" style="font-size:25px !important;">Basic Information</label>
          <div class="control-label">
            <label> Employee ID</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" name="edit_employee_id" required readonly/>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> Fullname</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" name="edit_fullname" required readonly />
            </div><!-- /.input group -->
          </div>
          <!--<div class="control-label">
            <label> Date Of Birth</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control datepicker" name="edit_dob" required readonly/>
            </div>
          </div>
          <div class="control-label">
            <label> Gender</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <select class="form-control empgender" name="edit_gender" data-placeholder="--Select--" style="width:100%" required disabled>
                <option></option>
                <option value="M">M</option>
                <option value="F">F</option>
              </select>
            </div>
          </div>-->
          <div class="control-label">
            <label> Referee's Name</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here......" name="edit_emergencyname" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> Referee's Telephone</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_emergencytel" required readonly>
            </div><!-- /.input group -->
          </div>
        </div>
        <div class="col-lg-6">
          <label class="col-md-12 control-label" style="font-size:25px !important;">Contact Information</label>
          <div class="control-label">
            <label> Residence Address</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_resAddr" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> Telephone</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_tel1" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label">
            <label> Alt. Telephone number</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_tel2" required readonly>
            </div><!-- /.input group -->
          </div>
          <!--<div class="control-label">
            <label> Email</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="email" class="form-control" placeholder="Enter Here....." name="edit_email" readonly>
            </div>
          </div>-->
        </div>
        </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-6">
            <button class="btn btn-primary editempbtn" type="button"><i class="fa fa-pencil"></i> Edit</button>
            <button class="btn btn-success empupdatebtn" type="submit" name="update_usr" style="display:none"><i class="fa fa-database"></i> Update</button>
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- vendor Details Modals -->
<div class="modal fade" id='edit-supplier' role='dialog' aria-hidden='true'>
  <div class="modal-dialog" style="width: 1228px">
    <div class="modal-content">
      <form action="Update_Vendor" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title"><i class="fa fa-user"></i> Vendor's Full Details</h4>
        </div>
        <div class="row">
        <div class="modal-body">
        <div class="col-md-12">
          <input type="hidden" class="form-control" name="edit_sup_id" readonly required />
          <label class="col-md-12 control-label" style="font-size:25px !important;">Basic Information</label>
          <div class="control-label col-md-4">
            <label> Fullname</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" name="edit_sup_name" required readonly />
            </div><!-- /.input group -->
          </div>
         <div class="control-label col-md-4">
            <label> Telephone number</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_sup_tel1" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label col-md-4">
            <label> Alt. Telephone number</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_sup_tel2" required readonly>
            </div><!-- /.input group -->
          </div>
      </div>
      <div class="col-md-12">
          <label class="col-md-12 control-label" style="font-size:25px !important;">Contact Information</label>
          
          <div class="control-label col-md-4">
            <label> Email</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="email" class="form-control" placeholder="Enter Here....." name="edit_sup_email" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label col-md-4">
            <label> Location</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_sup_loc" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label col-md-4">
            <label> Postal Address</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="edit_sup_addr" required readonly>
            </div><!-- /.input group -->
          </div>
        </div>
        <div class="col-md-12">
         <label class="col-md-12 control-label" style="font-size:25px !important;">Purchase Information</label>
             <div class="form-group col-md-4">
              <label class="col-md-12 control-label">Payment Terms</label>
              <div class="col-md-12">
                <select class="form-control" id="paytype" name="edit_sup_pay_type" data-placeholder="--- Select One ---" style="width:100% !important" onchange="paymentterms(this.value)" readonly>
                  <option label="--- Select One ---"></option>
                  <option value="Cheque">Cheque</option>
                  <option value="Cash">Cash</option>
                </select>
              </div>
            </div>
           <div class="control-label col-md-4 bank" style="display:none">
            <label>Bank Name</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="bank" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label col-md-4 branch" style="display:none">
            <label>Branch</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="branch" required readonly>
            </div><!-- /.input group -->
          </div>
           <div class="control-label col-md-4 accname" style="display:none">
            <label>Account Name</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="acc_name" required readonly>
            </div><!-- /.input group -->
          </div>
           <div class="control-label col-md-4 accno" style="display:none">
            <label>Account No</label>
            <div class="input-group">
              <div class="input-group-addon"></div>
              <input type="text" class="form-control" placeholder="Enter Here....." name="acc_no" required readonly>
            </div><!-- /.input group -->
          </div>
          <div class="control-label col-md-4">
           <label>Product Type</label>
           <div id="producttype">
                  
            </div>
          </div>

        </div>
        </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button class="btn btn-primary editsupbtn" type="button"><i class="fa fa-pencil"></i> Edit</button>
            <button class="btn btn-success supupdatebtn" type="submit" name="update_sup" style="display:none"><i class="fa fa-database"></i> Update</button>
            <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- Delete Modals -->
<div class="modal fade" id='deletemodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="form_url" action="" method="post">
      <input type="hidden" name="resulturl" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-body">
        Do You Want To Really Delete <?php echo "<strong><em id='deletename'></em></strong>"; ?> .... ?
        <input type="hidden" id="deleteId" name="deleteid" value="" /> 
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-danger" type="submit" name="deletebutton"><i class="fa fa-database"></i> Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- All Delete Modals -->

<!-- issue Modals -->
<div class="modal fade" id='salesrep_modal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="" action="" method="post">
      <input type="hidden" name="resulturl" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title">Sales Details</h4>
      </div>
      <div class="row">
      <div class="modal-body">
       <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Products</th>
          <th>Description</th>
          <th>Quantity</th>
          <th>Unit</th>
          <th>Sum</th>
        </tr>
      </thead>
      <tbody id="salesrep_details">
        
      </tbody>
    </table>

      </div>
      </div>
      </form>
      <div class="modal-footer">
        <div class="pull-right">
          
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- issue Modals -->
<div class="modal fade" id='expences' role='dialog' aria-hidden='true' >
  <div class="modal-dialog"style="width:900px;">
    <div class="modal-content" >
    <form action="" method="post">
      <input type="hidden" name="resulturl" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title">EXPENCES</h4>
      </div>
      <div class="row">
      <div class="modal-body">
       <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Account</th>
          <th>Purpose</th>
          <th>Debit</th>
          <th>Credit</th>
          <th>Balance</th>
        </tr>
      </thead>
      <tbody id="">
        <?php
          $counter = 1;
          if(!empty($cashreport)) : foreach($cashreport As $cashrep) :
        ?>
          <tr>
          <td><?=$counter?></td>
          <?php $result = $this->Universal_Retrieval->ret_data_with_s_cond_row('account_types','id',array('id'=>$cashrep->acc_type_id)); ?>
          <td><?php print_r($result->acc_name)?></td>
          <td><?=$cashrep->purpose?></td>
          <td><?=$cashrep->debit_amt?></td>
          <td><?=$cashrep->credit_amt?></td>
          <td><?=$cashrep->credit_amt - $cashrep->debit_amt?></td>
        </tr>

        <?php 
          endforeach; endif; 
        ?>
      </tbody>
    </table>

      </div>
      </div>
      </form>
      <div class="modal-footer">
        <div class="pull-right">
          <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-print"></i> Print</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- All Delete Modals -->
<!-- issue Modals -->
<div class="modal fade" id='issue' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="form_url" action="" method="post">
      <input type="hidden" name="resulturl" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title">Issue Ticket</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <label class="col-md-4 control-label">Company Name:</label>
        <div class="col-md-12">
          <input id="pay" type="text" class="form-control" placeholder="">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">E-mail:</label>
        <div class="col-md-12">
          <input id="pay" type="text" class="form-control" placeholder="">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Complanant</label>
        <div class="col-md-12">
          <input id="pay" type="text" class="form-control" placeholder="">
        </div>
        </div>
        <label class="col-md-4 control-label">Message</label>
       <textarea style="width:100%; height:170px;"></textarea>
      </div>
      <div class="modal-footer">
        <div class="pull-right">
          <button class="btn btn-success" type="submit" name=""><i class="fa fa-email"></i> Send</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- All Delete Modals -->

<!-- Edit Description / Category Modals -->
<div class="modal fade" id='catdescmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="update_description" method="post" id="cateditforms">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Edit Description</h4>
        </div>
        <div class="modal-body">
          <div class="control-label">
            <label></label>
            <div class="input-group">
              <input type="text" class="form-control" id="oldname" name="new_name" required/>
            </div><!-- /.input group -->
          </div>
          <input type="hidden" id="editid" name="ResetId"/> 
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button class="btn btn-success" type="submit" name="editbtn"><i class="fa fa-database"></i> Update</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- User Edit Modal -->
<div class="modal fade" id='usereditmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">User Role(s) Manage</h4>
        </div>
        <div class="modal-body" id="rolesview">
          
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- User Edit Modal-->

<!-- No Roles Modal -->
<div class="modal fade" id='noproductmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">No Products Found</h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- User Edit Modal-->

<!-- No Roles Modal -->
<div class="modal fade" id='amtexceed' role='dialog' aria-hidden='true' >
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Pay Amount Exceeds Total Cost</h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
          <div class="col-md-4"></div>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- User Edit Modal-->

<!-- User Edit Modal -->

<!-- User Edit Modal-->
<!-- modul-->
<div class="modal fade" id="Account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:900px;">
    <div class="modal-content" >
      <div class="modal-header" style="background-color:rgba(90, 61, 61, 0.13)!important;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
        Accounts
      </div>
      <form action="<?= base_url() ?>Dashboard/Save_Expenses" method="post">
        <div class="modal-body">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <label class="control-label">Select Account</label>
                  <select class="form-control" name="acc_type" data-placeholder="--- Select One ---" style="height:44px" onchange="fetch_acct_desc(this.value)" required>
                    <option></option>

                    <?php if(!empty($accounts)) :  foreach($accounts As $acc) :?>
                    <option value="<?= base64_encode($acc->id) ?>"><?= $acc->acc_name ?></option>
                    <?php endforeach; endif; ?>

                  </select>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Refenece Code</label>
                    <input type="text" class="form-control acct_code" readonly>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Account Description</label>
                    <input type="text" class="form-control acct_desc" readonly>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Debit Amount</label>
                    <input type="text" class="form-control" name="deb_amt">
                  </div>
                </div>
                <!--<div class="col-md-4 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Credit Amount</label>
                    <input type="text" class="form-control" name="cred_amt">
                  </div>
                </div> -->
                <div class="col-md-8 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Purpose</label>
                    <input type="text" class="form-control" name="purpose" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="resulturl" id="resulturl">
        <center>
          <div class="modal-footer" style="background-color:rgba(90, 61, 61, 0.13)!important;">
            <button type="submit" class="btn btn-sm btn-success" name="save_expenses" ><i class="fa fa-database"></i> Save</button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </center>
      </form>
    </div>
  </div>
</div>
<!--quantity modal-->

<!-- Daily Sales Ledger -->
<div class="modal fade" id='salesledger' role='dialog' aria-hidden='true' >
  <div class="modal-dialog" style="width:900px !important;">
    <div class="modal-content">
      <form action="<?= base_url() ?>dashboard/my_daily_ledger" method="post" id="cateditform">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Cash Flow</h4>
        </div>
        <div class="modal-body">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Account Type</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Balance</th>
              </tr>
            </thead>
            <tbody id="cashdet">
              
          </tbody>

        </table> 
        </div>
        <div class="modal-footer">
          
          <div class="col-md-6 pull-right">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
         
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- Reset Modals -->
<!-- modul-->
<div class="modal fade" id="pos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:900px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Company name</h4>

      </div>
      <div class="modal-body">
        <div class="row" >
          <div class="col-md-8 col-sm-6">
        <div class="pricing-table highlight">
         
          <div class="pricing-body">
            <table class="table" style="margin-left:-30px;">
          <thead>
            <tr>
              <th><i class="fa fa-close"></i></th>
              <th>Item</th>
              <th>Qty</th>
              <th>Unit</th>
              <th>Sum</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th ><i class="fa fa-close"></i></th>
              <td>Mark</td>
              <td>66</td>
              <td>66</td>
              <td scope="row">$98</td>
            </tr>
            <tr>
              <th ><i class="fa fa-close"></i></th>
              <td>Jacob</td>
              <td>45</td>
              <td>66</td>
              <td scope="row">$23</td>
            </tr>
            <tr>
              <th ><i class="fa fa-close"></i></th>
              <td>Larry</td>
              <td>23</td>
              <td>66</td>
              <td scope="row">$12</td>
            </tr>
            <tr>
              <th ><i class="fa fa-close"></i></th>
              <td>Larry</td>
              <td>9</td>
              <td>66</td>
              <td scope="row">$54</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th>Total</th>
              <th></th>
              <th></th>
              <th>109</th>
              <th> <div class="pricing-heading">
            
            <div class="price">
              <div class="title"><span class="sign">Ȼ</span>1,900</div>
             
            </div>
          </div>
        </th>
            </tr>
          </tfoot>
        </table>
          </div>
         
        </div>
        </div>
        <div class="col-md-4 col-sm-3">
       <div class="pricing-table highlight">
          <div class="pricing-heading">
             <div class="input-group">
            <select class="select2 " tabindex="-1" aria-hidden="true">
              <option value="1">bismark</option>
              <option value="2">kuku</option>
              <option value="3">nana</option>
              <option value="4">4</option>
            </select>
            <span class="input-group-addon">customer</span>
          </div>
            <div class="price">
              <div class="title">c 1900<span class="sign">$</span></div>
              <div class="subtitle">To be Payed</div>
            </div>
          </div>
          <div class="pricing-body">
            <ul class="description">
              <li><i class="icon ion-ios-lightbulb"></i>c 10 <span class="small">Owe</span></li>
              <li><i class="icon ion-person-stalker"></i>c 0 <span class="small">Cash</span></li>
              <li><i class="icon ion-ios-chatboxes-outline"></i>c -10 <span class="small">Total</span></li>
            </ul>
          </div>
          <div class="pricing-footer">
            <button type="button" class="btn btn-default">Add to depth</button>
          </div>
        </div>
        </div>
    </div>
      </div>
      <center>
      <div class="modal-footer">

        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-sm btn-success">Print</button>
         <button type="button" class="btn btn-sm btn-success">Payed</button>
      </div></center>
    </div>
  </div>
</div>
<!--quantity modal-->

<!-- Reset Modals -->

<!-- Reset Modals -->
