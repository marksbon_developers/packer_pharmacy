<nav class="navbar navbar-default" id="navbar">
  <div class="container-fluid">
    <div class="navbar-collapse collapse in">
      <ul class="nav navbar-nav navbar-mobile">
        <li>
          <button type="button" class="sidebar-toggle">
            <i class="fa fa-bars"></i>
          </button>
        </li>
        <li class="logo">
          <a class="navbar-brand" href="#"><span class="highlight">PARK</span> ER</a>
        </li>
        <li>
          <button type="button" class="navbar-toggle">
            <img class="profile-img" src="<?= base_url() ?>resources/images/profile.png">
          </button>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-left">
        <li class="navbar-title"  id="txt"><i class="fa fa-clock"></i></li>
       <div class="title"><?=$_SESSION['company_location']?></div>
        <li>
          <?php if(!empty($_SESSION['success'])) : ?>
            <div class="col-md-12 col-sm-12">
              <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong><?= $this->session->flashdata("success") ?></strong> 
              </div>
            </div>
          <?php elseif (!empty($_SESSION['error'])) : ?>
            <div class="col-md-12 col-sm-12">
              <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong><?= $this->session->flashdata("error") ?></strong> 
              </div>
            </div>
          <?php endif; ?>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown profile">
          <a href="#" class="dropdown-toggle"  data-toggle="dropdown">
            <img class="profile-img" src="<?= base_url()?>resources/images/profile.png"/>
            <div class="title">Profile</div>
          </a>
          <div class="dropdown-menu">
            <div class="profile-info">
              <h4 class="username"><?= $_SESSION['fullname'] ?></h4>
            </div>
            <ul class="action">
              <li>
                <a href="#">
                 Profile
                </a>
              </li>
              <li>
                <a href="<?= base_url()?>access/logout">
                  Logout
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
