   <div class="row">
  
        <div class="modal-header">
          <div class="col-md-6">
            <a class="btn btn-success"  target="_blink" href="<?= base_url()?>Report/dailyreport"> <i class="fa fa-print"></i> Print</a>
      
          </div>
           <div class="col-md-6 ">
          <label class="col-md-8 control-label">Total<span style="color:red">*</span></label>
          <div class="col-md-12">
          <input type="text" class="form-control" placeholder="0" name="" disabled value="<?= @$totalSales ?>">
          </div>
          </div>
        </div>
         
        <div class="modal-body">
        <!--  <form action="" method="" id=""> -->
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID#</th>
                <th>Products</th>
                <th>Description</th>
                <th>Total Cost</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php  if(!empty($dailyreport)) : $counter = 1; foreach($dailyreport As $report) :  ?>
              <tr>
                <td><?= $counter ?></td>
                <?php 
                  $prods = explode('|',$report->prod_id);

                  foreach($prods As $prod)
                  {
                    $data = [ 'prod_id' =>  $prod ];
                    $product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes','prod_id',$data);
                    
                    if(!empty($product)){
                      @$productname .= $product->prod_name. ",";
                    }
                    
                  }
                ?>

                <td><?php print @$productname; unset($productname); ?></td>
                <td><?= $report->description ?></td>
                <td><?= $report->tot_cost ?></td>
                <td><button data-toggle="modal" class='btn btn-success btn-xs salesdetails' data-trans="<?= $report->id ?>"><i class='fa fa-lock'></i> Details</button>
                </td>
              </tr>
              <?php $counter++; endforeach; endif; ?>
          </tbody>

        </table> 
      <!--    </form>-->
        </div>
        <div class="modal-footer">
          
         
         
        </div>
    
      </div>