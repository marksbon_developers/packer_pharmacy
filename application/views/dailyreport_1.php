  <head>
  <title>Report</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">

</head>



<body onload="window.print();">
    <div class="wrapper">
      <!-- Main content -->
        
        <div class="row" style="margin-top:-30px;">
         <div class="col-xs-12">
          <div class="col-xs-4 invoice-col">
           <!-- <h2 class="page-header"> -->
            <img src="<?= base_url().@$_SESSION['company_logo'] ?>" style="margin-left:0em;width:120px;height:60px;" class="">
            </h2>
            <!--</div>--><hr></hr>
          </div>
            <div class="col-xs-4 invoice-col" style="margin-top:60px;"><hr></hr>  </div>
              <div class="col-xs-4 invoice-col">
                
            <address style="height:60px;">
              <strong><?= @$_SESSION['company_name'] ?></strong><br>
              <?= @$_SESSION['company_address'] ?><br>
              <?= @$_SESSION['company_location'] ?><br>
              <?= @$_SESSION['company_tel'] ?> / 
              <?= @$_SESSION['company_alttel'] ?><br>
           
      
            </address>
            <hr></hr>
          <!--<h2 class="page-header"></h2>-->
          </div><!-- /.col -->
          
          </div><!-- /.col -->
        </div>

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 ">
                <table class="table-striped primary" cellspacing="0" width="100%">
       <thead>
              <tr>
                <th>ID</th>
                <th>Products</th>
                <th>Description</th>
                <th>Total Cost</th>
              </tr>
            </thead>
            <tbody>
              <?php  if(!empty($dailyreport)) : $counter = 1; foreach($dailyreport As $report) :  ?>
              <tr>
                <td><?= $counter ?></td>
                <?php 
                  $prods = explode('|',$report->prod_id);

                  foreach($prods As $prod)
                  {
                    $data = [ 'prod_id' =>  $prod ];
                    $product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes','prod_id',$data);
                    
                    if(!empty($product)){
                      @$productname .= $product->prod_name. ",";
                    }
                    
                  }
                ?>

                <td><?= @$productname ?></td>
                <td><?= $report->description ?></td>
                <td><?= $report->tot_cost ?></td>
                
                </td>
              </tr>
              <?php $counter++; endforeach; endif; ?>
          </tbody>

         </table> 
          </div><!-- /.col -->
        </div><!-- /.row -->

       

      </div>
    </div><!-- ./wrapper -->

    <!-- AdminLTE App -->
   
  </body>